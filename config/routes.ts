﻿export default [
  {
    path: '/user',
    component: '../layouts/UserLayout',
    routes: [
      {
        name: 'login',
        path: '/user/login',
        component: './user/login',
      },
    ],
  },
  {
    path: '/',
    component: '../layouts/SecurityLayout',
    routes: [
      {
        path: '/',
        component: '../layouts/BasicLayout',
        authority: ['admin', 'user'],
        routes: [
          {
            path: '/',
            redirect: '/welcome',
          },
          {
            path: '/welcome',
            name: 'welcome',
            icon: 'smile',
            component: './Welcome',
          },
          {
            path: '/admin',
            name: 'admin',
            icon: 'crown',
            component: './Admin',
            authority: ['admin'],
            routes: [
              {
                path: '/admin/sub-page',
                name: 'sub-page',
                icon: 'smile',
                component: './Welcome',
                authority: ['admin'],
              },
            ],
          },
          {
            name: 'dashboard',
            icon: 'table',
            path: '/dashboard',
            routes: [
              {
                name: 'dashboard-analysis',
                path: '/dashboard/analysis',
                component: './dashboard/analysis'
              },
              {
                name: 'dashboard-workbench',
                path: '/dashboard/workbench',
                component: './dashboard/workbench',
              },
              {
                name: 'dashboard-workbench-cqdetail',
                path: '/dashboard/workbench/fillCqDetailInfo',
                component: './dashboard/workbench/fillCqDetailInfo',
                hideInMenu: true
              },
              {
                name: 'dashboard-workbench-cqdoc',
                path: '/dashboard/workbench/fillCqDocInfo',
                component: './dashboard/workbench/fillCqDocInfo',
                hideInMenu: true
              },
              {
                name: 'dashboard-workbench-cqtrade',
                path: '/dashboard/workbench/allotCqTrade',
                component: './dashboard/workbench/allotCqTrade',
                hideInMenu: true
              },
            ]
          },
          {
            name: 'cq',
            icon: 'table',
            path: '/cq',
            routes: [
              {
                name: 'cq-add',
                path: '/cq/add',
                component: './cq/add'
              },
              {
                name: 'cq-list',
                path: '/cq/list',
                component: './cq/list'
              },
            ]
          },
          {
            name: 'task',
            icon: 'table',
            path: '/task',
            routes: [
              {
                name: 'task-list',
                path: '/task/list',
                component: './task/list'
              },
            ]
          },
          /*
          {
            name: 'task',
            icon: 'table',
            path: '/task',
            routes: [
              {
                name: 'task-list',
                path: '/task/list',
                component: './task/list'
              },
              {
                name: 'task-center',
                path: '/task/center',
                component: './task/center'
              },
              {
                path: '/task/center/fillCqDetailInfo',
                hideInMenu: true,
                component: './task/fillCqDetailInfo'
              },
              {
                path: '/task/center/fillCqDocInfo',
                hideInMenu: true,
                component: './task/fillCqDocInfo'
              },
              {
                path: '/task/center/allotCqTrade',
                hideInMenu: true,
                component: './task/allotCqTrade'
              }
            ]
          },
          {
            name: 'cq',
            icon: 'table',
            path: '/cq',
            routes: [
              {
                name: 'cq-add',
                path: '/cq/add',
                component: './cq/add'
              },
              {
                name: 'cq-list',
                path: '/cq/list',
                component: './cq/list'
              },
              {
                name: 'cq-detail',
                path: '/cq/detail',
                hideInMenu: true,
                component: './cq/detail'
              },
              {
                name: 'cq-change',
                path: '/cq/change',
                hideInMenu: true,
                component: './cq/change'
              }
            ]
          },
          {
            name: 'workbench',
            icon: 'table',
            path: '/workbench',
            routes: [
              {
                name: 'my-exam',
                path: '/workbench/exam/list',
                component: './workbench/exam/list'
              },
              {
                path: '/workbench/exam/list/test',
                hideInMenu: true,
                component: './workbench/exam/test'
              },
              {
                name: 'my-exam1',
                path: '/workbench/exam/list/test1',
                component: './workbench/exam/test1'
              },
              {
                path: '/workbench/exam/list/paper',
                hideInMenu: true,
                component: './workbench/exam/paper'
              },
              {
                name: 'workbench2',
                icon: 'table',
                path: '/workbench2',
                routes: [
                  {
                    name: 'my-exam1',
                    path: '/workbench2/exam/list/test1',
                    component: './workbench/exam/test1'
                  }
                ]
              },
            ]
          },
          */
          {
            name: 'base',
            icon: 'table',
            path: '/base',
            routes: [
              {
                name: 'menu-list',
                path: '/base/menu/list',
                component: './base/menu/list'
              },
              {
                name: 'role-list',
                path: '/base/role/list',
                component: './base/role/list'
              },
              {
                name: 'user-list',
                path: '/base/user/list',
                component: './base/user/list'
              }
            ]
          },
          {
            name: 'system',
            icon: 'table',
            path: '/system',
            routes: [
              {
                name: 'param-list',
                path: '/system/param/list',
                component: './system/param/list'
              },
              {
                name: 'app-list',
                path: '/system/app/list',
                component: './system/app/list'
              },
              {
                name: 'dict-list',
                path: '/system/dict/list',
                component: './system/dict/list'
              },
              {
                name: 'trade-list',
                path: '/system/trade/list',
                component: './system/trade/list'
              },
              {
                name: 'schedule-list',
                path: '/system/schedule/list',
                component: './system/schedule/list'
              }
            ]
          },
          {
            component: './404',
          },
        ],
      },
      {
        component: './404',
      },
    ],
  },
  {
    component: './404',
  },
];
