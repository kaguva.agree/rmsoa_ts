const { generateService } = require('@umijs/openapi');

generateService({
  schemaPath: 'http://127.0.0.1:8081/v2/api-docs-ext?group=default',
  serversPath: './servers',
})