import { sendGetJsonRequest } from "@/utils/requestEx";

export async function fetchOverview(params: any) {
  const endPointURI = '/dev/dashboard/overview';
  return sendGetJsonRequest(endPointURI, params);
}

export async function fetchStatisticOfLastWillPrd(params: any) {
  const endPointURI = '/dev/dashboard/lastWillPrd/GBSP';
  return sendGetJsonRequest(endPointURI, params);
}

export async function fetchStatistics(params: any) {
  const endPointURI = '/dev/dashboard/statistics';
  return sendGetJsonRequest(endPointURI, params);
}

export async function fetchStatisticOfAlreadyPrd(params: any) {
  const endPointURI = '/dev/dashboard/statistic/alreadyPrd';
  return sendGetJsonRequest(endPointURI, params);
}

export async function fetchStatisticOfNotPrd(params: any) {
  const endPointURI = '/dev/dashboard/statistic/notPrd';
  return sendGetJsonRequest(endPointURI, params);
}

export async function fetchRankings(params: any) {
  const endPointURI = '/dev/dashboard/ranking';
  return sendGetJsonRequest(endPointURI, params);
}