import { CqBaseInfoItem } from "@/pages/cq/data";
import { PageQueryParamType, pageRequest, sendPostRequest, sendGetJsonRequest } from "@/utils/requestEx";

/**
 * 分页查询参条件
 * @param paramName 可输，参数名称
 * @param paramKey 可输，参数键名
 */
export interface CqQueryType extends PageQueryParamType {
  paramName?: string;
  paramKey?: string;
}

export async function fetchAllCq(params: CqQueryType) {
  const endPointURI = '/dev/cq/list';
  return pageRequest(endPointURI, params);
}

export async function addCqBaseInfo(params: CqBaseInfoItem) {
  const endPointURI = '/dev/cq/insert';
  return sendPostRequest(endPointURI, params);
}

export async function fetchCqDetail(params: any) {
  const { cqId } = params;
  const endPointURI = `/dev/cq/detail/${cqId}`;
  return sendGetJsonRequest(endPointURI, params);
}

export async function fetchCqTrades(params: any) {
  const { cqId } = params;
  const endPointURI = `/dev/cq/trades/${cqId}`;
  return sendGetJsonRequest(endPointURI, params);
}