import { PageQueryParamType, pageRequest, sendPostRequest, sendGetJsonRequest } from "@/utils/requestEx";

/**
 * 分页查询参条件
 * @param paramName 可输，参数名称
 * @param paramKey 可输，参数键名
 */
export interface AppQueryType extends PageQueryParamType {
  paramName?: string;
  paramKey?: string;
}

export async function queryCqUndoTaskList(params: AppQueryType) {
  const endPointURI = '/dev/task/center/cq';
  return pageRequest(endPointURI, params);
}

export async function queryTradeUndoTaskList(params: AppQueryType) {
  const endPointURI = '/dev/task/center/trade';
  return pageRequest(endPointURI, params);
}

export async function fillCqDetailInfo(params: any) {
  const endPointURI = `/dev/cq/update/detail`;
  return sendPostRequest(endPointURI, params);
}

export async function fillCqDocInfo(params: any) {
  const endPointURI = `/dev/cq/update/doc`;
  return sendPostRequest(endPointURI, params);
}

export async function fetchCqTrades(params: any) {
  const { cqId } = params;
  const endPointURI = `/dev/cq/trades/${cqId}`;
  return sendGetJsonRequest(endPointURI, params);
}

export async function cqAddTrade(params: any) {
  const endPointURI = `/dev/cq/add/trade`;
  return sendPostRequest(endPointURI, params);
}

export async function preAllocTradeTask(params: any) {
  const endPointURI = `/dev/trade/task/prealloc`;
  return sendPostRequest(endPointURI, params);
}

export async function updateTradeTask(params: any) {
  const endPointURI = `/dev/trade/task/update`;
  return sendPostRequest(endPointURI, params);
}

export async function allotCqTrade(params: any) {
  const endPointURI = `/dev/cq/allot/trade`;
  return sendPostRequest(endPointURI, params);
}

/**
 * 开发人员确认任务
 * @param params 交易任务信息
 * @returns 任务处理结果
 */
export async function developerHandleTask(params: any) {
  const endPointURI = `/dev/trade/task/develop`;
  return sendPostRequest(endPointURI, params);
}

/**
 * 测试人员确认任务
 * @param params 交易任务信息
 * @returns 任务处理结果
 */
export async function testerHandleTask(params: any) {
  const endPointURI = `/dev/trade/task/test`;
  return sendPostRequest(endPointURI, params);
}

/**
 * 主开人员确认任务
 * @param params 交易任务信息
 * @returns 任务处理结果
 */
export async function auditorHandleTask(params: any) {
  const endPointURI = `/dev/trade/task/audit`;
  return sendPostRequest(endPointURI, params);
}

/**
 * 设计人员确认任务
 * @param params 交易任务信息
 * @returns 任务处理结果
 */
export async function designerHandleTask(params: any) {
  const endPointURI = `/dev/trade/task/confirm`;
  return sendPostRequest(endPointURI, params);
}