import request from '@/utils/request';
import { sendGetJsonRequest } from '@/utils/requestEx';

export async function query(): Promise<any> {
  return request('/api/users');
}

export async function queryCurrent(params: any): Promise<any> {
  let { userId } = params;
  if (!userId) {
    userId = '20';
  }
  const endPointURI = `/api/user/current/${userId}`;
  return sendGetJsonRequest(endPointURI, params);
}

export async function queryNotices(): Promise<any> {
  return request('/api/notices');
}
