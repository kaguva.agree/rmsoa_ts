import { TradeItem } from "@/pages/system/trade/data";
import { PageQueryParamType, pageRequest, sendPostRequest } from "@/utils/requestEx";

/**
 * 分页查询参条件
 * @param funcCode 可输，参数名称
 * @param funcName 可输，参数键名
 */
export interface TradeQueryType extends PageQueryParamType {
  funcCode?: string;
  funcName?: string;
  owner?: string;
  clstCode?: string;
  appCode?: string;
}

export async function fetchAllTrade(params: TradeQueryType) {
  const endPointURI = '/dev/systrade/list';
  return pageRequest(endPointURI, params);
}

export async function addTrade(params: TradeItem) {
  const endPointURI = '/dev/systrade/add';
  return sendPostRequest(endPointURI, params);
}

export async function updateTrade(params: TradeItem) {
  const endPointURI = '/dev/systrade/update';
  return sendPostRequest(endPointURI, params);
}

export async function deleteTrades(params: TradeItem) {
  const endPointURI = '/dev/systrade/delete';
  return sendPostRequest(endPointURI, params);
}
