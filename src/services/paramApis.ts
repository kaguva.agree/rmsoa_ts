import { PageQueryParamType, pageRequest, sendPostRequest } from "@/utils/requestEx";

/**
 * 分页查询参条件
 * @param paramName 可输，参数名称
 * @param paramKey 可输，参数键名
 */
export interface ParamQueryType extends PageQueryParamType {
  paramName?: string;
  paramKey?: string;
}

/**
 * 系统参数
 */
export interface ParamItem {
  paramName: string;
  paramKey: string;
  paramValue: string;
  paramValueDesc: string;
}

export async function fetchAllParam(params: ParamQueryType) {
  const endPointURI: string = '/dev/param/list';
  return pageRequest(endPointURI, params);
}

export async function addParam(params: ParamItem) {
  const endPointURI = '/dev/param/add';
  return sendPostRequest(endPointURI, params);
}

export async function updateParam(params: ParamItem) {
  const endPointURI = '/dev/param/update';
  return sendPostRequest(endPointURI, params);
}

export async function deleteParams(params: ParamItem) {
  const endPointURI = '/dev/param/delete';
  return sendPostRequest(endPointURI, params);
}
