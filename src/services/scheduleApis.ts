import { JobItem } from "@/pages/system/schedule/data";
import { PageQueryParamType, pageRequest, sendPostRequest } from "@/utils/requestEx";

/**
 * 分页查询参条件
 * @param paramName 可输，参数名称
 * @param paramKey 可输，参数键名
 */
export interface JobQueryType extends PageQueryParamType {
  paramName?: string;
  paramKey?: string;
}

export async function fetchAllJob(params: JobQueryType) {
  const endPointURI = '/dev/schedule/quartz/list';
  return pageRequest(endPointURI, params);
}

export async function addJob(params: JobItem) {
  const endPointURI = '/dev/schedule/quartz/add';
  return sendPostRequest(endPointURI, params);
}

export async function updateJob(params: JobItem) {
  const endPointURI = '/dev/schedule/quartz/update';
  return sendPostRequest(endPointURI, params);
}

export async function deleteJobs(params: JobItem[]) {
  const endPointURI = '/dev/schedule/quartz/delete';
  return sendPostRequest(endPointURI, params);
}

export async function runJob(params: JobItem) {
  const endPointURI = '/dev/schedule/quartz/run';
  return sendPostRequest(endPointURI, params);
}

export async function pauseJob(params: JobItem) {
  const endPointURI = '/dev/schedule/quartz/pause';
  return sendPostRequest(endPointURI, params);
}

export async function resumeJob(params: JobItem) {
  const endPointURI = '/dev/schedule/quartz/resume';
  return sendPostRequest(endPointURI, params);
}
