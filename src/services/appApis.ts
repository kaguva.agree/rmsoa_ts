import { AppItem } from "@/pages/system/app/data";
import { PageQueryParamType, pageRequest, sendPostRequest } from "@/utils/requestEx";

/**
 * 分页查询参条件
 * @param paramName 可输，参数名称
 * @param paramKey 可输，参数键名
 */
export interface AppQueryType extends PageQueryParamType {
  paramName?: string;
  paramKey?: string;
}

export async function fetchAllApp(params: AppQueryType) {
  const endPointURI = '/dev/sysapp/list';
  return pageRequest(endPointURI, params);
}

export async function addApp(params: AppItem) {
  const endPointURI = '/dev/sysapp/add';
  return sendPostRequest(endPointURI, params);
}

export async function updateApp(params: AppItem) {
  const endPointURI = '/dev/sysapp/update';
  return sendPostRequest(endPointURI, params);
}

export async function deleteApps(params: AppItem) {
  const endPointURI = '/dev/sysapp/delete';
  return sendPostRequest(endPointURI, params);
}
