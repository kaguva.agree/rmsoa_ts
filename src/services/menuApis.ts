import { MenuItem } from "@/pages/base/menu/data";
import { PageQueryParamType, pageRequest, sendPostRequest, sendGetJsonRequest } from "@/utils/requestEx";

/**
 * 分页查询参条件
 * @param funcCode 可输，参数名称
 * @param funcName 可输，参数键名
 */
export interface MenuQueryType extends PageQueryParamType {
  funcCode?: string;
  funcName?: string;
  owner?: string;
  clstCode?: string;
  appCode?: string;
  roleId?: string;
}

export async function fetchAllMenu(params: MenuQueryType) {
  const endPointURI = '/dev/menu/list';
  return pageRequest(endPointURI, params);
}

export async function addMenu(params: MenuItem) {
  const endPointURI = '/dev/menu/add';
  return sendPostRequest(endPointURI, params);
}

export async function updateMenu(params: MenuItem) {
  const endPointURI = '/dev/menu/update';
  return sendPostRequest(endPointURI, params);
}

export async function deleteMenus(params: MenuItem) {
  const endPointURI = '/dev/menu/delete';
  return sendPostRequest(endPointURI, params);
}

export async function fetchMenuTreeByRole(params: MenuQueryType) {
  const { roleId } = params;
  const endPointURI = `/dev/menu/tree/${roleId}`;
  return sendGetJsonRequest(endPointURI, params);
}