import { sendGetJsonRequest } from '@/utils/requestEx';

export async function queryCurrentUserMenus(params: any): Promise<any> {
  // 这里是服务端的获取菜单的地址，根据自己情况进行调整
  const { userId } = params;
  const endPointURI = `/api/user/menu/${userId}`;
  return sendGetJsonRequest(endPointURI, params);
}