import { sendGetJsonRequest } from "@/utils/requestEx";

/**
 * 查询 dictName 对应的字典项
 * @param {*} data 参数
 * @returns dictName 对应的字典项
 */
export async function querySysDict(data: any) {
  const { dictName } = data;
  // 判断会话缓存中是否有缓存
  const dictListStr = sessionStorage.getItem(dictName);
  if (dictListStr) {
    // 会话缓存中存在，则取缓存，不再去服务端通讯
    console.info(`缓存中获取数据 ${dictName}`);
    const result = JSON.parse(dictListStr);
    return Promise.resolve(result);
  }
  const endPointURI = `/dev/sysdict/params/${dictName}`;
  const res = sendGetJsonRequest(endPointURI, data);
  res.then(data => {
    const resStr = JSON.stringify(data);
    sessionStorage.setItem(dictName, resStr);
  });
  return res;
}