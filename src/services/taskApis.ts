import { PageQueryParamType, pageRequest, sendGetJsonRequest } from "@/utils/requestEx";

/**
 * 分页查询参条件
 * @param paramName 可输，参数名称
 * @param paramKey 可输，参数键名
 */
export interface TaskQueryType extends PageQueryParamType {
  paramName?: string;
  paramKey?: string;
}

export async function fetchAllTask(params: TaskQueryType) {
  const endPointURI = '/dev/trade/task/list';
  return pageRequest(endPointURI, params);
}

export async function fetchTaskDetail(params: any) {
  const { taskId } = params;
  const endPointURI = `/dev/trade/task/detail/${taskId}`;
  return sendGetJsonRequest(endPointURI, params);
}
