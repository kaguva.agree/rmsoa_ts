export default {
  'button.add.name': '新增',
  'button.edit.name': '修改',
  'button.view.name': '查看',
  'button.delete.name': '删除',
  'button.update.name': '修改',
  'button.run.name': '运行',
  'button.pause.name': '暂停',
  'button.resume.name': '恢复',
  'button.task.name': '分配任务',
};
