import React from 'react';
import { Tag, message } from 'antd';
import { connect, Dispatch } from 'umi';
import { PageContainer } from '@ant-design/pro-layout';
import ProCard from '@ant-design/pro-card';
import AGrid, { AGridButtonCallBackModel } from '@/components/SelfComp/AGrid';
import { JOB_INIT, LIST_JOB, ADD_JOB, UPDATE_JOB, DELETE_JOB, PAUSE_JOB, RESUME_JOB, RUN_JOB } from '@/actions/schedule';
import { getItemValue } from '@/utils/commons';
import { ConnectState } from '@/models/connect';
import { DataItem, FlagEnum } from '@/models/common';
import ScheduleQueryForm from './components/JobQueryForm';
import AddJobModal from './components/AddJobModal';
import UpdateJobModal from './components/UpdateJobModal';
import { JobItem } from './data';

interface JobListProps {
  dispatch: Dispatch;
  jobList: JobItem[],
  total: number,
  loading: boolean;
  pageSize: number;
  addLoading: boolean;
  updateLoading: boolean;
  jobStatusData: DataItem[];
  jobConcurrentData: DataItem[];
  jobMisfirePolicyData: DataItem[];
}

interface JobListState {
  addModalVisible: boolean;
  jobFormData: JobItem;
  updateModalVisible: boolean;
  // 表格选择的数据
  selectedRows: JobItem[];
  selectedRowKeys: React.Key[];
  queryParam?: JobItem;
  flag: FlagEnum;
}

const EMPTY: JobItem = {
  jobId: '',
  jobGroup: '',
  jobName: '',
  invokeTarget: '',
  jobDesc: '',
  cronExpression: '',
  jobStatus: '',
  concurrent: '',
  misfirePolicy: '',
  startDateTime: '',
  prevFireDateTime: '',
  nextFireDateTime: '',
  triggerGroup: '',
  triggerName: '',
}

/**
 * 定时任务管理，用于配置定时任务，启停任务
 */
@connect((state: ConnectState) => ({
  jobList: state.schedules.rows,
  pageSize: state.schedules.pageSize,
  total: state.schedules.total,
  jobStatusData: state.systems.jobStatusData,
  jobConcurrentData: state.systems.jobConcurrentData,
  jobMisfirePolicyData: state.systems.jobMisfirePolicyData,
  loading: state.loading.effects['schedules/fetchAllJob'],
  addLoading: state.loading.effects['schedules/addJob'],
  updateLoading: state.loading.effects['schedules/updateJob']
}))
export default class ScheduleList extends React.PureComponent<JobListProps, JobListState> {

  state: JobListState = {
    addModalVisible: false,
    jobFormData: EMPTY,
    updateModalVisible: false,
    // 表格选择的数据
    selectedRows: [],
    selectedRowKeys: [],
    queryParam: EMPTY,
    flag: 'edit',
  };

  columns = [
    {
      title: '任务分组',
      dataIndex: 'jobGroup',
      width: 120
    },
    {
      title: '任务名称',
      dataIndex: 'jobName',
      width: 150
    },
    {
      title: '任务执行bean',
      dataIndex: 'invokeTarget',
      width: 220
    },
    {
      title: '任务描述',
      dataIndex: 'jobDesc',
      width: 250,
      ellipsis: true
    },
    {
      title: 'Cron时间表达式',
      dataIndex: 'cronExpression',
      width: 140
    },
    {
      title: '任务状态',
      dataIndex: 'jobStatus',
      width: 100,
      render: (text: string, record: JobItem, index: number) => {
        const { jobStatusData } = this.props;
        const { jobStatus } = record;
        const value = getItemValue(jobStatusData, jobStatus);
        return (
          jobStatus === '0' ? <Tag color="success">{value}</Tag> : <Tag color="error">{value}</Tag>
        );
      },
    },
    {
      title: '是否允许并发执行',
      dataIndex: 'concurrent',
      width: 160,
      render: (text: string, record: JobItem, index: number) => {
        const { jobConcurrentData } = this.props;
        const { concurrent } = record;
        return (
          getItemValue(jobConcurrentData, concurrent)
        );
      },
    },
    {
      title: 'misfire处理策略',
      dataIndex: 'misfirePolicy',
      width: 160,
      render: (text: string, record: JobItem, index: number) => {
        const { jobMisfirePolicyData } = this.props;
        const { misfirePolicy } = record;
        return (
          getItemValue(jobMisfirePolicyData, misfirePolicy)
        );
      },
    },
    {
      title: '任务开始时间',
      dataIndex: 'startDateTime',
      width: 160
    },
    {
      title: '上一次执行时间',
      dataIndex: 'prevFireDateTime',
      width: 160
    },
    {
      title: '下一次执行时间',
      dataIndex: 'nextFireDateTime',
      width: 160
    },
    {
      title: '触发器分组',
      dataIndex: 'triggerGroup',
      width: 150
    },
    {
      title: '触发器名称',
      dataIndex: 'triggerName',
      width: 150
    }
  ];

  constructor(props: JobListProps) {
    super(props);
    console.log('ScheduleList.constructor');
    // 查询本页面需所有参数
    const { dispatch } = this.props;
    dispatch(JOB_INIT({}));
  }

  componentDidMount() {
    console.info('ScheduleList.componentDidMount');
    // 默认查询第一页
    // const { pageSize } = this.props;
    // this.fetchAllJob(1, pageSize);
  }

  openAddJobModal = () => {
    this.setState({
      addModalVisible: true
    });
  }

  handleAddModalOk = async (record: JobItem) => {
    console.log(record);
    const { dispatch } = this.props;
    const res = await dispatch(ADD_JOB(record));
    console.info(res);
    if (res) {
      this.setState({
        addModalVisible: false
      });
      const { pageSize } = this.props;
      this.fetchAllJob(1, pageSize);
    }
  }

  handleAddModalCancel = () => {
    this.setState({ addModalVisible: false });
  };

  /**
   * 修改参数，打开参数修改窗口
   */
  updateJob = (record: JobItem, flag: FlagEnum) => {
    // 表格选中内容
    const { jobFormData } = this.state;
    this.setState({
      jobFormData,
      updateModalVisible: true,
      flag
    });
  }

  openUpdateJobModal = (record: JobItem[], flag: FlagEnum) => {
    // 表格选中内容
    console.info('openUpdateJobModal');
    console.info(record);
    const row = record[0];
    const jobFormData = {
      ...row,
    };
    this.setState({
      jobFormData,
      updateModalVisible: true,
      flag
    });
  }

  /**
   * 更新面板的确定事件
   * @param {*} record 更新表单
   */
  handleUpdateModalOk = async (record: JobItem) => {
    console.log('handleUpdateModalOk');
    console.log(record);
    const { jobFormData, flag } = this.state;
    console.info(jobFormData);
    if (flag === 'edit') {
      const { dispatch } = this.props;
      const res = await dispatch(UPDATE_JOB(record));
      console.info(res);
      if (res) {
        this.setState({
          updateModalVisible: false,
          jobFormData: EMPTY
        });
        const { pageSize } = this.props;
        this.fetchAllJob(1, pageSize);
      }
    } else {
      this.setState({
        updateModalVisible: false,
        jobFormData: EMPTY
      });
    }
  }

  handleUpdateModalCancel = () => {
    this.setState({
      updateModalVisible: false,
      jobFormData: EMPTY
    });
  };

  /**
   * 根据条件分页查询表格数据
   * @param {*} record 条件
   */
  handleFetchJob = (record: JobItem) => {
    console.log(record);
    const queryParam = {
      ...record
    };
    // 查询条件保存起来，方便点击页码时使用
    // setState回调方法，保证先更新state
    this.setState({
      queryParam
    }, () => {
      const { pageSize } = this.props;
      this.fetchAllJob(1, pageSize);
    });
  }

  /**
   * 页码改变的回调，参数是改变后的页码及每页条数
   * @param pageNum 改变后的页码，上送服务器端
   * @param pageSize 每页数据条数
   */
  onPageNumAndSizeChange = (pageNum: number, pageSize: number) => {
    console.log(pageNum, pageSize);
    this.fetchAllJob(pageNum, pageSize);
  };

  /**
   * 分页查询系统参数信息
   * @param {*} pageNum 页码
   * @param {*} pageSize 每页记录条数
   */
  fetchAllJob = (pageNum: number, pageSize: number) => {
    const { selectedRows } = this.state;
    // 查询前先清除掉表格勾选
    if (selectedRows.length > 0) {
      this.setState({
        selectedRows: []
      }, () => {
        this.fetchAllJob0(pageNum, pageSize);
      });
    } else {
      this.fetchAllJob0(pageNum, pageSize);
    }
  }

  /**
   * 分页查询系统参数信息
   * @param {*} pageNum 页码
   * @param {*} pageSize 每页记录条数
   */
  fetchAllJob0 = (pageNum: number, pageSize: number) => {
    const { queryParam } = this.state;
    const { dispatch } = this.props;
    dispatch(LIST_JOB({
      ...queryParam,
      pageSize,
      pageNum
    }));
  }

  /**
   * 处理按钮点击回调事件
   * @param {*} payload 数据包
   */
  handleBtnCallBack = (callBackModel: AGridButtonCallBackModel<JobItem>) => {
    console.log('handleBtnCallBack');
    console.log(callBackModel);
    // btn 按钮
    // keys 表格勾选数组
    const { btn } = callBackModel;
    const { alias } = btn;
    // 新增
    if (alias === 'add') {
      this.openAddJobModal();
      return;
    }
    // 修改
    if (alias === 'edit') {
      const { rows } = callBackModel;
      this.openUpdateJobModal(rows, 'edit');
      return;
    }
    // 删除
    if (alias === 'delete') {
      const { keys } = callBackModel;
      // 调用删除服务，删除勾选数据
      this.deleteJobs(keys);
      return;
    }
    // 查看
    if (alias === 'view') {
      const { rows } = callBackModel;
      this.openUpdateJobModal(rows, 'view');
      return;
    }
    // 运行一次任务
    if (alias === 'run') {
      const { rows } = callBackModel;
      const record = rows[0];
      const { jobStatus } = record;
      if (jobStatus !== '0') {
        message.error("任务状态异常，不允许运行");
        return;
      }
      this.runOnceJob(rows[0]);
      return;
    }
    // 暂停任务
    if (alias === 'pause') {
      const { rows } = callBackModel;
      const record = rows[0];
      const { jobStatus } = record;
      if (jobStatus !== '0') {
        message.error("任务状态异常，不允许暂停");
        return;
      }
      this.pauseJob(rows[0]);
      return;
    }
    // 恢复任务
    if (alias === 'resume') {
      const { rows } = callBackModel;
      const record = rows[0];
      const { jobStatus } = record;
      if (jobStatus !== '1') {
        message.error("任务状态异常，不允许恢复");
        return;
      }
      this.resumeJob(rows[0]);
      return;
    }
  }

  deleteJobs = async (keys: React.Key[]) => {
    const { dispatch } = this.props;
    const res = await dispatch(DELETE_JOB(keys));
    if (res) {
      const { pageSize } = this.props;
      this.fetchAllJob(1, pageSize);
    }
  }

  runOnceJob = async (record: JobItem) => {
    console.log(record);
    const { dispatch } = this.props;
    const res = await dispatch(RUN_JOB(record));
    console.info(res);
    if (res) {
      const { pageSize } = this.props;
      this.fetchAllJob(1, pageSize);
    }
  }

  pauseJob = async (record: JobItem) => {
    console.log(record);
    const { dispatch } = this.props;
    const res = await dispatch(PAUSE_JOB(record));
    console.info(res);
    if (res) {
      const { pageSize } = this.props;
      this.fetchAllJob(1, pageSize);
    }
  }

  resumeJob = async (record: JobItem) => {
    console.log(record);
    const { dispatch } = this.props;
    const res = await dispatch(RESUME_JOB(record));
    console.info(res);
    if (res) {
      const { pageSize } = this.props;
      this.fetchAllJob(1, pageSize);
    }
  }

  /**
   * 表格勾选回调函数
   * @param {*} keys 指定选中项的 key 数组
   * @param {*} rows 表格选中数据集合
   */
  onSelectRow = (keys: React.Key[], rows: JobItem[]) => {
    this.setState({
      selectedRows: rows,
      selectedRowKeys: keys
    });
  };

  render() {
    const code = 'schedule-list';
    const { jobList, loading, total, addLoading, updateLoading, jobStatusData, jobConcurrentData, jobMisfirePolicyData } = this.props;
    const { addModalVisible, updateModalVisible, jobFormData, flag } = this.state;
    const rowKey = (record: JobItem) => record.jobId;
    const pkField = 'jobId';

    return (
      <PageContainer>
        <ProCard title="定时任务列表" headerBordered>
          <ScheduleQueryForm
            colon={false}
            onSubmit={this.handleFetchJob}
            loading={loading}
            jobStatusData={jobStatusData}
          />
          <AGrid
            code={code}
            btnCallBack={this.handleBtnCallBack}
            columns={this.columns}
            rowKey={rowKey}
            dataSource={jobList}
            total={total}
            loading={loading}
            onSelectRow={this.onSelectRow}
            pkField={pkField}
            actionColumnWidth={200}
            actionColumnFixed={'right'}
            scroll={{x:2200}}
            onPageNumAndSizeChange={this.onPageNumAndSizeChange}
          />
        </ProCard>
        {
          !addModalVisible ? null :
          <AddJobModal
            colon={false}
            modalTitle='新增定时任务'
            modalWidth={1000}
            modalVisible={addModalVisible}
            loading={addLoading}
            jobStatusData={jobStatusData}
            jobConcurrentData={jobConcurrentData}
            jobMisfirePolicyData={jobMisfirePolicyData}
            onHandlerOK={this.handleAddModalOk}
            onHandlerCancel={this.handleAddModalCancel}
          />
        }
        {
          !updateModalVisible ? null :
          <UpdateJobModal
            colon={false}
            modalWidth={1000}
            modalVisible={updateModalVisible}
            modalTitle=''
            flag={flag}
            loading={updateLoading}
            formData={jobFormData}
            jobStatusData={jobStatusData}
            jobConcurrentData={jobConcurrentData}
            jobMisfirePolicyData={jobMisfirePolicyData}
            onHandlerOK={this.handleUpdateModalOk}
            onHandlerCancel={this.handleUpdateModalCancel}
          />
        }
      </PageContainer>
    );
  }
}