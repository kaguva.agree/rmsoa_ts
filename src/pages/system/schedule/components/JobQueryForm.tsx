import React from 'react';
import { Form, Input } from 'antd';
import { basicFormItemLangLayout } from '@/utils/commons';
import QueryFilterForm from '@/components/SelfComp/QueryFilterForm';
import ASelect from '@/components/SelfComp/ASelect';
import { QueryFormProps } from '@/models/FormModal';
import { DataItem } from '@/models/common';

export type JobQueryProps = QueryFormProps & {
  jobStatusData: DataItem[]
}

const FormItem = Form.Item;

class JobQueryForm extends React.PureComponent<JobQueryProps> {

  render() {
    const { colon, loading, onSubmit, onReset, jobStatusData } = this.props;
    const formItemLayout = basicFormItemLangLayout;

    return (
      <QueryFilterForm
        colon={colon}
        loading={loading}
        onSubmit={onSubmit}
        onReset={onReset}
      >
        <FormItem label='任务名称' name='jobName' {...formItemLayout} colon={colon}>
          <Input placeholder='支持模糊查询'/>
        </FormItem>
        <FormItem label='任务执行bean' name='invokeTarget' {...formItemLayout} colon={colon}>
          <Input />
        </FormItem>
        <FormItem label='任务状态' name='jobStatus' {...formItemLayout} colon={colon}>
          <ASelect dataSource={jobStatusData} />
        </FormItem>
      </QueryFilterForm>
    );
  }
}

export default JobQueryForm;