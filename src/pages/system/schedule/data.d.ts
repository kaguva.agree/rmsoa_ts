/**
 * 定时任务
 */
export interface JobItem {
  /** 任务id */
  jobId: string;
  /** 任务分组 */
  jobGroup: string;
  /** 任务名称 */
  jobName: string;
  /** 任务执行bean */
  invokeTarget: string;
  /** 任务描述 */
  jobDesc: string;
  /** Cron时间表达式 */
  cronExpression: string;
  /** 任务状态 */
  jobStatus: string;
  /** 是否允许并发执行 */
  concurrent: string;
  /** misfire处理策略 */
  misfirePolicy: string;
  /** 任务开始时间 */
  startDateTime: string;
  /** 上一次执行时间 */
  prevFireDateTime: string;
  /** 下一次执行时间 */
  nextFireDateTime: string;
  /** 触发器分组 */
  triggerGroup: string;
  /** 触发器名称 */
  triggerName: string;
}