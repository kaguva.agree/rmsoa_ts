import React from 'react';
import { connect, Dispatch } from 'umi';
import { PageContainer } from '@ant-design/pro-layout';
import ProCard from '@ant-design/pro-card';
import AGrid, { AGridButtonCallBackModel } from '@/components/SelfComp/AGrid';
import { ConnectState } from '@/models/connect';
import { DataItem } from '@/models/common';
import { APP_INIT, APP_LIST, APP_ADD, APP_UPDATE, APP_DELETE, UPDATE_STATE } from '@/actions/app';
import AppQueryForm from './components/AppQueryForm';
import AddAppModal from './components/AddAppModal';
import UpdateAppModal from './components/UpdateAppModal';
import { AppItem } from './data';

interface AppListProps {
  dispatch: Dispatch;
  appList: AppItem[],
  total: number,
  loading: boolean;
  pageSize: number;
  addLoading: boolean;
  updateLoading: boolean;
  systemData: DataItem[];
  clusterData: DataItem[];
  statusData: DataItem[];
}

interface AppListState {
  addModalVisible: boolean;
  appFormData: AppItem;
  updateModalVisible: boolean;
  // 表格选择的数据
  selectedRows: AppItem[];
  selectedRowKeys: React.Key[];
  queryParam?: AppItem;
  flag: 'view' | 'edit' | 'add' | 'delete';
}

const EMPTY: AppItem = {
  appId: '',
  systemCode: '',
  systemName: '',
  appCode: '',
  appName: '',
  manager: '',
  cluster: '',
  status: ''
}

/**
 * 应用管理，管理项目中的系统应用，以便职责分明
 */
@connect((state: ConnectState) => ({
  appList: state.apps.rows,
  total: state.apps.total,
  pageSize: state.apps.pageSize,
  systemData: state.apps.systemData,
  clusterData: state.apps.clusterData,
  statusData: state.apps.statusData,
  // 当用户 dispatch 对应 effect 时，dva 会自动注入对应 effect 的 loading 状态
  loading: state.loading.effects['apps/fetchAllApp'],
  addLoading: state.loading.effects['apps/addApp'],
  updateLoading: state.loading.effects['apps/updateApp'],
}))
export default class AppList extends React.PureComponent<AppListProps, AppListState> {

  state: AppListState = {
    addModalVisible: false,
    appFormData: EMPTY,
    updateModalVisible: false,
    // 表格选择的数据
    selectedRows: [],
    selectedRowKeys: [],
    flag: 'edit',
  };

  columns = [
    {
      title: '系统编码',
      dataIndex: 'systemCode',
      // width: 150
    },
    {
      title: '应用编码',
      dataIndex: 'appCode',
      // width: 150
    },
    {
      title: '应用名称',
      dataIndex: 'appName',
      // width: 150
    },
    {
      title: '负责人',
      dataIndex: 'manager',
      // width: 70
    },
    {
      title: '所在集群',
      dataIndex: 'cluster',
      // width: 70
    }
  ];

  componentDidMount() {
    console.log('AppList.componentDidMount');
    // 查询本页面需所有参数
    const { dispatch } = this.props;
    dispatch(APP_INIT({}));
  }

  componentWillUnmount() {
    console.log('AppList WILL UNMOUNT!');
    // 将state里的数据清理一下
    const result = {
      rows: [],
      total: 0,
    }
    const { dispatch } = this.props;
    dispatch(UPDATE_STATE({
      ...result
    }));
  }

    /**
   * 查询满足条件的数据字典
   */
  handleSubmitEx = (record: AppItem) => {
    console.info('AppList.handleSubmitEx');
    console.info(record);
    const queryParam = {
      ...record
    };
    // 查询条件保存起来，方便点击页码时使用
    // setState回调方法，保证先更新state
    this.setState({
      queryParam
    }, () => {
      const { pageSize } = this.props;
      this.fetchAllApp(1, pageSize);
    });
  }

  handleReset = () => {
    // 是否清空表格
  }

  openAddAppModal = () => {
    this.setState({ addModalVisible: true });
  }

  handleAddModalOk = async (record: AppItem) => {
    console.log(record);
    const { dispatch } = this.props;
    console.info(dispatch);
    const res = await dispatch(APP_ADD(record));
    console.info(res);
    if (res) {
      this.setState({
        addModalVisible: false
      });
      const { pageSize } = this.props;
      this.fetchAllApp(1, pageSize);
    }
  }

  handleAddModalCancel = () => {
    this.setState({ addModalVisible: false });
  };

  openUpdateAppModal = (record: AppItem[], flag: string) => {
    // 表格选中内容
    console.info('openUpdateAppModal');
    console.info(record);
    const row = record[0];
    const appFormData = {
      ...row,
      flag
    };
    this.setState({
      appFormData,
      updateModalVisible: true
    });
  }

  /**
   * 更新面板的确定事件
   * @param {*} record 更新表单
   */
  handleUpdateModalOk = async (record: AppItem) => {
    console.log('handleUpdateModalOk');
    console.log(record);
    const { appFormData, flag } = this.state;
    console.info(appFormData);
    if (flag === 'edit') {
      const { dispatch } = this.props;
      const res = await dispatch(APP_UPDATE(record));
      console.info(res);
      if (res) {
        this.setState({
          updateModalVisible: false,
          appFormData: EMPTY
        });
        const { pageSize } = this.props;
        this.fetchAllApp(1, pageSize);
      }
    } else {
      this.setState({
        updateModalVisible: false,
        appFormData: EMPTY
      });
    }
  }

  handleUpdateModalCancel = () => {
    this.setState({
      updateModalVisible: false,
      appFormData: EMPTY
    });
  };

  /**
   * 分页查询系统参数信息
   * @param {*} pageNum 页码
   * @param {*} pageSize 每页记录条数
   */
  fetchAllApp = (pageNum: number, pageSize: number) => {
    const { queryParam } = this.state;
    const { dispatch } = this.props;
    dispatch(APP_LIST({
      ...queryParam,
      pageSize,
      pageNum
    }));
  }

  /**
   * 处理按钮点击回调事件
   * @param {*} payload 数据包
   */
  handleBtnCallBack = (callBackModel: AGridButtonCallBackModel<AppItem>) => {
    console.log('handleBtnCallBack');
    console.log(callBackModel);
    // btn 按钮
    // keys 表格勾选数组
    const { btn } = callBackModel;
    const { alias } = btn;
    // 新增
    if (alias === 'add') {
      this.openAddAppModal();
      return;
    }
    // 修改
    if (alias === 'edit') {
      const { rows } = callBackModel;
      this.openUpdateAppModal(rows, 'edit');
      return;
    }
    // 删除
    if (alias === 'delete') {
      // const { rows } = callBackModel;
      // 调用删除服务，删除勾选数据

      return;
    }
    // 查看
    if (alias === 'view') {
      const { rows } = callBackModel;
      this.openUpdateAppModal(rows, 'view');
      return;
    }
  }

  /**
   * 表格勾选回调函数
   * @param {*} rows 表格选中数据集合
   */
  onSelectRow = (keys: React.Key[], rows: AppItem[]) => {
    this.setState({
      selectedRows: rows,
      selectedRowKeys: keys
    });
  };

  /**
   * 页码改变的回调，参数是改变后的页码及每页条数
   * @param page 改变后的页码，上送服务器端
   * @param pageSize 每页数据条数
   */
  onPageNumAndSizeChange = (page: number, pageSize: number) => {
    console.log(page, pageSize);
    this.fetchAllApp(page, pageSize);
  };

  render() {
    console.info('AppList.render');
    const code = 'app-list';
    const { appList, loading, total, addLoading, updateLoading, systemData, clusterData, statusData } = this.props;
    const { addModalVisible, updateModalVisible, appFormData, flag } = this.state;
    // console.log(appList);
    // console.log(pageSize);
    const rowKey = (record: AppItem) => record.appId;

    return (
      <PageContainer>
        <ProCard title="系统应用查询" headerBordered>
          <AppQueryForm
            colon={false}
            loading={loading}
            onSubmit={this.handleSubmitEx}
            onReset={this.handleReset}
          />
          <AGrid<AppItem>
            code={code}
            btnCallBack={this.handleBtnCallBack}
            columns={this.columns}
            rowKey={rowKey}
            pkField='appId'
            dataSource={appList}
            total={total}
            loading={loading}
            onSelectRow={this.onSelectRow}
            onPageNumAndSizeChange={this.onPageNumAndSizeChange}
          />
        </ProCard>
        {
          !addModalVisible ? null :
          <AddAppModal
            systemData={systemData}
            clusterData={clusterData}
            statusData={statusData}
            colon={false}
            modalTitle='新增系统应用'
            modalWidth={1000}
            modalVisible={addModalVisible}
            loading={addLoading}
            onHandlerOK={this.handleAddModalOk}
            onHandlerCancel={this.handleAddModalCancel}
          />
        }
        {
          !updateModalVisible ? null :
          <UpdateAppModal
            systemData={systemData}
            clusterData={clusterData}
            statusData={statusData}
            colon={false}
            modalTitle=''
            modalWidth={1000}
            modalVisible={updateModalVisible}
            flag={flag}
            loading={updateLoading}
            formData={appFormData}
            onHandlerOK={this.handleUpdateModalOk}
            onHandlerCancel={this.handleUpdateModalCancel}
          />
        }
      </PageContainer>
    );
  }
}