import React from 'react';
import { Modal, Form, Row, Col, Input } from 'antd';
import { FormInstance } from 'antd/es/form/Form';
import ASelect from '@/components/SelfComp/ASelect';
import { formLayout, basicFormItemLangLayout } from '@/utils/commons';
import { FormModalProps } from '@/models/FormModal';
import { DataItem } from '@/models/common';
import { AppItem } from '../data';

type UpdateAppProps<T> = FormModalProps<T> & {
  /** 标识，view-查看，edit-更新 */
  flag: string;
  systemData: DataItem[];
  clusterData: DataItem[];
  statusData: DataItem[];
}

const FormItem = Form.Item;

export default class UpdateAppModal extends React.PureComponent<UpdateAppProps<AppItem>> {
  // useForm 是 React Hooks 的实现，只能用于函数组件
  // formRef = Form.useForm;
  formRef = React.createRef<FormInstance>();

  componentDidMount() {
    console.info('UpdateAppModal.componentDidMount');
  }

  onOk = () => {
    const { formData, flag } = this.props;
    if (flag === 'view') {
      const { onHandlerOK } = this.props;
      if (onHandlerOK) {
        onHandlerOK(formData);
      }
    } else {
      const { appId } = formData;
      this.formRef.current!.validateFields().then(fieldsValue => {
        console.info(fieldsValue);
        const values = {
          ...fieldsValue,
          appId
        };
        console.log('Received values of form: ', values);
        const { onHandlerOK } = this.props;
        if (onHandlerOK) {
          onHandlerOK({
            ...values
          });
        }
      }).catch((err) => console.info('表单校验不通过'));
    }
  };

  onCancel = () => {
    // 先清除form表单
    this.formRef.current!.resetFields();
    const {onHandlerCancel} = this.props;
    if (onHandlerCancel) {
      onHandlerCancel();
    }
  }

  render() {
    console.info("UpdateAppModal.render");
    const { formData, colon, modalWidth, modalVisible, systemData, clusterData, statusData, loading, flag } = this.props;
    console.info(`flag: ${flag}`);
    const formItemLayout = basicFormItemLangLayout;
    const modalTitle = (flag === 'edit' ? '修改系统应用' : '查看系统应用信息');
    // const visible = (flag === 'edit' ? true : false);
    // const disabled = (flag === 'edit' ? false : true);

    return (
      <Modal
        title={modalTitle}
        destroyOnClose
        width={modalWidth}
        open={modalVisible}
        confirmLoading={loading}
        onOk={this.onOk}
        onCancel={this.onCancel}>
        <Form layout={formLayout} ref={this.formRef}>
          <Row>
            <Col span={12}>
              <FormItem label='系统编码' name='systemCode' {...formItemLayout} colon={colon}
                rules={[
                  { required: true, message: '系统编码必输' }
                ]}
                initialValue={formData.systemCode}
              >
                <ASelect dataSource={systemData} disabled/>
              </FormItem>
            </Col>
            <Col span={12}>
              <FormItem label='所在集群' name='cluster' {...formItemLayout} colon={colon}
                rules={[
                  { required: true, message: '所在集群必输' }
                ]}
                initialValue={formData.cluster}
              >
                <ASelect dataSource={clusterData} disabled/>
              </FormItem>
            </Col>
          </Row>
          <Row>
            <Col span={12}>
              <FormItem label='应用编码' name='appCode' {...formItemLayout} colon={colon}
                rules={[
                  { max: 10, message: '应用编码最多允许输入10个字符' },
                  { required: true, message: '应用编码必输' }
                ]}
                initialValue={formData.appCode}
              >
                <Input disabled/>
              </FormItem>
            </Col>
            <Col span={12}>
              <FormItem label='应用名称' name='appName' {...formItemLayout} colon={colon}
                rules={[
                  { max: 50, message: '应用名称最多允许输入50个字符' },
                  { required: true, message: '应用名称必输' }
                ]}
                initialValue={formData.appName}
              >
                <Input/>
              </FormItem>
            </Col>
          </Row>
          <Row>
            <Col span={12}>
              <FormItem label="负责人" name='manager' {...formItemLayout} colon={colon}
                rules={[
                  { max: 16, message: '负责人最多允许输入16个字符' },
                  { required: true, message: '负责人必输' }
                ]}
                initialValue={formData.manager}
              >
                <Input/>
              </FormItem>
            </Col>
            <Col span={12}>
              <FormItem label="状态" name='status' {...formItemLayout} colon={colon}
                rules={[
                  { required: true, message: '状态必输' }
                ]}
                initialValue={formData.status}
              >
                <ASelect dataSource={statusData}/>
              </FormItem>
            </Col>
          </Row>
        </Form>
      </Modal>
    );
  }
}