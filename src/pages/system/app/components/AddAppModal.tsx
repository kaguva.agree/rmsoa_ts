import React from 'react';
import { Modal, Form, Row, Col, Input } from 'antd';
import { FormInstance } from 'antd/es/form/Form';
import ASelect from '@/components/SelfComp/ASelect';
import { formLayout, modalFormItemLayout } from '@/utils/commons';
import { FormModalProps } from '@/models/FormModal';
import { DataItem } from '@/models/common';
import { AppItem } from '../data';

type AddAppProps<T> = FormModalProps<T> & {
  systemData: DataItem[];
  clusterData: DataItem[];
  statusData: DataItem[];
}
type AddAppPropsEx<T> = Omit<AddAppProps<T>, 'formData'>;

const FormItem = Form.Item;

export default class AddAppModal extends React.PureComponent<AddAppPropsEx<AppItem>> {

  formRef = React.createRef<FormInstance>();

  componentDidMount() {
    console.info('AddAppModal.componentDidMount');
  }

  onOk = () => {
    // console.log(this.formRef);
    this.formRef.current!.validateFields().then(fieldsValue => {
      console.info(fieldsValue);
      const values = {
        ...fieldsValue
      };
      console.log('Received values of form: ', values);
      const { onHandlerOK } = this.props;
      if (onHandlerOK) {
        onHandlerOK({
          ...values
        });
      }
    }).catch();
  };

  onCancel = () => {
    // 先清除form表单
    this.formRef.current!.resetFields();
    const { onHandlerCancel } = this.props;
    if (onHandlerCancel) {
      onHandlerCancel();
    }
  }

  render() {
    console.info("AddAppModal.render");
    // const colon = false;
    // const formLayout = 'horizontal'
    const { colon, modalTitle, modalWidth, modalVisible, systemData, clusterData, statusData, loading } = this.props;
    // const form = Form.useForm;
    // const { resetFields } = form;
    const formItemLayout = modalFormItemLayout;

    return (
      <Modal
        title={modalTitle}
        destroyOnClose
        maskClosable={false}
        width={modalWidth}
        open={modalVisible}
        confirmLoading={loading}
        onOk={this.onOk}
        onCancel={this.onCancel}>
        <Form layout={formLayout} ref={this.formRef}>
          <Row>
            <Col span={12}>
              <FormItem label='系统编码' name='systemCode' {...formItemLayout} colon={colon}
                rules={[
                  { required: true, message: '系统编码必输' }
                ]}
              >
                <ASelect dataSource={systemData}/>
              </FormItem>
            </Col>
            <Col span={12}>
              <FormItem label='所在集群' name='cluster' {...formItemLayout} colon={colon}
                rules={[
                  { required: true, message: '所在集群必输' }
                ]}
              >
                <ASelect dataSource={clusterData}/>
              </FormItem>
            </Col>
          </Row>
          <Row>
            <Col span={12}>
              <FormItem label='应用编码' name='appCode' {...formItemLayout} colon={colon}
                rules={[
                  { max: 10, message: '应用编码最多允许输入10个字符' },
                  { required: true, message: '应用编码必输' }
                ]}
              >
                <Input/>
              </FormItem>
            </Col>
            <Col span={12}>
              <FormItem label='应用名称' name='appName' {...formItemLayout} colon={colon}
                rules={[
                  { max: 50, message: '应用名称最多允许输入50个字符' },
                  { required: true, message: '应用名称必输' }
                ]}
              >
                <Input/>
              </FormItem>
            </Col>
          </Row>
          <Row>
            <Col span={12}>
              <FormItem label="负责人" name='manager' {...formItemLayout} colon={colon}
                rules={[
                  { max: 16, message: '负责人最多允许输入16个字符' },
                  { required: true, message: '负责人必输' }
                ]}
              >
                <Input/>
              </FormItem>
            </Col>
            <Col span={12}>
              <FormItem label="状态" name='status' {...formItemLayout} colon={colon}
                rules={[
                  { required: true, message: '状态必输' }
                ]}
                initialValue='0'
              >
                <ASelect dataSource={statusData}/>
              </FormItem>
            </Col>
          </Row>
        </Form>
      </Modal>
    );
  }
}