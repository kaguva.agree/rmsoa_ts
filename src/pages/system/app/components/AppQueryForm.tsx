import React from 'react';
import { Form, Input } from 'antd';
import { basicFormItemLangLayout } from '@/utils/commons';
import QueryFilterForm from '@/components/SelfComp/QueryFilterForm';
import { QueryFormProps } from '@/models/FormModal';

export type AppQueryProps = QueryFormProps & {
}

const FormItem = Form.Item;

class AppQueryForm extends React.PureComponent<AppQueryProps> {

  render() {
    const { colon, loading, onSubmit, onReset } = this.props;
    const formItemLayout = basicFormItemLangLayout;

    return (
      <QueryFilterForm
        colon={colon}
        loading={loading}
        onSubmit={onSubmit}
        onReset={onReset}
      >
        <FormItem label='系统编码' name='systemCode' {...formItemLayout} colon={colon}>
          <Input />
        </FormItem>
        <FormItem label='应用编码' name='appCode' {...formItemLayout} colon={colon}>
          <Input placeholder='支持模糊查询'/>
        </FormItem>
        <FormItem label='所在集群' name='cluster' {...formItemLayout} colon={colon}>
          <Input />
        </FormItem>
        <FormItem label='负责人' name='manager' {...formItemLayout} colon={colon}>
          <Input />
        </FormItem>
      </QueryFilterForm>
    );
  }
}

export default AppQueryForm;