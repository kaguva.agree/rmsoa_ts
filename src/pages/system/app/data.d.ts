/**
 * 应用参数
 */
export interface AppItem {
  appId: string;
  appCode: string;
  appName: string;
  manager: string;
  cluster: string;
  systemCode: string;
  systemName?: string;
  status: string;
}