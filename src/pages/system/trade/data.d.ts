/**
 * 交易
 */
export interface TradeItem {
  /** 交易id */
  tradeId: string;
  /** 交易码 */
  funcCode: string;
  /** 交易码 */
  funcName: string;
  /** 交易码 */
  funcAction: string;
  /** 交易所属子系统 */
  subSystem?: string;
  /** 交易负责人 */
  owner: string;
  /** 交易所属集群 */
  clstCode: string;
  /** 交易所属集群名 */
  clstName: string;
  /** 交易所属应用 */
  appCode: string;
  /** 交易所属应用名 */
  appName: string;
  /** 状态，0-正常，1-失效 */
  status: string;
}