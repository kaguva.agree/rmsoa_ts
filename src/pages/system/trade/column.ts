import type { ColumnsType } from 'antd/es/table';
import { TradeItem } from "./data";

export const tradeListColumns: ColumnsType<TradeItem> = [
  {
    title: '交易功能码',
    dataIndex: 'funcCode',
  },
  {
    title: '交易名称',
    dataIndex: 'funcName',
  },
  {
    title: '交易路径',
    dataIndex: 'funcAction',
  },
  {
    title: '负责人',
    dataIndex: 'owner',
  },
  {
    title: '交易所属集群',
    dataIndex: 'clstCode',
    render: (_, record) => record.clstName,
  },
  {
    title: '交易所属应用',
    dataIndex: 'appCode',
    render: (_, record) => record.appName,
  }
]