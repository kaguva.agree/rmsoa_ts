import React from 'react';
import { Modal, Tree, message } from 'antd';
import type { DataNode, TreeProps } from 'antd/es/tree';
import { connect, Dispatch } from 'umi';
import { PageContainer } from '@ant-design/pro-layout';
import ProCard from '@ant-design/pro-card';
import AGrid, { AGridButtonCallBackModel } from '@/components/SelfComp/AGrid';
import { ConnectState } from '@/models/connect';
import { DataItem } from '@/models/common';
import { TRADE_INIT, TRADE_LIST, TRADE_ADD, TRADE_UPDATE } from '@/actions/trade';
import TradeQueryForm from './components/TradeQueryForm';
// import AddTradeModal from './components/AddTradeModal';
// import UpdateTradeModal from './components/UpdateTradeModal';
import { TradeItem } from './data';
import { tradeListColumns } from './column';
import { AppItem } from '../app/data';
import { getItemValue } from '@/utils/commons';

interface TradeListProps {
  dispatch: Dispatch;
  tradeList: TradeItem[],
  total: number,
  loading: boolean;
  pageSize: number;
  addLoading: boolean;
  updateLoading: boolean;
  systemData: DataItem[];
  clusterData: DataItem[];
  statusData: DataItem[];
  appList: AppItem[];
}

interface TradeListState {
  addModalVisible: boolean;
  tradeFormData: TradeItem;
  updateModalVisible: boolean;
  // 表格选择的数据
  selectedRows: TradeItem[];
  selectedRowKeys: React.Key[];
  queryParam?: TradeItem;
  addFormData: TradeItem;
  flag: 'view' | 'edit' | 'add' | 'delete';
  pageSize: number;
  importModalVisible: false,
  fileList: [],
  expandedKeys: React.Key[],
  autoExpandParent: boolean,
  checkedKeys: React.Key[] | {
    checked: React.Key[];
    halfChecked: React.Key[];
  },
  selectedKeys: React.Key[],
}

const EMPTY: TradeItem = {
  tradeId: '',
  funcCode: '',
  funcName: '',
  funcAction: '',
  subSystem: '',
  owner: '',
  clstCode: '',
  clstName: '',
  appCode: '',
  appName: '',
  status: '',
}

/**
 * 一颗空树
 */
const EMPTY_TREE: DataNode[] = [];

@connect((state: ConnectState) => ({
  dictList: state.trades.rows,
  total: state.trades.total,
  systemData: state.trades.systemData,
  clusterData: state.trades.clusterData,
  statusData: state.trades.statusData,
  appList: state.trades.appList,
  // 当用户 dispatch 对应 effect 时，dva 会自动注入对应 effect 的 loading 状态
  loading: state.loading.effects['trades/fetchAllTrade'],
  pageSize: state.trades.pageSize,
  addLoading: state.loading.effects['trades/addTrade'],
  updateLoading: state.loading.effects['trades/updateTrade'],
}))
export default class TradeList extends React.PureComponent<TradeListProps, TradeListState> {

  state: TradeListState = {
    addModalVisible: false,
    tradeFormData: EMPTY,
    updateModalVisible: false,
    // 表格选择的数据
    selectedRows: [],
    selectedRowKeys: [],
    queryParam: EMPTY,
    addFormData: EMPTY,
    flag: 'edit',
    pageSize: 10,
    importModalVisible: false,
    fileList: [],
    expandedKeys: [],
    autoExpandParent: true,
    checkedKeys: [],
    selectedKeys: [],
  }

  columns = [
    ...tradeListColumns
  ];

  componentDidMount() {
    console.log('TradeList.componentDidMount');
    // 查询本页面需所有参数
    const { dispatch } = this.props;
    dispatch(TRADE_INIT({}));
  }

  /**
   * 查询满足条件的数据字典
   */
  handleSubmitEx = (record: TradeItem) => {
    console.info('TradeList.handleSubmitEx');
    console.info(record);
    const queryParam = {
      ...record
    };
    // 查询条件保存起来，方便点击页码时使用
    // setState回调方法，保证先更新state
    this.setState({
      queryParam
    }, () => {
      const { pageSize } = this.state;
      this.fetchAllTrade(1, pageSize);
    });
  }

  handleReset = () => {
    // 是否清空表格
  }

  openAddTradeModal = (records: TradeItem[] | undefined) => {
    console.info('openAddTradeModal');
    console.info(records);
    if (records) {
      const row = records[0];
      const addFormData = {
        ...row
      };
      this.setState({
        addFormData,
        addModalVisible: true
      });
    } else {
      this.setState({
        addModalVisible: true
      });
    }
  }

  handleAddModalOk = async (record: TradeItem) => {
    console.log(record);
    const { dispatch } = this.props;
    const res = await dispatch(TRADE_ADD(record));
    console.info(res);
    if (res) {
      this.setState({
        addModalVisible: false,
        addFormData: EMPTY
      });
      const { pageSize } = this.state;
      this.fetchAllTrade(1, pageSize);
    }
  }

  handleAddModalCancel = () => {
    this.setState({
      addModalVisible: false,
      addFormData: EMPTY
    });
  };

  openUpdateTradeModal = (record: TradeItem[], flag: string) => {
    // 表格选中内容
    console.info('openUpdateTradeModal');
    console.info(record);
    const row = record[0];
    const tradeFormData = {
      ...row,
    };
    this.setState({
      tradeFormData,
      updateModalVisible: true
    });
  }

  /**
   * 更新面板的确定事件
   * @param {*} record 更新表单
   */
  handleUpdateModalOk = async (record: TradeItem) => {
    console.log('handleUpdateModalOk');
    console.log(record);
    const { tradeFormData, flag } = this.state;
    console.info(tradeFormData);
    if (flag === 'edit') {
      const { dispatch } = this.props;
      const res = await dispatch(TRADE_UPDATE(record));
      console.info(res);
      if (res) {
        this.setState({
          updateModalVisible: false,
          tradeFormData: EMPTY
        });
        const { pageSize } = this.state;
        this.fetchAllTrade(1, pageSize);
      }
    } else {
      this.setState({
        updateModalVisible: false,
        tradeFormData: EMPTY
      });
    }
  }

  handleUpdateModalCancel = () => {
    this.setState({
      updateModalVisible: false,
      tradeFormData: EMPTY
    });
  };

  /**
   * 分页查询系统参数信息
   * @param {*} pageNum 页码
   * @param {*} pageSize 每页记录条数
   */
  fetchAllTrade = (pageNum: number, pageSize: number) => {
    const { queryParam } = this.state;
    const { dispatch } = this.props;
    dispatch(TRADE_LIST({
      ...queryParam,
      pageSize,
      pageNum
    }));
  }

  /**
   * 处理按钮点击回调事件
   * @param {*} payload 数据包
   */
  handleBtnCallBack = (callBackModel: AGridButtonCallBackModel<TradeItem>) => {
    console.log('handleBtnCallBack');
    console.log(callBackModel);
    // btn 按钮
    // keys 表格勾选数组
    const { btn, keys } = callBackModel;
    const { alias } = btn;
    // 新增
    if (alias === 'add') {
      if (keys && keys.length > 1) {
        message.warn('新增时最多只允许选择一条数据当父节点！');
        return;
      }
      // 选中一条，可以将字典名当父节点使用
      if (keys && keys.length === 1) {
        const { rows } = callBackModel;
        this.openAddTradeModal(rows);
        return;
      }
      this.openAddTradeModal(undefined);
      return;
    }
    // 修改
    if (alias === 'edit') {
      const { rows } = callBackModel;
      this.openUpdateTradeModal(rows, 'edit');
      return;
    }
    // 删除
    if (alias === 'delete') {
      // const { rows } = callBackModel;
      // 调用删除服务，删除勾选数据

      return;
    }
    // 查看
    if (alias === 'view') {
      const { rows } = callBackModel;
      this.openUpdateTradeModal(rows, 'view');
      return;
    }
  }

  /**
   * 表格勾选回调函数
   * @param {*} rows 表格选中数据集合
   */
  onSelectRow = (keys: React.Key[], rows: TradeItem[]) => {
    this.setState({
      selectedRows: rows,
      selectedRowKeys: keys
    });
  };

  /**
   * 页码改变的回调，参数是改变后的页码及每页条数
   * @param page 改变后的页码，上送服务器端
   * @param pageSize 每页数据条数
   */
  onPageNumAndSizeChange = (page: number, pageSize: number) => {
    console.log('onPageNumAndSizeChange', page, pageSize);
    this.setState({
      pageSize
    }, () => {
      this.fetchAllTrade(page, pageSize);
    });
  };

  /**
   * 根据服务端返回的系统应用集合，系统集合，集群集合生成树形结构数据
   * @param appList 系统应用集合
   * @param systemData 系统集合
   * @param clusterData 集群集合
   * @returns 树形结构数据
   */
  createTreeData = (appList: AppItem[], systemData: DataItem[], clusterData: DataItem[]): DataNode[] => {
    if (!appList) {
      return EMPTY_TREE;
    }
    const len = appList.length;
    const treeData: DataNode[] = [];
    const keyExists: string[] = [];
    for (let i = 0; i < len; i += 1) {
      const appItem = appList[i];
      // console.info(appItem);
      const { systemCode, cluster, appCode, appName } = appItem;
      const treeNodeSize = treeData.length;
      // console.info('createTreeData', treeNodeSize);
      const keyStr: string = `${systemCode}-${cluster}`;
      const clusterName = getItemValue(clusterData, cluster);
      // 判断系统-集群是否存在
      if (keyExists.indexOf(keyStr) !== -1) {
        // 系统-集群存在
        for (let j = 0; j < treeNodeSize; j += 1) {
          const treeNode = treeData[j];
          const sysKey = treeNode['key'];
          // 找到对应系统节点
          if (sysKey === systemCode) {
            if (!treeNode.children) {
              treeNode.children = [];
            }
            const cluLen = treeNode.children.length;
            for (let k = 0; k < cluLen; k += 1) {
              const cluNode = treeNode.children[k];
              const cluKey = cluNode['key'];
              // 找到对应集群节点
              // 集群节点的key变成了系统编码-集群编码
              if (cluKey === keyStr) {
                // 集群存在
                const appNode = this.createNode(keyStr, appCode, appName, false);
                if (!cluNode.children) {
                  cluNode.children = [];
                }
                cluNode.children.push(appNode);
              }
            }
          }
        }
      } else if (keyExists.indexOf(systemCode) !== -1) {
        // 系统存在，集群不存在
        for (let j = 0; j < treeNodeSize; j += 1) {
          const treeNode = treeData[j];
          const sysKey = treeNode['key'];
          // 找到对应系统节点
          if (sysKey === systemCode) {
            const appNode = this.createNode(keyStr, appCode, appName, false);
            const cluNode = this.createNode(systemCode, cluster, clusterName, true);
            cluNode.children!.push(appNode);
            if (!treeNode.children) {
              treeNode.children = [];
            }
            treeNode.children.push(cluNode);
          }
        }
      } else {
        // 系统不存在
        const systemNode = this.createTreeNode(appItem, systemData, clusterData);
        treeData.push(systemNode);
      }
      keyExists.push(systemCode);
      keyExists.push(keyStr);
    }
    return treeData;
  }

  /**
   * 创建节点
   * @param keyPre key前缀，可以不送
   * @param code code值
   * @param name 名称
   * @param hasSub 是否有子节点
   * @returns 节点
   */
  createNode = (keyPre: string, code: string, name: string, hasSub: boolean) => {
    let node: DataNode;
    if (keyPre) {
      node = {
        key: `${keyPre}-${code}`,
        title: `${code}-${name}`
      }
    } else {
      node = {
        key: `${code}`,
        title: `${code}-${name}`
      }
    }
    if (hasSub) {
      const subNode: DataNode[] = [];
      node.children = subNode;
    }
    return node;
  }

  /**
   * 将应用数据转换成树节点结构
   * @param treeData 树节点集合
   * @param appIiem 应用数据
   */
  createTreeNode = (appIiem: AppItem, systemData: DataItem[], clusterData: DataItem[]) => {
    // console.info('createTreeNode', systemData);
    const { systemCode, cluster } = appIiem;
    // 系统编码是否存在
    const systemName = getItemValue(systemData, systemCode);
    // 集群是否存在
    const clusterName = getItemValue(clusterData, cluster);
    // console.info('createTreeNode', systemName);
    const systemNode = this.createNode('', systemCode, systemName, true);
    const clusterNode = this.createNode(systemCode, cluster, clusterName, true);
    const { appCode, appName } = appIiem;
    const keyPre = `${systemCode}-${cluster}`;
    const appNode = this.createNode(keyPre, appCode, appName, false);
    clusterNode.children!.push(appNode);
    systemNode.children!.push(clusterNode);
    return systemNode;
  }

  onSelect: TreeProps['onSelect'] = (selectedKeys, info) => {
    console.log('selected', selectedKeys, info);
    this.setState({
      selectedKeys
    });
  };

  onCheck: TreeProps['onCheck'] = (checkedKeys, info) => {
    console.log('checked', checkedKeys, info);
    this.setState({
      checkedKeys
    });
  };

  render() {
    console.info('TradeList.render');
    const code = 'trade-list';
    const { tradeList, loading, total, addLoading, updateLoading, systemData, clusterData, statusData, appList } = this.props;
    const { addModalVisible, updateModalVisible, dictFormData, addFormData, flag } = this.state;
    const { autoExpandParent, checkedKeys, selectedKeys } = this.state;
    // 查询有哪些系统，系统下有哪些集群，集群下有哪些应用
    // 系统清单，集群清单，应用清单
    const treeData: DataNode[] = this.createTreeData(appList, systemData, clusterData);
    const rowKey = (record: TradeItem) => record.tradeId;

    return (
      <PageContainer>
        <ProCard split="vertical">
          <ProCard title="项目系统" colSpan="17%" headerBordered>
            <Tree
              checkable
              showLine
              autoExpandParent={autoExpandParent}
              onCheck={this.onCheck}
              checkedKeys={checkedKeys}
              onSelect={this.onSelect}
              selectedKeys={selectedKeys}
              treeData={treeData}
            />
          </ProCard>
          <ProCard title="交易列表" headerBordered>
            <TradeQueryForm
              colon={false}
              loading={loading}
              onSubmit={this.handleSubmitEx}
              onReset={this.handleReset}
            />
            <AGrid
              code={code}
              btnCallBack={this.handleBtnCallBack}
              columns={this.columns}
              rowKey={rowKey}
              pkField='tradeId'
              dataSource={tradeList}
              total={total}
              loading={loading}
              onSelectRow={this.onSelectRow}
              onPageNumAndSizeChange={this.onPageNumAndSizeChange}
            />
          </ProCard>
        </ProCard>
      </PageContainer>
    );
  }

}