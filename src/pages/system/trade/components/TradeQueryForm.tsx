import React from 'react';
import { Form, Input } from 'antd';
import { basicFormItemLangLayout } from '@/utils/commons';
import QueryFilterForm from '@/components/SelfComp/QueryFilterForm';
import { QueryFormProps } from '@/models/FormModal';

export type TradeQueryProps = QueryFormProps & {
}

const FormItem = Form.Item;

/**
 * 交易查询表单
 */
export default class TradeQueryForm extends React.PureComponent<TradeQueryProps> {

  render() {

    const { colon, loading, onSubmit, onReset } = this.props;
    const formItemLayout = basicFormItemLangLayout;

    return (
      <QueryFilterForm
        colon={colon}
        loading={loading}
        onSubmit={onSubmit}
        onReset={onReset}
      >
        <FormItem label='交易功能码' name='funcCode' {...formItemLayout} colon={colon}>
          <Input />
        </FormItem>
        <FormItem label='交易名称' name='funcName' {...formItemLayout} colon={colon}>
          <Input />
        </FormItem>
        <FormItem label="交易负责人" name='owner' {...formItemLayout} colon={colon}>
          <Input />
        </FormItem>
        <FormItem label="交易所属集群" name='clstCode' {...formItemLayout} colon={colon}>
          <Input />
        </FormItem>
        <FormItem label="交易所属应用" name='appCode' {...formItemLayout} colon={colon}>
          <Input />
        </FormItem>
      </QueryFilterForm>
    );
  }
}