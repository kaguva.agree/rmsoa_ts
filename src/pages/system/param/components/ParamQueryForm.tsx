import React from 'react';
import { Button, Form, Col, Input } from 'antd';
import { FormInstance } from 'antd/es/form/Form';
import { basicFormItemLangLayout } from '@/utils/commons';

export interface ParamQueryProps {
  /** 表单提交函数 */
  onSubmit: Function;
  /** 表单重置回调函数 */
  onReset?: Function;
  colon: boolean;
  loading: boolean;
}

const FormItem = Form.Item;
/**
 * 参数查询表单
 */
export default class ParamQueryForm extends React.PureComponent<ParamQueryProps> {

  formRef = React.createRef<FormInstance>();

  /**
   * 表单提交
   */
  handleSubmit = () => {
    // 收集表单数据
    // 传递回父组件
    // 感叹号! 断言
    const fieldsValue = this.formRef.current!.getFieldsValue();
    console.info(fieldsValue);
    const values = {
      paramName: fieldsValue.paramName,
      paramKey: fieldsValue.paramKey,
    };
    console.log('Received values of form: ', values);
    const {onSubmit} = this.props;
    if (onSubmit) {
      onSubmit({
        ...values
      });
    }
  }

  handleReset = () => {
    this.formRef.current!.resetFields();
    const { onReset } = this.props;
    if (onReset) {
      onReset();
    }
  }

  render() {

    const { colon, loading } = this.props;
    const formItemLayout = basicFormItemLangLayout;

    return (
      <Form layout='inline'
        ref={this.formRef}
        onFinish={this.handleSubmit}
        style={{marginBottom: '16px'}}
      >
        <Col span={8}>
          <FormItem label='参数名称' name='paramName' {...formItemLayout} colon={colon}>
            <Input placeholder='支持模糊查询'/>
          </FormItem>
        </Col>
        <Col span={8}>
          <FormItem label='参数键名' name='paramKey' {...formItemLayout} colon={colon}>
            <Input />
          </FormItem>
        </Col>
        <Col span={8}>
          <div style={{ float: 'right' }}>
            <Button type='primary' htmlType='submit' loading={loading}>
              查询
            </Button>
            <Button style={{ marginLeft: 8 }} onClick={this.handleReset}>
              重置
            </Button>
          </div>
        </Col>
      </Form>
    );
  }
}