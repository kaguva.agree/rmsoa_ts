import React from 'react';
import { Modal, Form, Row, Col, Input } from 'antd';
import { FormInstance } from 'antd/es/form/Form';
import { formLayout, modalFormItemLayout, maxModalFormItemLayout } from '@/utils/commons';
import { FormModalProps } from '@/models/FormModal';
import { ParamItem } from '../data';
// import styles from './list.less';

type AddParamProps<T> = FormModalProps<T> & {
}
type AddParamPropsEx<T> = Omit<AddParamProps<T>, 'formData'>;

const FormItem = Form.Item;

/**
 * 新增参数弹窗表单
 */
export default class AddParamModal extends React.PureComponent<AddParamPropsEx<ParamItem>> {
  // useForm 是 React Hooks 的实现，只能用于函数组件
  // formRef = Form.useForm;
  formRef = React.createRef<FormInstance>();

  componentDidMount() {
    console.info('AddParamModal.componentDidMount');
  }

  onOk = () => {
    // console.log(this.formRef);
    this.formRef.current!.validateFields().then(fieldsValue => {
      console.info(fieldsValue);
      const values = {
        ...fieldsValue
      };
      console.log('Received values of form: ', values);
      const { onHandlerOK } = this.props;
      if (onHandlerOK) {
        onHandlerOK({
          ...values
        });
      }
    }).catch();
  };

  onCancel = () => {
    // 先清除form表单
    this.formRef.current!.resetFields();
    const { onHandlerCancel } = this.props;
    if (onHandlerCancel) {
      onHandlerCancel();
    }
  }

  render(){
    console.info("AddParamModal.render");
    // const colon = false;
    // const formLayout = 'horizontal'
    const { colon, modalTitle, modalWidth, modalVisible, loading } = this.props;
    // const form = Form.useForm;
    // const { resetFields } = form;
    const formItemLayout = modalFormItemLayout;

    return(
      <Modal
        title={modalTitle}
        destroyOnClose
        maskClosable={false}
        width={modalWidth}
        open={modalVisible}
        confirmLoading={loading}
        onOk={this.onOk}
        onCancel={this.onCancel}>
        <Form layout={formLayout} ref={this.formRef}>
          <Row>
            <Col span={12}>
              <FormItem label='参数名称' name='paramName' {...formItemLayout} colon={colon}
                rules={[
                  { max: 20, message: '参数名称最多允许输入20个字符' },
                  { required: true, message: '参数名称必输' }
                ]}
              >
                <Input/>
              </FormItem>
            </Col>
          </Row>
          <Row>
            <Col span={12}>
              <FormItem label='参数键名' name='paramKey' {...formItemLayout} colon={colon}
                rules={[
                  { max: 20, message: '参数键名最多允许输入20个字符' },
                  { required: true, message: '参数键名必输' },
                  { pattern: new RegExp(/^[A-Za-z][A-Za-z0-9\\._]{1,19}$/, "g"), message: '参数键名只能以字母开头，由A-Za-z0-9._组成，最大长度为20' }
                ]}
              >
                <Input/>
              </FormItem>
            </Col>
          </Row>
          <Row>
            <Col span={12}>
              <FormItem label='参数键值' name='paramValue' {...formItemLayout} colon={colon}
                rules={[
                  { max: 20, message: '参数键值最多允许输入20个字符' },
                  { required: true, message: '参数键值必输' },
                ]}
              >
                <Input/>
              </FormItem>
            </Col>
          </Row>
          <Row>
            <Col span={24}>
              <FormItem label='参数键值描述' name='paramValueDesc' {...maxModalFormItemLayout} colon={colon}
                rules={[
                  { max: 30, message: '参数键值描述最多允许输入30个字符' },
                  { required: true, message: '参数键值描述必输' }
                ]}
              >
                <Input />
              </FormItem>
            </Col>
          </Row>
        </Form>
      </Modal>
    );
  }
}
