import React from 'react';
import { connect, Dispatch } from 'umi';
import { PageContainer } from '@ant-design/pro-layout';
import ProCard from '@ant-design/pro-card';
import AGrid, { AGridButtonCallBackModel } from '@/components/SelfComp/AGrid';
import { ConnectState } from '@/models/connect';
import { PARAM_LIST, PARAM_ADD, PARAM_UPDATE } from '@/actions/param';
import ParamQueryForm from './components/ParamQueryForm';
import AddParamModal from './components/AddParamModal';
import UpdateParamModal from './components/UpdateParamModal';
import { ParamItem } from './data';

interface ParamListProps {
  dispatch: Dispatch;
  paramList: ParamItem[],
  total: number,
  loading: boolean;
  pageSize: number;
  addLoading: boolean;
  updateLoading: boolean;
}

interface ParamListState {
  addModalVisible: boolean;
  paramFormData: ParamItem;
  updateModalVisible: boolean;
  // 表格选择的数据
  selectedRows: ParamItem[];
  selectedRowKeys: React.Key[];
  queryParam?: ParamItem;
  flag: 'view' | 'edit' | 'add' | 'delete';
}

const EMPTY: ParamItem = {
  paramId: '',
  paramName: '',
  paramKey: '',
  paramValue: '',
  paramValueDesc: '',
  paramStates: ''
}

/**
 * 参数管理，用于配置默认密码，登录允许密码最大错误次数等参数
 */
@connect((state: ConnectState) => ({
  paramList: state.params.rows,
  total: state.params.total,
  loading: state.loading.effects['params/fetchAllParam'],
  pageSize: state.params.pageSize,
  addLoading: state.loading.effects['params/addParam'],
  updateLoading: state.loading.effects['params/updateParam']
}))
export default class ParamList extends React.PureComponent<ParamListProps, ParamListState> {

  state: ParamListState = {
    addModalVisible: false,
    updateModalVisible: false,
    // 表格选择的数据
    selectedRows: [],
    selectedRowKeys: [],
    paramFormData: EMPTY,
    flag: 'edit',
  }

  paramColumns = [
    {
      title: '参数名称',
      dataIndex: 'paramName',
      width: 250,
    },
    {
      title: '参数键名',
      dataIndex: 'paramKey',
      width: 300,
    },
    {
      title: '参数键值',
      dataIndex: 'paramValue',
      width: 300,
    },
    {
      title: '参数键值描述',
      dataIndex: 'paramValueDesc',
    }
  ];

  constructor(props: ParamListProps) {
    super(props);
    console.log('ParamList.constructor');
  }

  componentDidMount() {
    console.info('ParamList.componentDidMount');
    // 默认查询第一页
    const { pageSize } = this.props;
    this.fetchAllParam(1, pageSize);
  }

  /**
   * 打开参数新增窗口
   */
  addParam = () => {
    this.setState({ addModalVisible: true });
  }

  openAddParamModal = () => {
    this.setState({ addModalVisible: true });
  }

  handleAddModalOk = async (record: ParamItem) => {
    console.log(record);
    const { dispatch } = this.props;
    const res = await dispatch(PARAM_ADD(record));
    console.info(res);
    if (res) {
      this.setState({
        addModalVisible: false
      });
      const { pageSize } = this.props;
      this.fetchAllParam(1, pageSize);
    }
  }

  handleAddModalCancel = () => {
    this.setState({ addModalVisible: false });
  };

  /**
   * 修改参数，打开参数修改窗口
   */
  updateParam = (record: ParamItem, flag: string) => {
    // 表格选中内容
    const { paramFormData } = this.state;
    this.setState({
      paramFormData,
      updateModalVisible: true
    });
  }

  openUpdateParamModal = (records: ParamItem[], flag: string) => {
    // 表格选中内容
    console.info('openUpdateParamModal');
    console.info(records);
    const row = records[0];
    const paramFormData = {
      ...row,
      flag
    };
    this.setState({
      paramFormData,
      updateModalVisible: true
    });
  }

  /**
   * 更新面板的确定事件
   * @param {*} record 更新表单
   */
  handleUpdateModalOk = async (record: ParamItem) => {
    console.log('handleUpdateModalOk');
    console.log(record);
    const { paramFormData, flag } = this.state;
    console.info(paramFormData);
    if (flag === 'edit') {
      const { dispatch } = this.props;
      const res = await dispatch(PARAM_UPDATE(record));
      console.info(res);
      if (res) {
        this.setState({
          updateModalVisible: false,
          paramFormData: EMPTY
        });
        const { pageSize } = this.props;
        this.fetchAllParam(1, pageSize);
      }
    } else {
      this.setState({
        updateModalVisible: false,
        paramFormData: EMPTY
      });
    }
  }

  handleUpdateModalCancel = () => {
    this.setState({
      updateModalVisible: false,
      paramFormData: EMPTY
    });
  };

  /**
   * 根据条件分页查询表格数据
   * @param {*} record 条件
   */
  handleFetchParam = (record: ParamItem) => {
    console.log(record);
    const queryParam = {
      ...record
    };
    // 查询条件保存起来，方便点击页码时使用
    // setState回调方法，保证先更新state
    this.setState({
      queryParam
    }, () => {
      const { pageSize } = this.props;
      this.fetchAllParam(1, pageSize);
    });
  }

  /**
   * 页码改变的回调，参数是改变后的页码及每页条数
   * @param page 改变后的页码，上送服务器端
   * @param pageSize 每页数据条数
   */
  onPageNumAndSizeChange = (page: number, pageSize: number) => {
    console.log(page, pageSize);
    this.fetchAllParam(page, pageSize);
  };

  /**
   * 分页查询系统参数信息
   * @param {*} pageNum 页码
   * @param {*} pageSize 每页记录条数
   */
  fetchAllParam = (pageNum: number, pageSize: number) => {
    const { queryParam } = this.state;
    const { dispatch } = this.props;
    dispatch(PARAM_LIST({
      ...queryParam,
      pageSize,
      pageNum
    }));
  }

  /**
   * 处理按钮点击回调事件
   * @param {*} callBacModel 数据包
   */
  handleBtnCallBack = (callBackModel: AGridButtonCallBackModel<ParamItem>) => {
    console.log('handleBtnCallBack');
    console.log(callBackModel);
    // btn 按钮
    // keys 表格勾选数组
    const { btn } = callBackModel;
    const { alias } = btn;
    // 新增
    if (alias === 'add') {
      this.openAddParamModal();
      return;
    }
    // 修改
    if (alias === 'edit') {
      const { rows } = callBackModel;
      this.openUpdateParamModal(rows, 'edit');
      return;
    }
    // 删除
    if (alias === 'delete') {
      // const { rows } = callBackModel;
      // 调用删除服务，删除勾选数据

      return;
    }
    // 查看
    if (alias === 'view') {
      const { rows } = callBackModel;
      this.openUpdateParamModal(rows, 'view');
      return;
    }
  }

  /**
   * 表格勾选回调函数
   * @param {*} rows 表格选中数据集合
   */
  onSelectRow = (keys: React.Key[], rows: ParamItem[]) => {
    this.setState({
      selectedRows: rows,
      selectedRowKeys: keys
    });
  };

  render() {
    const code = 'param-list';
    const { paramList, loading, total, addLoading, updateLoading } = this.props;
    const { addModalVisible, updateModalVisible, paramFormData, flag } = this.state;
    const rowKey = (record: ParamItem) => record.paramId;

    return (
      <PageContainer>
        <ProCard title="参数列表" headerBordered>
          <ParamQueryForm colon={false} onSubmit={this.handleFetchParam} loading={loading} />
          <AGrid<ParamItem>
            code={code}
            btnCallBack={this.handleBtnCallBack}
            columns={this.paramColumns}
            rowKey={rowKey}
            dataSource={paramList}
            total={total}
            loading={loading}
            onSelectRow={this.onSelectRow}
            pkField='paramKey'
            onPageNumAndSizeChange={this.onPageNumAndSizeChange}
          />
        </ProCard>
        {
          !addModalVisible ? null :
          <AddParamModal
            colon={false}
            modalTitle='新增参数'
            modalWidth={1000}
            modalVisible={addModalVisible}
            loading={addLoading}
            onHandlerOK={this.handleAddModalOk}
            onHandlerCancel={this.handleAddModalCancel}
          />
        }
        {
          !updateModalVisible ? null :
          <UpdateParamModal
            colon={false}
            modalTitle=''
            modalWidth={1000}
            modalVisible={updateModalVisible}
            flag={flag}
            loading={updateLoading}
            formData={paramFormData}
            onHandlerOK={this.handleUpdateModalOk}
            onHandlerCancel={this.handleUpdateModalCancel}
          />
        }
      </PageContainer>
    );
  }
}