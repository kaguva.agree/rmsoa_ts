/**
 * 应用参数
 */
export interface DictItem {
  dictId: string;
  dictName: string;
  dictDesc?: string;
  dictItemName: string;
  dictItemDesc?: string;
  status: string;
  editFlag?: string;
}