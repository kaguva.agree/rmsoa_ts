import React from 'react';
import { message, Tag } from 'antd';
import type { ColumnsType } from 'antd/es/table';
import { connect, Dispatch } from 'umi';
import { PageContainer } from '@ant-design/pro-layout';
import ProCard from '@ant-design/pro-card';
import AGrid, { AGridButtonCallBackModel } from '@/components/SelfComp/AGrid';
import { ConnectState } from '@/models/connect';
import { DataItem } from '@/models/common';
import { DICT_LIST, DICT_ADD, DICT_UPDATE, DICT_DELETE, UPDATE_STATE } from '@/actions/dict';
import { getItemValue } from '@/utils/commons';
import DictQueryForm from './components/DictQueryForm';
import AddDictModal from './components/AddDictModal';
import UpdateDictModal from './components/UpdateDictModal';
import { DictItem } from './data';

interface DictListProps {
  dispatch: Dispatch;
  dictList: DictItem[],
  total: number,
  loading: boolean;
  pageSize: number;
  addLoading: boolean;
  updateLoading: boolean;
  editFlagData: DataItem[];
  statusData: DataItem[];
}

interface DictListState {
  addModalVisible: boolean;
  dictFormData: DictItem;
  updateModalVisible: boolean;
  // 表格选择的数据
  selectedRows: DictItem[];
  selectedRowKeys: React.Key[];
  queryParam?: DictItem;
  addFormData: DictItem;
  flag: 'view' | 'edit' | 'add' | 'delete';
  pageSize: number;
}

const EMPTY: DictItem = {
  dictId: '',
  dictName: '',
  dictDesc: '',
  dictItemName: '',
  dictItemDesc: '',
  status: '',
  editFlag: '',
}

@connect((state: ConnectState) => ({
  dictList: state.dicts.rows,
  total: state.dicts.total,
  editFlagData: state.dicts.editFlagData,
  statusData: state.dicts.statusData,
  // 当用户 dispatch 对应 effect 时，dva 会自动注入对应 effect 的 loading 状态
  loading: state.loading.effects['dicts/fetchAllDict'],
  pageSize: state.dicts.pageSize,
  addLoading: state.loading.effects['dicts/addDict'],
  updateLoading: state.loading.effects['dicts/updateDict'],
}))
export default class DictList extends React.PureComponent<DictListProps, DictListState> {

  state: DictListState = {
    addModalVisible: false,
    dictFormData: EMPTY,
    updateModalVisible: false,
    // 表格选择的数据
    selectedRows: [],
    selectedRowKeys: [],
    queryParam: EMPTY,
    addFormData: EMPTY,
    flag: 'edit',
    pageSize: 10
  };

  columns: ColumnsType<DictItem> = [
    {
      title: '字典名',
      dataIndex: 'dictName',
      // width: 150
    },
    {
      title: '字典描述',
      dataIndex: 'dictDesc',
      // width: 150
    },
    {
      title: '字典项',
      dataIndex: 'dictItemName',
      // width: 150
    },
    {
      title: '字典项描述',
      dataIndex: 'dictItemDesc',
      // width: 150
    },
    {
      title: '状态',
      dataIndex: 'status',
      render: (text: string, record: DictItem, index: number) => this.dictStatusFunc(record),
      // width: 70
    },
    {
      title: '是否可编辑',
      dataIndex: 'editFlag',
      // width: 70
    }
  ];

  componentDidMount() {
    console.log('DictList.componentDidMount');
    // 默认查询10条
    // const {pageSize} = this.props;
    // this.props.queryAllDictionary(
    //     {
    //         'pageSize': pageSize,
    //         'pageNum': 1
    //     }
    // );
  }

  componentWillUnmount() {
    console.log('DictList WILL UNMOUNT!');
    // 将state里的数据清理一下
    // const result = {
    //   rows: [],
    //   total: 0,
    // }
    // const { dispatch } = this.props;
    // dispatch(UPDATE_STATE({
    //   ...result
    // }));
  }

  dictStatusFunc = (record: DictItem) => {
    const { statusData } = this.props;
    const { status } = record;
    const value = getItemValue(statusData, status);
    return <Tag color='processing'>{value}</Tag>
  }

  /**
   * 查询满足条件的数据字典
   */
  handleSubmitEx = (record: DictItem) => {
    console.info('DictList.handleSubmitEx');
    console.info(record);
    const queryParam = {
      ...record
    };
    // 查询条件保存起来，方便点击页码时使用
    // setState回调方法，保证先更新state
    this.setState({
      queryParam
    }, () => {
      const { pageSize } = this.state;
      this.fetchAllDict(1, pageSize);
    });
  }

  handleReset = () => {
    // 是否清空表格
  }

  openAddDictModal = (records: DictItem[] | undefined) => {
    console.info('openAddDictModal');
    console.info(records);
    if (records) {
      const row = records[0];
      const addFormData = {
        ...row
      };
      this.setState({
        addFormData,
        addModalVisible: true
      });
    } else {
      this.setState({
        addModalVisible: true
      });
    }
  }

  handleAddModalOk = async (record: DictItem) => {
    console.log(record);
    const { dispatch } = this.props;
    const res = await dispatch(DICT_ADD(record));
    console.info(res);
    if (res) {
      this.setState({
        addModalVisible: false,
        addFormData: EMPTY
      });
      const { pageSize } = this.state;
      this.fetchAllDict(1, pageSize);
    }
  }

  handleAddModalCancel = () => {
    this.setState({
      addModalVisible: false,
      addFormData: EMPTY
    });
  };

  openUpdateDictModal = (record: DictItem[], flag: string) => {
    // 表格选中内容
    console.info('openUpdateDictModal');
    console.info(record);
    const row = record[0];
    const dictFormData = {
      ...row,
      flag
    };
    this.setState({
      dictFormData,
      updateModalVisible: true
    });
  }

  /**
   * 更新面板的确定事件
   * @param {*} record 更新表单
   */
  handleUpdateModalOk = async (record: DictItem) => {
    console.log('handleUpdateModalOk');
    console.log(record);
    const { dictFormData, flag } = this.state;
    console.info(dictFormData);
    if (flag === 'edit') {
      const { dispatch } = this.props;
      const res = await dispatch(DICT_UPDATE(record));
      console.info(res);
      if (res) {
        this.setState({
          updateModalVisible: false,
          dictFormData: EMPTY
        });
        const { pageSize } = this.state;
        this.fetchAllDict(1, pageSize);
      }
    } else {
      this.setState({
        updateModalVisible: false,
        dictFormData: EMPTY
      });
    }
  }

  handleUpdateModalCancel = () => {
    this.setState({
      updateModalVisible: false,
      dictFormData: EMPTY
    });
  };

  /**
   * 根据条件分页查询表格数据
   * @param {*} record 条件
   */
  handleFetchParam = (record: DictItem) => {
    console.log(record);
    const queryParam = {
      ...record
    };
    // 查询条件保存起来，方便点击页码时使用
    // setState回调方法，保证先更新state
    this.setState({
      queryParam
    }, () => {
      const { pageSize } = this.state;
      this.fetchAllDict(1, pageSize);
    });
  }

  /**
   * 分页查询系统参数信息
   * @param {*} pageNum 页码
   * @param {*} pageSize 每页记录条数
   */
  fetchAllDict = (pageNum: number, pageSize: number) => {
    const { queryParam } = this.state;
    const { dispatch } = this.props;
    dispatch(DICT_LIST({
      ...queryParam,
      pageSize,
      pageNum
    }));
  }

  /**
   * 处理按钮点击回调事件
   * @param {*} payload 数据包
   */
  handleBtnCallBack = (callBackModel: AGridButtonCallBackModel<DictItem>) => {
    console.log('handleBtnCallBack');
    console.log(callBackModel);
    // btn 按钮
    // keys 表格勾选数组
    const { btn, keys } = callBackModel;
    const { alias } = btn;
    // 新增
    if (alias === 'add') {
      if (keys && keys.length > 1) {
        message.warn('新增时最多只允许选择一条数据当父节点！');
        return;
      }
      // 选中一条，可以将字典名当父节点使用
      if (keys && keys.length === 1) {
        const { rows } = callBackModel;
        this.openAddDictModal(rows);
        return;
      }
      this.openAddDictModal(undefined);
      return;
    }
    // 修改
    if (alias === 'edit') {
      const { rows } = callBackModel;
      this.openUpdateDictModal(rows, 'edit');
      return;
    }
    // 删除
    if (alias === 'delete') {
      // const { rows } = callBackModel;
      // 调用删除服务，删除勾选数据

      return;
    }
    // 查看
    if (alias === 'view') {
      const { rows } = callBackModel;
      this.openUpdateDictModal(rows, 'view');
      return;
    }
  }

  /**
   * 表格勾选回调函数
   * @param {*} rows 表格选中数据集合
   */
  onSelectRow = (keys: React.Key[], rows: DictItem[]) => {
    this.setState({
      selectedRows: rows,
      selectedRowKeys: keys
    });
  };

  /**
   * 页码改变的回调，参数是改变后的页码及每页条数
   * @param page 改变后的页码，上送服务器端
   * @param pageSize 每页数据条数
   */
  onPageNumAndSizeChange = (page: number, pageSize: number) => {
    console.log('onPageNumAndSizeChange', page, pageSize);
    // const { pageSize } = this.state;
    // if (pageSize === pageSizeEx) {
    //   console.log('每页数据条目无变化，无需通讯');
    //   return;
    // }
    // this.fetchAllDict(page, pageSize);
    this.setState({
      pageSize
    }, () => {
      this.fetchAllDict(page, pageSize);
    });
  };

  render() {

    console.info('DictList.render');
    const code = 'dict-list';
    const { dictList, loading, total, addLoading, updateLoading, editFlagData, statusData } = this.props;
    const { addModalVisible, updateModalVisible, dictFormData, addFormData, flag } = this.state;
    // console.log(dicList);
    // console.log(pageSize);
    const rowKey = (record: DictItem) => record.dictId;

    return (
      <PageContainer>
        <ProCard title="系统字典查询" headerBordered>
          <DictQueryForm
            colon={false}
            loading={loading}
            onSubmit={this.handleSubmitEx}
            onReset={this.handleReset}
          />
          <AGrid
            code={code}
            btnCallBack={this.handleBtnCallBack}
            columns={this.columns}
            rowKey={rowKey}
            pkField='dictId'
            dataSource={dictList}
            total={total}
            loading={loading}
            onSelectRow={this.onSelectRow}
            onPageNumAndSizeChange={this.onPageNumAndSizeChange}
          />
        </ProCard>
        {
          !addModalVisible ? null :
          <AddDictModal
            editFlagData={editFlagData}
            statusData={statusData}
            colon={false}
            modalTitle='新增字典项'
            modalWidth={1000}
            modalVisible={addModalVisible}
            loading={addLoading}
            formData={addFormData}
            onHandlerOK={this.handleAddModalOk}
            onHandlerCancel={this.handleAddModalCancel}
          />
        }
        {
          !updateModalVisible ? null :
          <UpdateDictModal
            editFlagData={editFlagData}
            statusData={statusData}
            colon={false}
            modalTitle=''
            modalWidth={1000}
            modalVisible={updateModalVisible}
            flag={flag}
            loading={updateLoading}
            formData={dictFormData}
            onHandlerOK={this.handleUpdateModalOk}
            onHandlerCancel={this.handleUpdateModalCancel}
          />
        }
      </PageContainer>
    );
  }
}