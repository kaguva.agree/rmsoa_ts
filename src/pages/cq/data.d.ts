import { TradeItem } from '../system/trade/data';
import { Moment } from "moment";

export interface CascaderOption {
  value: string;
  label: string;
  children?: CascaderOption[];
}

/**
 * 需求基础信息
 */
export interface CqBaseInfoItem {
  /** 需求编号 */
  cqCode: string;
  /** 需求名称 */
  cqName: string;
  /** 分配系统 */
  subSystem: string;
  /** 分配集群 */
  clstCode: string;
  /** 分配应用 */
  appCode: string;
  /** 主开人员 */
  auditor: string;
  /** 录入人员 */
  recorder: string;
}

/**
 * 需求信息
 */
export interface CqItem {
  cqId: string;
  cqCode: string;
  cqName: string;
  subSystem: string;
  /** 分配集群 */
  clstCode: string;
  /** 分配应用 */
  appCode: string;
  auditor: string;
  designerA: string;
  status: string;
  prdDate: string;
  devStartDate: string;
  devEndDate: string;
  sitStartDate: string;
  sitEndDate: string;
  uatStartDate: string;
  uatEndDate: string;
}

export interface CqQueryParam {
  cqCode: string;
  cqName: string;
  subSystem: string;
  auditor: string;
  designerA: string;
  status: string;
  prdDate: string;
  devDate: Moment[];
  sitDate: Moment[];
  uatDate: Moment[];
}

/**
 * 需求详细信息
 */
export interface CqDetailItem extends CqItem {
  testFlag?: boolean;
  srdPath?: string;
  odsPath?: string;
  ddsPath?: string;
  oarPath?: string;
  trades: TradeItem[],
  tradeTotal: number;
}