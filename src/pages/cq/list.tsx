import React from 'react';
import { message, Space } from 'antd';
import { connect, Dispatch } from 'umi';
import { PageContainer } from '@ant-design/pro-layout';
import ProCard from '@ant-design/pro-card';
import AGrid from '@/components/SelfComp/AGrid';
import AButton from '@/components/SelfComp/AButton';
import { ConnectState } from '@/models/connect';
import { DataItem } from '@/models/common';
import { getItemValue } from '@/utils/commons';
import { CQ_LIST_INIT, CQ_LIST, CQ_DETAIL, UPDATE_STATE } from '@/actions/cq';
import CqListQueryForm from './components/CqListQueryForm';
import ViewCqDrawer from './components/ViewCqDrawer';
import { CqItem, CqDetailItem } from './data';

interface CqListProps {
  dispatch: Dispatch;
  cqList: CqItem[],
  total: number,
  loading: boolean;
  detailLoading: boolean;
  exportLoading: boolean;
  auditorData: DataItem[],
  designerData: DataItem[],
  systemData: DataItem[];
  cqStatusData: DataItem[];
}

interface CqListState {
  cqFormData: CqDetailItem;
  viewDrawerVisible: boolean;
  // 表格选择的数据
  selectedRows: CqItem[];
  selectedRowKeys: React.Key[];
  queryParam?: CqItem;
  pageSize: number;
}

const EMPTY: CqItem = {
  cqId: '',
  cqCode: '',
  cqName: '',
  subSystem: '',
  clstCode: '',
  appCode: '',
  auditor: '',
  designerA: '',
  status: '',
  prdDate: '',
  devStartDate: '',
  devEndDate: '',
  sitStartDate: '',
  sitEndDate: '',
  uatStartDate: '',
  uatEndDate: '',
}

const EMPTY_DETAIL: CqDetailItem = {
  ...EMPTY,
  trades: [],
  tradeTotal: 0,
}

/**
 * 需求列表，表格展示需求基本信息，可查看需求详细信息
 */
@connect((state: ConnectState) => ({
  cqList: state.cqs.rows,
  total: state.cqs.total,
  pageSize: state.cqs.pageSize,
  auditorData: state.users.auditorData,
  designerData: state.users.designerData,
  systemData: state.apps.systemData,
  cqStatusData: state.systems.cqStatusData,
  // 当用户 dispatch 对应 effect 时，dva 会自动注入对应 effect 的 loading 状态
  loading: state.loading.effects['cqs/fetchAllCq'],
  detailLoading: state.loading.effects['cqs/fetchCqDetail'],
  exportLoading: state.loading.effects['cqs/exportCqList'],
}))
export default class CqList extends React.PureComponent<CqListProps, CqListState> {

  state: CqListState = {
    cqFormData: EMPTY_DETAIL,
    viewDrawerVisible: false,
    // 表格选择的数据
    selectedRows: [],
    selectedRowKeys: [],
    queryParam: EMPTY,
    pageSize: 10,
  }

  columns = [
    {
      title: '需求编号',
      dataIndex: 'cqCode',
      width: 150
    },
    {
      title: '需求名称',
      dataIndex: 'cqName',
      width: 300,
      ellipsis: true
    },
    {
      title: '所属系统',
      dataIndex: 'subSystem',
      width: 120
    },
    {
      title: '所属集群',
      dataIndex: 'clstCode',
      width: 120
    },
    {
      title: '所属应用',
      dataIndex: 'appCode',
      width: 120
    },
    {
      title: '审核人(主开)',
      dataIndex: 'auditor',
      width: 100
    },
    {
      title: '设计人A',
      dataIndex: 'designerA',
      width: 100
    },
    {
      title: '需求状态',
      dataIndex: 'status',
      width: 150,
      render: (text: string, record: CqItem, index: number) => this.cqStatusFunc(record)
    },
    {
      title: '投产时间',
      dataIndex: 'prdDate',
      width: 100,
    },
    {
      title: 'DEV开始时间',
      dataIndex: 'devStartDate',
      width: 120,
    },
    {
      title: 'DEV结束时间',
      dataIndex: 'devEndDate',
      width: 120,
    },
    {
      title: 'SIT开始时间',
      dataIndex: 'sitStartDate',
      width: 120,
    },
    {
      title: 'SIT结束时间',
      dataIndex: 'sitEndDate',
      width: 120,
    },
    {
      title: 'UAT开始时间',
      dataIndex: 'uatStartDate',
      width: 120,
    },
    {
      title: 'UAT结束时间',
      dataIndex: 'uatEndDate',
      width: 120,
    },
  ];

  componentDidMount() {
    console.log('CqList WILL MOUNT!');
    // 查询本页面需所有参数
    const { dispatch } = this.props;
    dispatch(CQ_LIST_INIT({}));
  }

  componentWillUnmount() {
    console.log('CQList WILL UNMOUNT!');
    // 将state里的数据清理一下
    const result = {
      rows: [],
      total: 0,
    }
    const { dispatch } = this.props;
    dispatch(UPDATE_STATE({
      ...result
    }));
  }

  /**
   * 需求状态转换
   */
  cqStatusFunc = (record: CqItem) => {
    const { cqStatusData } = this.props;
    const { status } = record;
    const value = getItemValue(cqStatusData, status);
    return value;
  }

  /**
   * 分页查询需求信息
   * @param {*} pageNum 页码
   * @param {*} pageSize 每页记录条数
   */
  fetchAllCq = (pageNum: number, pageSize: number) => {
    const { queryParam } = this.state;
    const { dispatch } = this.props;
    dispatch(CQ_LIST({
      ...queryParam,
      pageSize,
      pageNum
    }));
  }

  /**
   * 表单提交
   */
  handleSubmitEx = (record: CqItem) => {
    console.info('TaskList.handleSubmitEx');
    console.info(record);
    const queryParam = {
      ...record
    };
    // 查询条件保存起来，方便点击页码时使用
    // setState回调方法，保证先更新state
    this.setState({
      queryParam
    }, () => {
      const { pageSize } = this.state;
      this.fetchAllCq(1, pageSize);
    });
  }

  /**
   * 根据查询条件导出表格数据
   * @param {*} record 查询表单
   */
  handleExportEx = () => {
    // 先判断表格是否有数据
    const { cqList } = this.props;
    const size = cqList.length;
    if (size === 0) {
      message.warn('请先查询数据，在导出！');
      return;
    }
    const { total } = this.props;
    if (total === 5000) {
      message.warning('导出数据总条数超过5000，请增加查询条件！');
      return;
    }
    // 下载文件
    message.success('导出成功');
  }

  openViewCqDrawer = async () => {
    // 表格选中内容
    console.info('openViewCqDrawer');
    const { selectedRowKeys } = this.state;
    if (selectedRowKeys.length === 0) {
      message.warn('请先选择一条数据!');
      return;
    }
    if (selectedRowKeys.length > 1) {
      message.warn('只能选择一条数据!');
      return;
    }
    const cqId = selectedRowKeys[0];
    const { dispatch } = this.props;
    const res = await dispatch(CQ_DETAIL({
      cqId
    }));
    console.info(res);
    if (res) {
      const { result } = res;
      console.info(result);
      const cqFormData = {
        ...result
      }
      this.setState({
        cqFormData,
        viewDrawerVisible: true,
      });
    }
  }

  closeViewDrawer = () => {
    this.setState({
      viewDrawerVisible: false,
    });
  }

  renderLeftButton = () => {
    const { exportLoading, detailLoading } = this.props;
    return (
      <>
        <AButton type='primary' code='view' pageCode='cq-list' name='查看详细' onClick={this.openViewCqDrawer} loading={detailLoading} />
        <AButton type='primary' code='export' pageCode='cq-list' name='导出' onClick={this.handleExportEx} loading={exportLoading} />
      </>
    );
  }

  /**
   * 表格勾选回调函数
   * @param {*} rows 表格选中数据集合
   */
  onSelectRow = (keys: React.Key[], rows: CqItem[]) => {
    this.setState({
      selectedRows: rows,
      selectedRowKeys: keys
    });
  };

  /**
   * 页码改变的回调，参数是改变后的页码及每页条数
   * @param page 改变后的页码，上送服务器端
   * @param pageSize 每页数据条数
   */
  onPageNumAndSizeChange = (page: number, pageSize: number) => {
    console.log(page, pageSize);
    // 页码需要保存起来
    this.setState({
      pageSize
    }, () => {
      this.fetchAllCq(page, pageSize);
    });
  };

  render() {
    console.info('CqList.render');
    const code = 'cq-list';
    const { cqList, loading, total, auditorData, designerData, systemData, cqStatusData } = this.props;
    const { viewDrawerVisible, cqFormData } = this.state;
    const rowKey = (record: CqItem) => record.cqId;
    const pkField = 'cqId';

    return (
      <PageContainer>
        <Space direction='vertical' size='middle' style={{ display: 'flex' }}>
          <ProCard title='查询条件' headerBordered >
            <CqListQueryForm
              colon={false}
              loading={loading}
              auditorData={auditorData}
              designerData={designerData}
              systemData={systemData}
              cqStatusData={cqStatusData}
              onSubmit={this.handleSubmitEx}
            />
          </ProCard>
          <ProCard>
            <AGrid
              code={code}
              noActionColumn={true}
              renderLeftButton={this.renderLeftButton}
              columns={this.columns}
              rowKey={rowKey}
              pkField={pkField}
              dataSource={cqList}
              loading={loading}
              total={total}
              scroll={{x:2300}}
              onSelectRow={this.onSelectRow}
              onPageNumAndSizeChange={this.onPageNumAndSizeChange}
            />
          </ProCard>
        </Space>
        {
          !viewDrawerVisible ? null :
          <ViewCqDrawer
            drawerVisible={viewDrawerVisible}
            drawerTitle='需求详情'
            drawerWidth={1000}
            colon={false}
            loading={false}
            formData={cqFormData}
            cqStatusData={cqStatusData}
            onHandlerCancel={this.closeViewDrawer}
          />
        }
      </PageContainer>
    );
  }
}