import React from 'react';
import { Form, Input, DatePicker } from 'antd';
import moment from 'moment';
import ASelect from '@/components/SelfComp/ASelect';
import QueryFilterForm from '@/components/SelfComp/QueryFilterForm';
import { basicFormItemLangLayout } from '@/utils/commons';
import { QueryFormProps } from '@/models/FormModal';
import { DataItem } from '@/models/common';
import { CqQueryParam } from '../data';

export type CqListQueryProps = QueryFormProps & {
  auditorData: DataItem[],
  designerData: DataItem[],
  systemData: DataItem[];
  cqStatusData: DataItem[];
}

const FormItem = Form.Item;
const { RangePicker } = DatePicker;
const dateFormat: string = 'YYYY-MM-DD';

class CqListQueryForm extends React.PureComponent<CqListQueryProps> {

  handleSubmit = (record: CqQueryParam) => {
    const { devDate, ...res } = record
    let startDate = '';
    let endDate = '';
    if (devDate) {
      startDate = devDate[0].format(dateFormat).replaceAll('-', '');
      endDate = devDate[1].format(dateFormat).replaceAll('-', '');
    }
    const newRecord = {
      startDate,
      endDate,
      ...res
    }
    const { onSubmit } = this.props;
    onSubmit({
      ...newRecord
    });
  }

  render() {
    const { colon, loading, onReset } = this.props;
    const { systemData, auditorData, designerData, cqStatusData } = this.props;
    const formItemLayout = basicFormItemLangLayout;

    return (
      <QueryFilterForm
        colon={colon}
        loading={loading}
        onSubmit={this.handleSubmit}
        onReset={onReset}
      >
        <FormItem label="需求编号" name='cqCode' {...formItemLayout} colon={colon}>
          <Input />
        </FormItem>
        <FormItem label="需求名称" name='cqName' {...formItemLayout} colon={colon}>
          <Input />
        </FormItem>
        <FormItem label="所属系统" name='subSystem' {...formItemLayout} colon={colon}>
          <ASelect dataSource={systemData} />
        </FormItem>
        <FormItem label="审核人(主开)" name='auditor' {...formItemLayout} colon={colon}>
          <ASelect dataSource={auditorData} />
        </FormItem>
        <FormItem label="设计人员" name='designerA' {...formItemLayout} colon={colon}>
          <ASelect dataSource={designerData} />
        </FormItem>
        <FormItem label="需求状态" name='status' {...formItemLayout} colon={colon}>
          <ASelect dataSource={cqStatusData} />
        </FormItem>
        <FormItem label="投产时间" name='prdDate' {...formItemLayout} colon={colon}>
          <DatePicker format={dateFormat} style={{ width: '100%' }} />
        </FormItem>
        <FormItem label="DEV起止时间" name='devDate' {...formItemLayout} colon={colon}>
          <RangePicker format={dateFormat} style={{ width: '100%' }}/>
        </FormItem>
        <FormItem label="SIT起止时间" name='sitDate' {...formItemLayout} colon={colon}>
          <RangePicker format={dateFormat} style={{ width: '100%' }}/>
        </FormItem>
        <FormItem label="UAT起止时间" name='uatDate' {...formItemLayout} colon={colon}>
          <RangePicker format={dateFormat} style={{ width: '100%' }}/>
        </FormItem>
      </QueryFilterForm>
    );
  }
}

export default CqListQueryForm;