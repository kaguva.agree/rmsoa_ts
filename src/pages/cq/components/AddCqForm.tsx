import React from 'react';
import { Form, Row, Col, Input, Button } from 'antd';
import { FormInstance } from 'antd/es/form/Form';
import ASelect from '@/components/SelfComp/ASelect';
import { formLayout, basicFormItemLayout, basicFormItemLangLayout } from '@/utils/commons'
import { FormProps } from '@/models/FormModal';
import { DataItem } from '@/models/common';
import { CqBaseInfoItem } from '../data';

type AddCqProps<T> = FormProps<T> & {
  auditorData: DataItem[];
  systemData: DataItem[];
}
type AddCqPropsEx<T> = Omit<AddCqProps<T>, 'formData'>;

const FormItem = Form.Item;

export default class AddCqForm extends React.PureComponent<AddCqPropsEx<CqBaseInfoItem>> {

  formRef = React.createRef<FormInstance>();

  componentDidMount() {
    console.info('AddCqForm.componentDidMount');
  }

  handleSubmit = () => {
    // console.log(this.formRef);
    this.formRef.current!.validateFields().then(fieldsValue => {
      console.info(fieldsValue);
      const values = {
        ...fieldsValue
      };
      console.log('Received values of form: ', values);
      const { onSubmit } = this.props;
      if (onSubmit) {
        onSubmit({
          ...values
        });
      }
    }).catch();
  };

  handleReset = () => {
    // 先清除form表单
    this.formRef.current!.resetFields();
    const { onCancel } = this.props;
    if (onCancel) {
      onCancel();
    }
  }

  render() {
    console.info("AddCqForm.render");
    const { colon, loading, auditorData, systemData } = this.props;
    // 8-8布局
    const formItemLayout = basicFormItemLayout;
    // 8-16布局
    const formItemLangLayout = basicFormItemLangLayout;

    return (
      <Form layout={formLayout} ref={this.formRef} onFinish={this.handleSubmit}>
        <Row>
          <Col span={12}>
            <FormItem label="需求编号" name='cqCode' {...formItemLayout} colon={colon}
              rules={[
                { required: true, message: '需求编号必输' },
                /**
                { pattern: new RegExp(/^[A-Za-z][A-Za-z0-9]{16,16}$/, "g"), message: '需求编号只能以字母开头，16位的A-Za-z0-9组成' }
                */
              ]}>
              <Input placeholder='必须为16位长度，只包含A-Za-z0-9'/>
            </FormItem>
          </Col>
        </Row>
        <Row>
          <Col span={12}>
            <FormItem label="需求名称" name='cqName' {...formItemLangLayout} colon={colon}
              rules={[
                { required: true, message: '需求名称必输' }
              ]}>
              <Input />
            </FormItem>
          </Col>
        </Row>
        <Row>
          <Col span={12}>
            <FormItem label="所属系统" name='subSystem' {...formItemLayout} colon={colon}
              rules={[
                { required: true, message: '所属系统必输' },
              ]}>
              <ASelect dataSource={systemData} />
            </FormItem>
          </Col>
        </Row>
        <Row>
          <Col span={12}>
            <FormItem label="审核人(主开)" name='auditor' {...formItemLayout} colon={colon}
              rules={[
                { required: true, message: '审核人(主开)必输' },
              ]}>
              <ASelect dataSource={auditorData} />
            </FormItem>
          </Col>
        </Row>
        <Row justify='center'>
          <Col>
            <FormItem>
              <Button type="primary" loading={loading} htmlType="submit">
                提交
              </Button>
              <Button style={{ marginLeft: 8 }} onClick={this.handleReset}>重置</Button>
            </FormItem>
          </Col>
        </Row>
      </Form>
    );
  }
}