import React from 'react';
import { Drawer, Descriptions, Steps, Popover, Card } from 'antd';
import { FormInstance } from 'antd/es/form/Form';
import { GridContent } from '@ant-design/pro-layout';
import { getItemValue } from '@/utils/commons';
import { FormDrawerProps } from '@/models/FormModal';
import { DataItem, FlagEnum } from '@/models/common';
import { dateFormat } from '@/utils/utils';
import { CqDetailItem } from '../data';
import AGrid from '@/components/SelfComp/AGrid';
import { TradeItem } from '@/pages/system/trade/data';

type ViewCqProps<T> = FormDrawerProps<T> & {
  flag?: FlagEnum
  cqStatusData: DataItem[];
}

export default class ViewCqDrawer extends React.PureComponent<ViewCqProps<CqDetailItem>> {

  formRef = React.createRef<FormInstance>();

  columns = [
    {
      title: '交易功能码',
      dataIndex: 'funcCode',
    },
    {
      title: '交易名称',
      dataIndex: 'funcName',
    },
    {
      title: '负责人',
      dataIndex: 'owner',
    },
    {
      title: '交易所属集群',
      dataIndex: 'clstCode',
    },
    {
      title: '交易所属应用',
      dataIndex: 'appCode',
    }
  ]

  componentDidMount() {
    console.info('ViewCqDrawer.componentDidMount');
  }

  onClose = () => {
    // 先清除form表单
    const {onHandlerCancel} = this.props;
    if (onHandlerCancel) {
      onHandlerCancel();
    }
  }

  /**
   * 表格勾选回调函数
   * @param {*} rows 表格选中数据集合
   */
  onSelectRow = (keys: React.Key[], rows: TradeItem[]) => {
    // 暂无作用
  };

  /**
   * 页码改变的回调，参数是改变后的页码及每页条数
   * @param page 改变后的页码，上送服务器端
   * @param pageSize 每页数据条数
   */
  onPageNumAndSizeChange = (page: number, pageSize: number) => {
    console.log(page, pageSize);
  };

  render() {
    console.info("ViewCqDrawer.render");
    const { formData, drawerWidth, drawerVisible, flag, cqStatusData } = this.props;
    const { status = '', trades, tradeTotal, testFlag } = formData;
    const cqStatus = getItemValue(cqStatusData, status);
    const cqStatusStr = `${status}-${cqStatus}`;
    const testFlagStr = !testFlag ? '非配合测试' : '配合测试';
    console.info(`flag: ${flag}`);
    const code = 'cq-view';
    const loading = false;
    const rowKey = (record: TradeItem) => record.tradeId;
    const pkField = 'tradeId';
    const customDot = (
      dot: React.ReactNode,
      {
        status,
      }: {
        status: string;
      },
    ) => {
      if (status === 'process') {
        return (
          <Popover placement="topLeft" arrowPointAtCenter content={'111'}>
            {dot}
          </Popover>
        );
      }
      return dot;
    };

    return (
      <Drawer
        open={drawerVisible}
        title="需求详情"
        placement="right"
        width={drawerWidth}
        onClose={this.onClose}
      >
        <GridContent>
          <Card title='流程进度' style={{ marginBottom: 16, backgroundColor: '#C5EBC9' }}>
            <Steps
              direction={'horizontal'}
              progressDot={customDot}
              current={1}
            >
              <Steps.Step title="等待主开录入" description={"desc1"} />
              <Steps.Step title="测试确认" description={"desc2"} />
              <Steps.Step title="主开审查" />
              <Steps.Step title="设计确认" />
              <Steps.Step title="完成" />
            </Steps>
          </Card>
          <Card title='需求基本信息' style={{ marginBottom: 16, backgroundColor: '#C5EBC9' }}>
            <Descriptions>
              <Descriptions.Item label="需求编号">{formData.cqCode}</Descriptions.Item>
              <Descriptions.Item label="需求名称" span={2}>{formData.cqName}</Descriptions.Item>
              <Descriptions.Item label="所属系统">{formData.subSystem}</Descriptions.Item>
              <Descriptions.Item label="所属集群">{formData.clstCode}</Descriptions.Item>
              <Descriptions.Item label="所属应用">{formData.appCode}</Descriptions.Item>
              <Descriptions.Item label="是否配合测试">{testFlagStr}</Descriptions.Item>
              <Descriptions.Item label="主开人员">{formData.auditor}</Descriptions.Item>
              <Descriptions.Item label="设计人员">{formData.designerA}</Descriptions.Item>
              <Descriptions.Item label="需求状态">{cqStatusStr}</Descriptions.Item>
              <Descriptions.Item label="投产日期">{dateFormat(formData.prdDate)}</Descriptions.Item>
            </Descriptions>
          </Card>
          <Card title='需求日期信息' style={{ marginBottom: 16, backgroundColor: '#C5EBC9' }}>
            <Descriptions column={2}>
              <Descriptions.Item label="DEV开始时间">{dateFormat(formData.devStartDate)}</Descriptions.Item>
              <Descriptions.Item label="DEV结束时间">{dateFormat(formData.devEndDate)}</Descriptions.Item>
              <Descriptions.Item label="SIT开始时间">{dateFormat(formData.sitStartDate)}</Descriptions.Item>
              <Descriptions.Item label="SIT结束时间">{dateFormat(formData.sitEndDate)}</Descriptions.Item>
              <Descriptions.Item label="UAT开始时间">{dateFormat(formData.uatStartDate)}</Descriptions.Item>
              <Descriptions.Item label="UAT结束时间">{dateFormat(formData.uatEndDate)}</Descriptions.Item>
            </Descriptions>
          </Card>
          <Card title='需求文档信息' style={{ marginBottom: 16, backgroundColor: '#C5EBC9' }}>
            <Descriptions column={1}>
              <Descriptions.Item label="需求文件路径">{formData.srdPath}</Descriptions.Item>
              <Descriptions.Item label="概设文件路径">{formData.odsPath}</Descriptions.Item>
              <Descriptions.Item label="详设文件路径">{formData.ddsPath}</Descriptions.Item>
              <Descriptions.Item label="接口分析结果路径">{formData.oarPath}</Descriptions.Item>
            </Descriptions>
          </Card>
          <Card title='需求交易列表' style={{ marginBottom: 16, backgroundColor: '#C5EBC9' }}>
            <AGrid
              code={code}
              noActionColumn={true}
              columns={this.columns}
              rowKey={rowKey}
              pkField={pkField}
              dataSource={trades}
              loading={loading}
              total={tradeTotal}
              scroll={{x:1000}}
              onSelectRow={this.onSelectRow}
              onPageNumAndSizeChange={this.onPageNumAndSizeChange}
            />
          </Card>
        </GridContent>
      </Drawer>
    );
  }

}