import React from 'react';
import { connect, Dispatch } from 'umi';
import { Form, Row, Col, Input, Button, Cascader } from 'antd';
import { FormInstance } from 'antd/es/form/Form';
import { PageContainer } from '@ant-design/pro-layout';
import ProCard from '@ant-design/pro-card';
import { ConnectState } from '@/models/connect';
import ASelect from '@/components/SelfComp/ASelect';
import { formLayout, basicFormItemLayout, basicFormItemLangLayout } from '@/utils/commons';
import { getCurrentLoginUser } from '@/utils/authority';
import { DataItem } from '@/models/common';
import { CQ_ADD_INIT, CQ_ADD_BASE } from '@/actions/cq';
import { CascaderOption, CqBaseInfoItem } from './data';

interface CqAddProps {
  dispatch: Dispatch;
  addLoading: boolean;
  auditorData: DataItem[];
  systemData: DataItem[];
}

const FormItem = Form.Item;

/**
 * 录入需求基本信息
 */
@connect((state: ConnectState) => ({
  auditorData: state.users.auditorData,
  systemData: state.systems.systemData,
  addLoading: state.loading.effects['cqs/addCqBaseInfo'],
}))
export default class CqAdd extends React.PureComponent<CqAddProps> {

  formRef = React.createRef<FormInstance>();

  componentDidMount() {
    console.log('CQAdd WILL MOUNT!');
    // 获取所属系统
    // 获取审核人列表
    const { dispatch } = this.props;
    dispatch(CQ_ADD_INIT({}));
  }

  handleSubmit = async () => {
    // console.log(this.formRef);
    this.formRef.current!.validateFields().then(fieldsValue => {
      console.info(fieldsValue);
      const { system, ...res } = fieldsValue;
      const [ subSystem, clstCode, appCode ] = system;
      const values = {
        ...res,
        subSystem,
        clstCode,
        appCode
      };
      console.log('Received values of form: ', values);
      this.addCqBaseInfo(values);
    }).catch();
  };

  addCqBaseInfo = async (record: CqBaseInfoItem) => {
    // 与服务端通讯
    const { dispatch } = this.props;
    // 获取当前登录用户，作为录入人员
    const userCode = getCurrentLoginUser();
    const res = await dispatch(CQ_ADD_BASE({
      ...record,
      recorder: userCode
    }));
    if (res) {
      // 是否重置表单
      this.formRef.current!.resetFields();
    }
  }

  handleReset = () => {
    // 先清除form表单
    this.formRef.current!.resetFields();
  }

  render() {
    console.info('CQAdd.render');
    const { addLoading, auditorData } = this.props;
    const colon = false;
    // 8-8布局
    const formItemLayout = basicFormItemLayout;
    // 8-16布局
    const formItemLangLayout = basicFormItemLangLayout;
    const options: CascaderOption[] = [
      {
        value: 'GBSP',
        label: '通用业务服务平台',
        children: [
          {
            value: 'JRZF',
            label: '三方支付',
            children: [
              {
                value: 'VAFCOS',
                label: 'CROS业务',
              }
            ],
          },
          {
            value: 'SJTD',
            label: '运营管理',
            children: [
              {
                value: 'SYSYTD',
                label: '预填单业务',
              }
            ],
          },
        ],
      },
      {
        value: 'AFA',
        label: '中间业务系统',
        children: [
          {
            value: 'DLAP',
            label: '代理业务',
            children: [
              {
                value: 'ABOP',
                label: '批量业务',
              }
            ],
          },
          {
            value: 'ZFAP',
            label: '支付业务',
            children: [
              {
                value: 'BEPS',
                label: '小额支付',
              },
              {
                value: 'HVPS',
                label: '大额支付',
              }
            ],
          },
        ],
      },
    ];

    return (
      <PageContainer>
        <ProCard title='需求基本信息'>
          <Form layout={formLayout} ref={this.formRef} onFinish={this.handleSubmit}>
            <Row>
              <Col span={12}>
                <FormItem label="需求编号" name='cqCode' {...formItemLayout} colon={colon}
                  rules={[
                    { required: true, message: '需求编号必输' },
                    /**
                    { pattern: new RegExp(/^[A-Za-z][A-Za-z0-9]{16,16}$/, "g"), message: '需求编号只能以字母开头，16位的A-Za-z0-9组成' }
                    */
                  ]}>
                  <Input placeholder='必须为16位长度，只包含A-Za-z0-9'/>
                </FormItem>
              </Col>
            </Row>
            <Row>
              <Col span={12}>
                <FormItem label="需求名称" name='cqName' {...formItemLangLayout} colon={colon}
                  rules={[
                    { required: true, message: '需求名称必输' }
                  ]}>
                  <Input />
                </FormItem>
              </Col>
            </Row>
            <Row>
              <Col span={12}>
                <FormItem label="所属系统应用" name='system' {...formItemLangLayout} colon={colon}
                  rules={[
                    { required: true, message: '所属系统应用必输' },
                  ]}>
                  <Cascader options={options} />
                </FormItem>
              </Col>
            </Row>
            <Row>
              <Col span={12}>
                <FormItem label="审核人(主开)" name='auditor' {...formItemLayout} colon={colon}
                  rules={[
                    { required: true, message: '审核人(主开)必输' },
                  ]}>
                  <ASelect dataSource={auditorData} />
                </FormItem>
              </Col>
            </Row>
            <Row justify='center'>
              <Col>
                <FormItem>
                  <Button type="primary" loading={addLoading} htmlType="submit">
                    提交
                  </Button>
                  <Button style={{ marginLeft: 8 }} onClick={this.handleReset}>重置</Button>
                </FormItem>
              </Col>
            </Row>
          </Form>
        </ProCard>
      </PageContainer>
    );
  }
}