import React from 'react';
import { Form, Input, DatePicker } from 'antd';
import moment from 'moment';
import ASelect from '@/components/SelfComp/ASelect';
import QueryFilterForm from '@/components/SelfComp/QueryFilterForm';
import { basicFormItemLangLayout } from '@/utils/commons';
import { QueryFormProps } from '@/models/FormModal';
import { DataItem } from '@/models/common';
import { TaskQueryParam } from '../data';

export type TaskListQueryProps = QueryFormProps & {
  developerData: DataItem[];
  testerData: DataItem[];
  taskTypeData: DataItem[];
  taskStatusData: DataItem[];
  systemData: DataItem[];
}

const FormItem = Form.Item;
const { RangePicker } = DatePicker;
const dateFormat: string = 'YYYY-MM-DD';

class TaskListQueryForm extends React.PureComponent<TaskListQueryProps> {

  handleSubmit = (record: TaskQueryParam) => {
    const { taskDate, ...res } = record
    let startDate = '';
    let endDate = '';
    if (taskDate) {
      startDate = taskDate[0].format(dateFormat).replaceAll('-', '');
      endDate = taskDate[1].format(dateFormat).replaceAll('-', '');
    }
    const newRecord = {
      startDate,
      endDate,
      ...res
    }
    const { onSubmit } = this.props;
    onSubmit({
      ...newRecord
    });
  }

  render() {
    const { colon, loading, onReset } = this.props;
    const { developerData, testerData, taskTypeData, taskStatusData, systemData } = this.props;
    const formItemLayout = basicFormItemLangLayout;

    return (
      <QueryFilterForm
        colon={colon}
        loading={loading}
        onSubmit={this.handleSubmit}
        onReset={onReset}
      >
        <FormItem label="任务起止时间" name='taskDate' {...formItemLayout} colon={colon}
          rules={[
            { required: true, message: '任务起止时间必输' },
          ]}
          initialValue={[moment('2019-01-01', dateFormat), moment('2019-04-01', dateFormat)]}
        >
          <RangePicker
            format={dateFormat} style={{ width: '100%' }}
          />
        </FormItem>
        <FormItem label="需求编号" name='cqCode' {...formItemLayout} colon={colon}>
          <Input placeholder='支持模糊查询' />
        </FormItem>
        <FormItem label="交易功能码" name='funcCode' {...formItemLayout} colon={colon}>
          <Input />
        </FormItem>
        <FormItem label="开发人员" name='developer' {...formItemLayout} colon={colon}>
          <ASelect dataSource={developerData} />
        </FormItem>
        <FormItem label="测试人员" name='tester' {...formItemLayout} colon={colon}>
          <ASelect dataSource={testerData} />
        </FormItem>
        <FormItem label="任务类型" name='taskType' {...formItemLayout} colon={colon}>
          <ASelect dataSource={taskTypeData} />
        </FormItem>
        <FormItem label="任务状态" name='status' {...formItemLayout} colon={colon}>
          <ASelect dataSource={taskStatusData} />
        </FormItem>
        <FormItem label="所属系统" name='subSystem' {...formItemLayout} colon={colon}>
          <ASelect dataSource={systemData} />
        </FormItem>
      </QueryFilterForm>
    );
  }
}

export default TaskListQueryForm;