import React from 'react';
import { Drawer, Descriptions, Steps, Popover, Card } from 'antd';
import { FormInstance } from 'antd/es/form/Form';
import { GridContent } from '@ant-design/pro-layout';
import { getItemValue } from '@/utils/commons';
import { FormDrawerProps } from '@/models/FormModal';
import { DataItem, FlagEnum } from '@/models/common';
import { dateFormat } from '@/utils/utils';
import { TaskDetailItem } from '../data';

type ViewTaskProps<T> = FormDrawerProps<T> & {
  flag?: FlagEnum
  taskStatusData: DataItem[];
}

export default class ViewTaskDrawer extends React.PureComponent<ViewTaskProps<TaskDetailItem>> {

  formRef = React.createRef<FormInstance>();

  componentDidMount() {
    console.info('ViewTaskDrawer.componentDidMount');
  }

  onClose = () => {
    // 先清除form表单
    const {onHandlerCancel} = this.props;
    if (onHandlerCancel) {
      onHandlerCancel();
    }
  }

  render() {
    console.info("ViewTaskDrawer.render");
    const { formData, drawerWidth, drawerVisible, flag, taskStatusData } = this.props;
    const { startDate, endDate, status } = formData;
    const taskStatus = getItemValue(taskStatusData, status);
    const taskStatusStr = `${status}-${taskStatus}`;
    const systemStr = `${formData.subSystem}-${formData.systemName}`;
    const clstStr = `${formData.clstCode}-${formData.clstName}`;
    const appStr = `${formData.appCode}-${formData.appName}`;
    console.info(`flag: ${flag}`);
    const customDot = (
      dot: React.ReactNode,
      {
        status,
      }: {
        status: string;
      },
    ) => {
      if (status === 'process') {
        return (
          <Popover placement="topLeft" arrowPointAtCenter content={'111'}>
            {dot}
          </Popover>
        );
      }
      return dot;
    };

    return (
      <Drawer
        open={drawerVisible}
        title="任务详情"
        placement="right"
        width={drawerWidth}
        onClose={this.onClose}
      >
        <GridContent>
          <Card title='任务信息' style={{ marginBottom: 16 }}>
            <Descriptions>
              <Descriptions.Item label="任务名称">{formData.taskName}</Descriptions.Item>
              <Descriptions.Item label="任务状态">{taskStatusStr}</Descriptions.Item>
              <Descriptions.Item label="开发人员">{formData.developer}</Descriptions.Item>
              <Descriptions.Item label="测试人员">{formData.tester}</Descriptions.Item>
              <Descriptions.Item label="主开人员">{formData.auditor}</Descriptions.Item>
              <Descriptions.Item label="设计人员">{formData.designer}</Descriptions.Item>
              <Descriptions.Item label="开始时间">{dateFormat(startDate)}</Descriptions.Item>
              <Descriptions.Item label="结束时间">{dateFormat(endDate)}</Descriptions.Item>
              <Descriptions.Item label="备注">{formData.comment}</Descriptions.Item>
            </Descriptions>
          </Card>
          <Card title='交易信息' style={{ marginBottom: 16 }}>
            <Descriptions>
              <Descriptions.Item label="交易功能码">{formData.funcCode}</Descriptions.Item>
              <Descriptions.Item label="交易名称">{formData.funcName}</Descriptions.Item>
              <Descriptions.Item label="所属系统">{systemStr}</Descriptions.Item>
              <Descriptions.Item label="所属集群">{clstStr}</Descriptions.Item>
              <Descriptions.Item label="所属应用">{appStr}</Descriptions.Item>
            </Descriptions>
          </Card>
          <Card title='需求信息' style={{ marginBottom: 16 }}>
            <Descriptions>
              <Descriptions.Item label="需求编号">{formData.cqCode}</Descriptions.Item>
              <Descriptions.Item label="需求名称">{formData.cqName}</Descriptions.Item>
            </Descriptions>
          </Card>
          <Card title='流程进度' style={{ marginBottom: 16 }}>
            <Steps
              direction={'horizontal'}
              progressDot={customDot}
              current={1}
            >
              <Steps.Step title="开发确认" description={"desc1"} />
              <Steps.Step title="测试确认" description={"desc2"} />
              <Steps.Step title="主开审查" />
              <Steps.Step title="设计确认" />
              <Steps.Step title="完成" />
            </Steps>
          </Card>
        </GridContent>
      </Drawer>
    );
  }

}