import { Moment } from "moment";

/**
 * 任务，需求任务，交易任务
 */
export interface TaskItem {
  cqId: string;
  cqCode: string;
  taskId: string;
  taskName: string;
  funcCode: string;
  startDate: string;
  endDate: string;
  developer: string;
  tester: string;
  status: string;
  subSystem: string;
  comment: string;
  actInstId: string;
  actTaskId: string;
}

export interface TaskQueryParam {
  cqCode: string;
  funcCode: string;
  taskDate: Moment[];
  developer: string;
  tester: string;
  taskStatus: string;
  subSystem: string;
}

export interface TaskDetailItem extends TaskItem {
  /** 需求名称 */
  cqName: string;
  /** 交易名称 */
  funcName: string;
  /** 系统名称 */
  systemName: string;
  /** 交易所属应用 */
  appCode: string;
  /** 应用名称 */
  appName: string;
  /** 交易所属集群 */
  clstCode: string;
  /** 集群名称 */
  clstName: string;
  /** 需求主开负责人 */
  auditor: string;
  /** 需求设计人员 */
  designer: string;
  /** 流程进度 */
  taskComments?: TaskComment[]
}

export interface TaskComment {
  /** 处理人 */
  userCode: string;
  /** 处理时间 */
	time: string;
  /** 附言 */
	comment: string;
}