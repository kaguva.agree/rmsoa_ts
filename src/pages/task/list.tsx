import React from 'react';
import { message, Space } from 'antd';
import { connect, Dispatch } from 'umi';
import { PageContainer } from '@ant-design/pro-layout';
import ProCard from '@ant-design/pro-card';
import AGrid from '@/components/SelfComp/AGrid';
import AButton from '@/components/SelfComp/AButton';
import { ConnectState } from '@/models/connect';
import { DataItem } from '@/models/common';
import { getItemValue } from '@/utils/commons';
import { dateFormat } from '@/utils/utils';
import { TASK_INIT, TASK_LIST, TASK_DETAIL, UPDATE_STATE } from '@/actions/task';
import TaskListQueryForm from './components/TaskListQueryForm';
import ViewTaskDrawer from './components/ViewTaskDrawer';
import { TaskDetailItem, TaskItem } from './data';
import styles from './list.less';

interface TaskListProps {
  dispatch: Dispatch;
  taskList: TaskItem[],
  total: number,
  loading: boolean;
  detailLoading: boolean;
  exportLoading: boolean;
  developerData: DataItem[];
  testerData: DataItem[];
  taskTypeData: DataItem[];
  taskStatusData: DataItem[];
  systemData: DataItem[];
}

interface TaskListState {
  taskFormData: TaskDetailItem;
  viewDrawerVisible: boolean;
  // 表格选择的数据
  selectedRows: TaskItem[];
  selectedRowKeys: React.Key[];
  queryParam?: TaskItem;
  pageSize: number;
}

const EMPTY: TaskItem = {
  cqId: '',
  cqCode: '',
  taskId: '',
  taskName: '',
  funcCode: '',
  startDate: '',
  endDate: '',
  developer: '',
  tester: '',
  status: '',
  subSystem: '',
  comment: '',
  actInstId: '',
  actTaskId: '',
}

const EMPTY_DETAIL: TaskDetailItem = {
  ...EMPTY,
  cqName: '',
  funcName: '',
  systemName: '',
  appCode: '',
  appName: '',
  clstCode: '',
  clstName: '',
  auditor: '',
  designer: ''
}

@connect((state: ConnectState) => ({
  taskList: state.tasks.rows,
  total: state.tasks.total,
  pageSize: state.tasks.pageSize,
  developerData: state.users.developerData,
  testerData: state.users.testerData,
  taskTypeData: state.systems.taskTypeData,
  taskStatusData: state.systems.taskStatusData,
  systemData: state.systems.systemData,
  loading: state.loading.effects['tasks/fetchAllTask'],
  detailLoading: state.loading.effects['tasks/fetchTaskDetail'],
  exportLoading: state.loading.effects['users/fetchRolesByUser'],
}))
export default class TaskList extends React.PureComponent<TaskListProps, TaskListState> {

  state: TaskListState = {
    taskFormData: EMPTY_DETAIL,
    viewDrawerVisible: false,
    // 表格选择的数据
    selectedRows: [],
    selectedRowKeys: [],
    queryParam: EMPTY,
    pageSize: 10,
  }

  columns = [
    {
      title: '任务名称',
      dataIndex: 'taskName',
      width: 150
    },
    {
      title: '需求编号',
      dataIndex: 'cqCode',
      width: 200
    },
    {
      title: '交易功能码',
      dataIndex: 'funcCode',
      width: 120
    },
    {
      title: '开始时间',
      dataIndex: 'startDate',
      width: 120,
      render: (text: string, record: TaskItem, index: number) => this.startDateFormat(record)
    },
    {
      title: '结束时间',
      dataIndex: 'endDate',
      width: 120,
      render: (text: string, record: TaskItem, index: number) => this.startDateFormat(record)
    },
    {
      title: '开发人员',
      dataIndex: 'developer',
      width: 120,
    },
    {
      title: '测试人员',
      dataIndex: 'tester',
      width: 120,
    },
    {
      title: '任务状态',
      dataIndex: 'status',
      width: 120,
      render: (text: string, record: TaskItem, index: number) => this.commonStatusFunc(record)
    }
  ];

  /**
   * 初始化时，查询开发人员，测试人员
   */
  componentDidMount() {
    console.info('TaskList.componentDidMount');
    const { dispatch } = this.props;
    dispatch(TASK_INIT({}));
  }

  componentWillUnmount() {
    console.log('TaskList WILL UNMOUNT!');
    // 将state里的数据清理一下
    const result = {
      rows: [],
      total: 0,
      // selectedRowKeys: [],
    }
    const { dispatch } = this.props;
    dispatch(UPDATE_STATE({
      ...result
    }));
  }

  /**
   * 表格中任务状态转换
   */
  commonStatusFunc = (record: TaskItem) => {
    const { taskName } = record;
    // 没有任务名，则为CQ任务
    if (!taskName) {
      return this.cqTaskStatusFunc(record.status);
    }
    // 否则为交易开发任务
    return this.tradeTaskStatusFunc(record.status);
  }

  /**
   * 需求任务状态转换
   */
  cqTaskStatusFunc = (status: string) => {
    if (status === "1") {
        return "主开录入详细信息";
    }
    if (status === "2") {
        return "设计录入文档信息";
    }
    if (status === "3") {
        return "主开分配交易";
    }
    if (status === "4") {
        return "任务完成";
    }
    return status;
  }

  /**
   * 交易任务状态转换
   */
  tradeTaskStatusFunc = (status: string) => {
    const { taskStatusData } = this.props;
    const value = getItemValue(taskStatusData, status);
    return value;
  }

  startDateFormat = (record: TaskItem) => {
    const { startDate } = record;
    // 20190101 --> 2019-01-01
    return dateFormat(startDate);
  }

  endDateFormat = (record: TaskItem) => {
    const { endDate } = record;
    // 20190101 --> 2019-01-01
    return dateFormat(endDate);
  }

  /**
   * 分页查询系统参数信息
   * @param {*} pageNum 页码
   * @param {*} pageSize 每页记录条数
   */
  fetchAllTask = (pageNum: number, pageSize: number) => {
    const { queryParam } = this.state;
    const { dispatch } = this.props;
    dispatch(TASK_LIST({
      ...queryParam,
      pageSize,
      pageNum
    }));
  }

  /**
   * 表单提交
   */
  handleSubmitEx = (record: TaskItem) => {
    console.info('TaskList.handleSubmitEx');
    console.info(record);
    const queryParam = {
      ...record
    };
    // 查询条件保存起来，方便点击页码时使用
    // setState回调方法，保证先更新state
    this.setState({
      queryParam
    }, () => {
      const { pageSize } = this.state;
      this.fetchAllTask(1, pageSize);
    });
  }

  /**
   * 根据查询条件导出表格数据
   * @param {*} record 查询表单
   */
  handleExportEx = () => {
    // 先判断表格是否有数据
    const { taskList } = this.props;
    const size = taskList.length;
    if (size === 0) {
      message.warn('请先查询数据，在导出！');
      return;
    }
    const { total } = this.props;
    if (total === 5000) {
      message.warning('导出数据总条数超过5000，请增加查询条件！');
      return;
    }
    // 下载文件
    message.success('导出成功');
  }

  /**
   * 页码改变的回调，参数是改变后的页码及每页条数
   * @param page 改变后的页码，上送服务器端
   * @param pageSize 每页数据条数
   */
  onPageNumAndSizeChange = (page: number, pageSize: number) => {
    console.log(page, pageSize);
    this.setState({
      pageSize
    }, () => {
      this.fetchAllTask(page, pageSize);
    });
  };

  setRowClassName = (record: TaskItem, index: number) => {
      // console.log(`第${index}行，数据为${JSON.stringify(record)}`);
      const now = new Date().getTime();
      const { endDate } = record;
      const tmp = new Date(endDate).getTime();
      // console.log(`${now},${tmp}`);
      // 任务结束时间在今天之前就变红，警示
      if (now > tmp) {
          return styles.clickRowStyl;
      }
      return '';
  }

  openViewTaskDrawer = async () => {
    // 表格选中内容
    console.info('openViewTaskDrawer');
    const { selectedRowKeys } = this.state;
    if (selectedRowKeys.length === 0) {
      message.warn('请先选择一条数据!');
      return;
    }
    if (selectedRowKeys.length > 1) {
      message.warn('只能选择一条数据!');
      return;
    }
    const taskId = selectedRowKeys[0];
    // console.info('taskId', taskId);
    const { dispatch } = this.props;
    const res = await dispatch(TASK_DETAIL({
      taskId
    }));
    if (res) {
      // const { selectedRows } = this.state;
      // const row = selectedRows[0];
      const { result } = res;
      const taskFormData = {
        ...result
      }
      this.setState({
        taskFormData,
        viewDrawerVisible: true,
      });
    }
  }

  closeViewDrawer = () => {
    this.setState({
      viewDrawerVisible: false,
    });
  }

  renderLeftButton = () => {
    const { exportLoading, detailLoading } = this.props;
    return (
      <>
        <AButton type='primary' code='view' pageCode='task-list' name='查看详细' onClick={this.openViewTaskDrawer} loading={detailLoading} />
        <AButton type='primary' code='export' pageCode='task-list' name='导出' onClick={this.handleExportEx} loading={exportLoading} />
      </>
    );
  }

  /**
   * 表格勾选回调函数
   * @param {*} rows 表格选中数据集合
   */
  onSelectRow = (keys: React.Key[], rows: TaskItem[]) => {
    this.setState({
      selectedRows: rows,
      selectedRowKeys: keys
    });
  };

  render() {
    const code = 'task-list';
    const { taskList } = this.props;
    const { loading, total } = this.props;
    const { developerData, testerData, taskTypeData, taskStatusData, systemData } = this.props;
    const { viewDrawerVisible, taskFormData } = this.state;

    const rowKey = (record: TaskItem) => record.taskId;
    const pkField = 'taskId';

    return (
      <PageContainer>
        <Space direction='vertical' size='middle' style={{ display: 'flex' }}>
          <ProCard title='查询条件' headerBordered >
            <TaskListQueryForm
              colon={false}
              loading={loading}
              developerData={developerData}
              testerData={testerData}
              systemData={systemData}
              taskTypeData={taskTypeData}
              taskStatusData={taskStatusData}
              onSubmit={this.handleSubmitEx}
            />
          </ProCard>
          <ProCard>
            <AGrid
              code={code}
              noActionColumn={true}
              renderLeftButton={this.renderLeftButton}
              columns={this.columns}
              rowKey={rowKey}
              pkField={pkField}
              dataSource={taskList}
              loading={loading}
              total={total}
              onSelectRow={this.onSelectRow}
              onPageNumAndSizeChange={this.onPageNumAndSizeChange}
              rowClassName={this.setRowClassName}
            />
          </ProCard>
        </Space>
        {
          !viewDrawerVisible ? null :
          <ViewTaskDrawer
            drawerVisible={viewDrawerVisible}
            drawerTitle='任务详情'
            drawerWidth={1000}
            colon={false}
            loading={false}
            formData={taskFormData}
            taskStatusData={taskStatusData}
            onHandlerCancel={this.closeViewDrawer}
          />
        }
      </PageContainer>
    );
  }
}