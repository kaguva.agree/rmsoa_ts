export interface VisitDataType {
  x: string;
  y: number;
}

export interface SearchDataType {
  index: number;
  keyword: string;
  count: number;
  range: number;
  status: number;
}

export interface OfflineDataType {
  name: string;
  cvr: number;
}

export interface OfflineChartData {
  x: any;
  y1: number;
  y2: number;
}

export interface RadarData {
  name: string;
  label: string;
  value: number;
}

export interface AnalysisData {
  overviewDatas: ProjectOverviewDataType[];
  statisticsDatas: ProjectStatisticsDataType[];
}

export type DateTypeEnum = 'today' | 'week' | 'month' | 'year';

/** 项目总览 */
export interface ProjectOverviewDataType {
  /** 系统编码 */
  systemCode: string;
  /** 今日提测需求 */
  todaySitCqNum: number;
  /** 本周提测需求 */
  thisWeekSitCqNum: number;
  /** 下周提测需求 */
  nextWeekSitCqNum: number;
  /** 10天后提测需求 */
  tenDaysSitCqNum: number;
  /** 今日投产需求 */
  todayPrdCqNum: number;
  /** 本周投产需求 */
  thisWeekPrdCqNum: number;
  /** 下周投产需求 */
  nextWeekPrdCqNum: number;
  /** 10天后投产需求 */
  tenDaysPrdCqNum: number;
  /** 今日需求待处理任务 */
  todayUndoCqTaskNum: number;
  /** 本周需求待处理任务 */
  thisWeekUndoCqTaskNum: number;
  /** 下周需求待处理任务 */
  nextWeekUndoCqTaskNum: number;
  /** 已过期需求待处理任务 */
  expiredUndoCqTaskNum: number;
  /** 今日交易待处理任务 */
  todayUndoTradeTaskNum: number;
  /** 本周交易待处理任务 */
  thisWeekUndoTradeTaskNum: number;
  /** 下周交易待处理任务 */
  nextWeekUndoTradeTaskNum: number;
  /** 已过期交易待处理任务 */
  expiredUndoTradeTaskNum: number;
}

export interface BarDataItem {
  x: string;
  y: number;
}

export interface PieDataItem {
  type: string;
  value: number;
}

export interface ProjectStatisticPieDataType {
  /** 投产日期 */
  lastWillDate: string;
  /** 数据类别，需求任务-cqTasks，交易任务-tradeTasks */
  kind: string;
  total: number
  /** 饼状图数据 */
  data: PieDataItem[];
}

export interface RadarDataItem {
  /** 数据分组 */
  group: string;
  /** 数据类别，需求任务-cqTasks，交易任务-tradeTasks */
  kind: string;
  /** 数据 */
  value: number;
}

export interface ProjectOverviewRadarDataType {
  /** 投产日期 */
  lastWillPrdDate: string;
  /** 投产需求总数 */
  total: number;
  /** 雷达图数据 */
  data: RadarDataItem[];
}

export interface ProjectStatisticPieDataType {
  /** 投产日期 */
  lastWillDate: string;
  /** 数据类别，需求任务-cqTasks，交易任务-tradeTasks */
  kind: string;
  total: number
  /** 饼状图数据 */
  data: PieDataItem[];
}

/** 排名数据项类型 */
export interface RankingDataItem {
  /** 排名显示标题 */
  title: string;
  /** 排名数量 */
  total: number;
  tip?: string;
}

/**
 * 项目统计数据类型
 * 1-任务数量
 * 2-任务排名
 */
export interface ProjectStatisticsDataType {
  /** 数据类别，需求任务-cqTasks，交易任务-tradeTasks */
  kind: string;
  /** 柱状图数据 */
  barDatas: BarDataItem[];
  /** 排名数据 */
  rankingDatas: RankingDataItem[]
}

export interface ProjectWorkloadDataType {
  /** 数据类别，需求工作量-workload */
  kind: string;
  /** 柱状图数据 */
  barDatas: BarDataItem[];
  /** 排名数据 */
  rankingDatas: RankingDataItem[]
}

/**
 * 项目统计数据类型
 * 1-任务数量
 * 2-任务排名
 */
export interface ProjectStatisticDataType {
  /** 数据类别，需求任务-cqTasks，交易任务-tradeTasks */
  kind: string;
  /** 日期 */
  date: string;
  /** 统计数据 */
  count: number;
}

export interface ProjectRankingDataType {
  /** 数据类别，需求任务-cqTasks，交易任务-tradeTasks */
  kind: string;
  /** 排名数据 */
  rankingData: RankingDataItem[]
}

export interface UndoTaskType {
  taskId: string;
	taskName: string;
	taskCreateTime: string;
	status: string;
	actInstId: string;
	actTaskId: string;
}

export interface CqUndoTaskType extends UndoTaskType {
  cqId: string;
  cqCode: string;
  cqName: string;
  subSystem: string;
  clstCode: string;
  appCode: string;
}

export interface CqUndoTaskRecord extends CqUndoTaskType {
  userCode: string;
}

export interface TradeUndoTaskType extends UndoTaskType {
  taskType: string;
  funcCode: string;
  cqCode: string;
  startDate: string;
  endDate: string;
  developer: string;
  tester: string;
  comment: string;
}

export interface TradeTaskConfirmItem {
  taskId: string;
  status: string;
  taskName: string;
  developer: string;
  tester: string;
  comment: string;
  startDate: string;
  endDate: string;
  actInstId: string;
	actTaskId: string;
  hisList: TradeTaskCommentItem[];
}

export interface TradeTaskCommentItem {
  userCode: string;
  time: string;
  comment: string;
}

/**
 * 交易任务处理提交对象
 */
export interface TradeTaskHandleRecord {
  taskId: string;
  status: string;
  /** 任务处理意见 */
  confirmMsg: string;
  /** 审批操作，通过还是驳回，true表示通过，false表示驳回 */
  agree: boolean;
  actTaskId: string;
  actInstId: string;
}