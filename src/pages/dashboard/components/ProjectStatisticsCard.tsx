import React from 'react';
import { Card, Col, DatePicker, Empty, Row, Tabs } from 'antd';
import { RangePickerProps } from 'antd/es/date-picker/generatePicker';
import { Column } from '@ant-design/plots';
import moment from 'moment';
import numeral from 'numeral';
import { DateTypeEnum, ProjectStatisticsDataType, BarDataItem, RankingDataItem } from '../data.d';
import styles from '../style.less';
import ASelect from '@/components/SelfComp/ASelect';
import { DataItem } from '@/models/common';

const { RangePicker } = DatePicker;

const systemMap: Object = new Object();
systemMap['GBSP'] = '通用业务服务平台';
systemMap['AFA'] = '中间业务系统';
systemMap['TELLER'] = '综合柜面系统';

const systems: DataItem[] = [
  {
    key: 'GBSP',
    value: '通用业务服务平台'
  },
  {
    key: 'AFA',
    value: '中间业务系统'
  },
  {
    key: 'TELLER',
    value: '综合柜面系统'
  }
]

const rankingListData: RankingDataItem[] = [];
for (let i = 0; i < 10; i += 1) {
  rankingListData.push({
    title: `工专路 ${i} 号店`,
    total: 323234,
  });
}

const statisticsKindMap: Object = new Object();
statisticsKindMap['cqTasks'] = '需求任务';
statisticsKindMap['tradeTasks'] = '交易任务';
statisticsKindMap['workload'] = '工作量';

type RangePickerValue = RangePickerProps<moment.Moment>['value'];

interface ProjectStatisticsCardProps {
  kind: string;
  /** 日期范围 */
  rangePickerValue: RangePickerValue;
  isActive: (key: DateTypeEnum) => string;
  loading: boolean;
  /** 日期范围变化函数，这里用作父组件重新查询柱状图数据 */
  handleRangePickerChange: (dates: RangePickerValue, dateStrings: [string, string]) => void;
  selectDate: (key: DateTypeEnum) => void;
  statisticsDatas: ProjectStatisticsDataType[];
}

export default class ProjectStatisticsCard extends React.PureComponent<ProjectStatisticsCardProps> {

  createTabItem = (label: string, barDatas: BarDataItem[], rankingDatas: RankingDataItem[]) => {
    const barTitle = `${label}数量统计`;
    const rankingTitle = `小组${label}排名`;
    const config = {
      title: barTitle,
      data: barDatas,
      xField: 'x',
      yField: 'y',
      seriesField: 'systemCode',
      colorField: 'systemCode',
      group: true,
      label: {
        text: 'y',
        textBaseline: 'bottom',
        position: 'inside',
      }
    };
    return (
      <Row>
        <Col xl={16} lg={12} md={12} sm={24} xs={24}>
          <div className={styles.salesBar}>
            <Column {...config} />
          </div>
        </Col>
        <Col xl={8} lg={12} md={12} sm={24} xs={24}>
          <div className={styles.salesRank}>
            <h4 className={styles.rankingTitle}>
              {rankingTitle}
            </h4>
            <ul className={styles.rankingList}>
              {rankingDatas.map((item, i) => (
                <li key={item.title}>
                  <span className={`${styles.rankingItemNumber} ${i < 3 ? styles.active : ''}`}>
                    {i + 1}
                  </span>
                  <span className={styles.rankingItemTitle} title={item.title}>
                    {item.title}
                  </span>
                  <span className={styles.rankingItemValue}>
                    {numeral(item.total).format('0,0')}
                  </span>
                </li>
              ))}
            </ul>
          </div>
        </Col>
      </Row>
    );
  }

  render() {
    const { rangePickerValue,
      isActive,
      handleRangePickerChange,
      loading,
      selectDate, statisticsDatas, kind } = this.props;
    const items: any[] = [];
    // const items = statisticsDatas.map((item: ProjectStatisticsDataType) => {
    //   const { kind: kindEx, barDatas, rankingDatas } = item;
    //   if (kindEx === kind) {
    //     const label = statisticsKindMap[kind];
    //     return {
    //       label,
    //       key: kind,
    //       children: this.createTabItem(label, barDatas, rankingDatas)
    //     }
    //   }
    // });
    statisticsDatas.forEach((item: ProjectStatisticsDataType) => {
      const { kind: kindEx, barDatas, rankingDatas } = item;
      if (kindEx === kind) {
        const label = statisticsKindMap[kindEx];
        items.push({
          label,
          key: kind,
          children: this.createTabItem(label, barDatas, rankingDatas)
        });
      }
    });
    if (!items || items.length == 0) {
      items.push({
        label: '暂无数据',
        key: 'emptyData',
        children: <Empty/>
      });
    }

    return (
      <Card loading={loading} bordered={false} bodyStyle={{ padding: 0 }}>
        <div className={styles.salesCard}>
          <Tabs
            tabBarExtraContent={
              <div className={styles.salesExtraWrap}>
                <div className={styles.salesExtra}>
                  <ASelect dataSource={systems} style={{width: '200px'}} />
                  <a className={isActive('week')} onClick={() => selectDate('week')}>
                    本周
                  </a>
                  <a className={isActive('month')} onClick={() => selectDate('month')}>
                    本月
                  </a>
                  <a className={isActive('year')} onClick={() => selectDate('year')}>
                    本年
                  </a>
                </div>
                <RangePicker
                  value={rangePickerValue}
                  onChange={handleRangePickerChange}
                  style={{ width: 256 }}
                />
              </div>
            }
            tabBarStyle={{ marginBottom: 24 }}
            items={items}
          />
        </div>
      </Card>
    );
  }
}