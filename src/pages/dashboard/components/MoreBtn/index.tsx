import React from 'react';
import { Space, Dropdown  } from 'antd';
import type { MenuProps } from 'antd';
import { DownOutlined } from '@ant-design/icons';
import { DataItem } from '@/models/common';

interface MoreBtnProps<RecordType> {
  record: RecordType;
  current: string;
  menuItems: DataItem[];
  menuItemClick: (key: string, record: RecordType) => void;
}

export default class MoreBtn<RecordType> extends React.PureComponent<MoreBtnProps<RecordType>> {

  render() {
    const { current, menuItems } = this.props;
    const items: MenuProps['items'] = [];
    if (menuItems && menuItems.length > 0) {
      menuItems.forEach((menuItem: DataItem) => {
        const { key, value } = menuItem;
        if (current == key) {
          items.push({
            key,
            label: value
          });
        }
      });
    }
    if (items.length === 0) {
      items.push({
        key: 'error',
        label: '数据异常',
        disabled: true,
      });
    }

    const onClick: MenuProps['onClick'] = ({ key }) => {
      console.info(key);
      const { record, menuItemClick } = this.props;
      if (menuItemClick) {
        menuItemClick(key, record);
      }
    };
    return (
      <Dropdown menu={{items, onClick}}
      >
        <a>
          <Space>
            操作
            <DownOutlined />
          </Space>
        </a>
      </Dropdown>
    );
  }
}