import React from 'react';
import { ProjectStatisticPieDataType } from '../data.d';
import { Pie } from '@ant-design/plots';

const systemMap: Object = new Object();
systemMap['GBSP'] = '通用业务服务平台';
systemMap['AFA'] = '中间业务系统';
systemMap['TELLER'] = '综合柜面系统';

interface ProjectOverviewCardProps {
  loading: boolean;
  overviewDatas: ProjectStatisticPieDataType;
}

interface ProjectOverviewCardState {
  currentTabKey: string;
}

export default class ProjectOverviewPieCard extends React.PureComponent<ProjectOverviewCardProps, ProjectOverviewCardState> {

  state: ProjectOverviewCardState = {
    currentTabKey: 'GBSP',
  };

  render() {
    const { overviewDatas = {total: 0, data: []} } = this.props;
    const { total, data } = overviewDatas;
    const config = {
      data,
      height: 300,
      angleField: 'value',
      colorField: 'type',
      paddingRight: 80,
      innerRadius: 0.6,
      label: {
        text: 'value',
        style: {
          fontWeight: 'bold',
        },
      },
      legend: {
        color: {
          title: false,
          position: 'right',
          rowPadding: 5,
        },
      },
      tooltip: {
        title: 'type'
      },
      annotations: [
        {
          type: 'text',
          style: {
            text: `总计\n${total}`,
            x: '50%',
            y: '50%',
            textAlign: 'center',
            fontSize: 18
          },
        },
      ],
    };
    return (
      <Pie {...config} />
    );
  }
}