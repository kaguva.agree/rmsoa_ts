import React from 'react';
import { ProCard, StatisticCard } from '@ant-design/pro-card';
import { ProjectOverviewDataType } from '../data.d';

const systemMap: Object = new Object();
systemMap['GBSP'] = '通用业务服务平台';
systemMap['AFA'] = '中间业务系统';
systemMap['TELLER'] = '综合柜面系统';

interface ProjectOverviewCardProps {
  loading: boolean;
  overviewDatas: ProjectOverviewDataType;
}

interface ProjectOverviewCardState {
  currentTabKey: string;
}

export default class ProjectOverviewCard extends React.PureComponent<ProjectOverviewCardProps, ProjectOverviewCardState> {

  state: ProjectOverviewCardState = {
    currentTabKey: 'GBSP',
  };

  tiphHandleClick = (kind: string, type: string, systemCode: string) => {
    console.info(kind, type, systemCode);
  }

  /**
   * tip提示组件，可点击组件跳转至详情页面，查询相应的信息
   * @param kind 业务种类 1-提测需求，2-投产需求，3-交易任务
   * @param type 业务类型，1-今日，2-本周，3-下周，4-10天后
   * @param systemCode 系统编码
   * @returns TipItem
   */
  createTipItem = (kind: string, type: string, systemCode: string) => (
    <a
      title='点击可跳转至详细查询页面'
      onClick={() =>
        this.tiphHandleClick(kind, type, systemCode)
      }
    >
      查看详细信息
    </a>
  );

  render() {
    const { overviewDatas } = this.props;
    const redValueStyle: React.CSSProperties = { color: '#cf1322' };
    const {
      todaySitCqNum, thisWeekSitCqNum, nextWeekSitCqNum, tenDaysSitCqNum,
      todayPrdCqNum, thisWeekPrdCqNum, nextWeekPrdCqNum, tenDaysPrdCqNum,
      todayUndoCqTaskNum, thisWeekUndoCqTaskNum, nextWeekUndoCqTaskNum, expiredUndoCqTaskNum,
      todayUndoTradeTaskNum, thisWeekUndoTradeTaskNum, nextWeekUndoTradeTaskNum, expiredUndoTradeTaskNum,
      systemCode
    } = overviewDatas;

    return (
      <ProCard split="horizontal">
        <ProCard split="vertical">
          <StatisticCard
            statistic={{
              title: '今日提测需求',
              value: todaySitCqNum,
              valueStyle: redValueStyle,
              tip: this.createTipItem('1', '1', systemCode),
            }}
          />
          <StatisticCard
            statistic={{
              title: '本周提测需求',
              value: thisWeekSitCqNum,
              tip: this.createTipItem('1', '2', systemCode),
            }}
          />
          <StatisticCard
            statistic={{
              title: '下周提测需求',
              value: nextWeekSitCqNum,
              tip: this.createTipItem('1', '3', systemCode),
            }}
          />
          <StatisticCard
            statistic={{
              title: '10天后提测需求',
              value: tenDaysSitCqNum,
              tip: this.createTipItem('1', '4', systemCode),
            }}
          />
        </ProCard>
        <ProCard split="vertical">
          <StatisticCard
            statistic={{
              title: '今日投产需求',
              value: todayPrdCqNum,
              valueStyle: redValueStyle,
              tip: this.createTipItem('2', '1', systemCode),
            }}
          />
          <StatisticCard
            statistic={{
              title: '本周投产需求',
              value: thisWeekPrdCqNum,
              tip: this.createTipItem('2', '2', systemCode),
            }}
          />
          <StatisticCard
            statistic={{
              title: '下周投产需求',
              value: nextWeekPrdCqNum,
              tip: this.createTipItem('2', '3', systemCode),
            }}
          />
          <StatisticCard
            statistic={{
              title: '10天后投产需求',
              value: tenDaysPrdCqNum,
              tip: this.createTipItem('2', '4', systemCode),
            }}
          />
        </ProCard>
        <ProCard split="vertical">
          <StatisticCard
            statistic={{
              title: '今日需求待办任务',
              value: todayUndoCqTaskNum,
              valueStyle: redValueStyle,
              tip: this.createTipItem('3', '1', systemCode),
            }}
          />
          <StatisticCard
            statistic={{
              title: '本周需求待办任务',
              value: thisWeekUndoCqTaskNum,
              tip: this.createTipItem('3', '2', systemCode),
            }}
          />
          <StatisticCard
            statistic={{
              title: '下周需求待办任务',
              value: nextWeekUndoCqTaskNum,
              tip: this.createTipItem('3', '3', systemCode),
            }}
          />
          <StatisticCard
            statistic={{
              title: '已过期需求待办任务',
              value: expiredUndoCqTaskNum,
              valueStyle: redValueStyle,
              tip: this.createTipItem('3', '4', systemCode),
            }}
          />
        </ProCard>
        <ProCard split="vertical">
          <StatisticCard
            statistic={{
              title: '今日交易待办任务',
              value: todayUndoTradeTaskNum,
              valueStyle: redValueStyle,
              tip: this.createTipItem('3', '1', systemCode),
            }}
          />
          <StatisticCard
            statistic={{
              title: '本周交易待办任务',
              value: thisWeekUndoTradeTaskNum,
              tip: this.createTipItem('3', '2', systemCode),
            }}
          />
          <StatisticCard
            statistic={{
              title: '下周交易待办任务',
              value: nextWeekUndoTradeTaskNum,
              tip: this.createTipItem('3', '3', systemCode),
            }}
          />
          <StatisticCard
            statistic={{
              title: '已过期交易待办任务',
              value: expiredUndoTradeTaskNum,
              valueStyle: redValueStyle,
              tip: this.createTipItem('3', '4', systemCode),
            }}
          />
        </ProCard>
      </ProCard>
    );
  }
}