import React from 'react';
import { Row, Col } from 'antd';
import numeral from 'numeral';
import { RankingDataItem } from '../data.d';
import styles from '../style.less';

const kindMap: Object = new Object();
kindMap['alreadyPrdTradeTask'] = '已投产需求交易任务';
kindMap['alreadyPrdWorkload'] = '已投产需求工作量';
kindMap['notPrdTradeTask'] = '未投产需求交易任务';
kindMap['notPrdWorkload'] = '未投产需求工作量';

interface ProjectRankingCardProps {
  kind: string;
  rankingDatas: RankingDataItem[];
}

export default class ProjectRankingCard extends React.PureComponent<ProjectRankingCardProps> {

  render() {
    const { kind, rankingDatas = [] } = this.props;
    const rankingTitle = kindMap[kind];

    return (
      <Row style={{ display: 'block' }}>
        <Col>
          <div className={styles.salesRank}>
            <h4 className={styles.rankingTitle}>
              {rankingTitle}
            </h4>
            <ul className={styles.rankingList}>
              {rankingDatas.map((item, i) => (
                <li key={item.title}>
                  <span className={`${styles.rankingItemNumber} ${i < 3 ? styles.active : ''}`}>
                    {i + 1}
                  </span>
                  <span className={styles.rankingItemTitle} title={item.tip}>
                    <a>{item.title}</a>
                  </span>
                  <span className={styles.rankingItemValue}>
                    {numeral(item.total).format('0,0')}
                  </span>
                </li>
              ))}
            </ul>
          </div>
        </Col>
      </Row>
    );
  }
}