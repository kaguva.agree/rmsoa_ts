import React from 'react';
import { Col, Row } from 'antd';
import MessageBox from './MessageBox';

interface WorkbenchStatisticsCardProps {
  cqUndoTaskNum: number;
  todayCqDoTaskNum: number;
  tradeUndoTaskNum: number;
  todayTradeDoTaskNum: number;
  cqTaskLoading: boolean;
  tradeTaskLoading: boolean;
  onClick?: (boxType: string) => void
}

export default class WorkbenchStatisticsCard extends React.PureComponent<WorkbenchStatisticsCardProps> {

  boxClick = (boxType: string) => {
    console.info('WorkbenchStatisticsCard.onClick');
    const { onClick } = this.props;
    if (onClick) {
      onClick(boxType);
    }
  }

  render() {
    const { cqUndoTaskNum, todayCqDoTaskNum, tradeUndoTaskNum, todayTradeDoTaskNum, cqTaskLoading, tradeTaskLoading } = this.props;
    const topColResponsiveProps = {
      span: 4,
      style: { marginBottom: 8 },
    };

    return (
      <React.Fragment>
        <Row gutter={12}>
          <Col {...topColResponsiveProps}>
            <MessageBox
              title='需求待办任务'
              tooltipTitle='需求待办任务列表'
              footerLabel='今日完成任务数'
              undoTotal={cqUndoTaskNum}
              doTotal={todayCqDoTaskNum}
              loading={cqTaskLoading}
              onClick={() => this.boxClick('cqUndoTask')}
            />
          </Col>
          <Col {...topColResponsiveProps}>
            <MessageBox
              title='交易待办任务'
              tooltipTitle='交易待办任务列表'
              footerLabel='今日完成任务数'
              undoTotal={tradeUndoTaskNum}
              doTotal={todayTradeDoTaskNum}
              loading={tradeTaskLoading}
              onClick={() => this.boxClick('tradeUndoTask')}
            />
          </Col>
          <Col {...topColResponsiveProps}>
            <MessageBox
              title='本周提测需求'
              tooltipTitle='本周提测需求'
              footerLabel='今日提测需求数'
              undoTotal={tradeUndoTaskNum}
              doTotal={todayTradeDoTaskNum}
              loading={tradeTaskLoading}
            />
          </Col>
          <Col {...topColResponsiveProps}>
            <MessageBox
              title='本周投产需求'
              tooltipTitle='本周投产需求'
              footerLabel='今日投产需求数'
              undoTotal={tradeUndoTaskNum}
              doTotal={todayTradeDoTaskNum}
              loading={tradeTaskLoading}
            />
          </Col>
          <Col {...topColResponsiveProps}>
            <MessageBox
              title='下周提测需求'
              tooltipTitle='下周提测需求'
              footerLabel='需求对应任务数'
              undoTotal={tradeUndoTaskNum}
              doTotal={todayTradeDoTaskNum}
              loading={tradeTaskLoading}
            />
          </Col>
          <Col {...topColResponsiveProps}>
            <MessageBox
              title='下周投产需求'
              tooltipTitle='下周投产需求'
              footerLabel='需求对应任务数'
              undoTotal={tradeUndoTaskNum}
              doTotal={todayTradeDoTaskNum}
              loading={tradeTaskLoading}
            />
          </Col>
        </Row>
      </React.Fragment>
    );
  }
}