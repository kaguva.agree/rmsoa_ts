import React from 'react';
import { ProjectOverviewRadarDataType, RadarDataItem } from '../data.d';
import { Radar } from '@ant-design/plots';

const systemMap: Object = new Object();
systemMap['GBSP'] = '通用业务服务平台';
systemMap['AFA'] = '中间业务系统';
systemMap['TELLER'] = '综合柜面系统';

const radarKindMap: Object = new Object();
radarKindMap['cqTasks'] = '需求数量';
radarKindMap['tradeTasks'] = '交易任务数量';

interface ProjectOverviewRadarCardProps {
  loading: boolean;
  radarDatas: ProjectOverviewRadarDataType;
}

export default class ProjectOverviewRadarCard extends React.PureComponent<ProjectOverviewRadarCardProps> {

  render() {
    const { radarDatas } = this.props;
    const { data } = radarDatas;
    let newData: RadarDataItem[] = [];
    data.map((item) => {
      const { kind } = item;
      const newKind = radarKindMap[kind];
      const newItem: RadarDataItem = {
        ...item,
        kind: newKind
      }
      newData.push(newItem);
    });
    const config = {
      height: 380,
      data: newData,
      xField: 'group',
      yField: 'value',
      colorField: 'kind',
      // shapeField: 'smooth',// smooth 没有参数就是折线，有参数且是smooth就是光滑的
      area: {
        style: {
          fillOpacity: 0.5,
        },
      },
      scale: { x: { padding: 0.5, align: 0 }, y: { tickCount: 10, domainMax: 10 } },
      axis: { x: { grid: true }, y: { zIndex: 1, title: false } },
      style: {
        lineWidth: 2,
      },
    };
    return (
      <Radar {...config} />
    );
  }
}