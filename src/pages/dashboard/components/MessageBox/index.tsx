import React from 'react';
import { Tooltip, Button  } from 'antd';
import { FundProjectionScreenOutlined } from '@ant-design/icons';
import ChartCard from '../ChartCard';
import Field from '../Field';

interface MessageBoxProps {
  title: string;
  tooltipTitle: string;
  footerLabel: string;
  undoTotal: number;
  doTotal: number;
  onClick?: () => void;
  loading: boolean;
}

export default class MessageBox extends React.PureComponent<MessageBoxProps> {

  handleClick = () => {
    console.info('MessageBox.onClick');
    const { onClick } = this.props;
    if (onClick) {
      onClick();
    }
  }

  render() {
    const { title, tooltipTitle, footerLabel, undoTotal, doTotal, loading } = this.props;
    return (
      <ChartCard
        bordered={false}
        title={title}
        action={
          <Tooltip
            title={tooltipTitle}
          >
            <Button icon={<FundProjectionScreenOutlined />} onClick={this.handleClick} />
          </Tooltip>
        }
        total={undoTotal}
        footer={
          <Field
            label={footerLabel}
            value={doTotal}
          />
        }
        contentHeight={46}
        loading={loading}
      />
    );
  }
}