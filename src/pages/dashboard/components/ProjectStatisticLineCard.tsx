import React from 'react';
import { ProCard } from '@ant-design/pro-card';
import { Line } from '@ant-design/plots';
import { ProjectStatisticDataType } from '../data.d';

const statisticKindMap: Object = new Object();
statisticKindMap['cqTasks'] = '需求任务';
statisticKindMap['tradeTasks'] = '交易任务';
statisticKindMap['workload'] = '工作量';

interface ProjectStatisticLineCardProps {
  statisticDatas: ProjectStatisticDataType[];
}

export default class ProjectStatisticLineCard extends React.PureComponent<ProjectStatisticLineCardProps> {

  render() {
    let { statisticDatas } = this.props;
    if (!statisticDatas || statisticDatas.length == 0) {
      statisticDatas.push({
        kind: '暂无数据',
        date: '202403',
        count: 0
      });
    }
    const data = statisticDatas.map((item: ProjectStatisticDataType) => {
      const { kind } = item;
      const data: ProjectStatisticDataType = {
        ...item,
        kind: statisticKindMap[kind]
      }
      return data;
    });
    const config = {
      height: 350,
      data,
      xField: 'date',
      yField: 'count',
      legend: {
        layout: 'horizontal',
        position: 'right'
      },
      colorField: 'kind',
      seriesField: 'kind',
      interaction: {
        tooltip: {
          marker: false,
        },
      }
    };

    return (
      <ProCard>
        <Line {...config} />
      </ProCard>
    );
  }
}