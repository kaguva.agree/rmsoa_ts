import React from 'react';
import { Space, DatePicker } from 'antd';
import { RangePickerProps } from 'antd/es/date-picker/generatePicker';
import { connect, Dispatch } from 'umi';
import { PageContainer } from '@ant-design/pro-layout';
import { ProCard } from '@ant-design/pro-card';
import { FETCH_OVERVIEW, FETCH_LASTWILLPRD_STATISTICS, FETCH_STATISTICS, FETCH_ALREADYPRD_STATISTICS, FETCH_NOTPRD_STATISTICS, FETCH_RANKINGS } from '@/actions/dashboard';
import { ConnectState } from '@/models/connect';
import { getDateTime } from '@/utils/utils';
import { DateTypeEnum, ProjectOverviewDataType, ProjectStatisticsDataType, ProjectStatisticDataType, ProjectRankingDataType, ProjectOverviewRadarDataType } from './data.d';
import ProjectOverviewCard from './components/ProjectOverviewCard';
import ProjectOverviewRadarCard from './components/ProjectOverviewRadarCard';
import ProjectStatisticLineCard from './components/ProjectStatisticLineCard';
import ProjectRankingCard from './components/ProjectRankingCard';
import { getTimeDistance } from './utils/utils';
import styles from './style.less';

const { RangePicker } = DatePicker;

interface AnalysisProps {
  overviewDatas: ProjectOverviewDataType;
  lastWillPrdDatas: ProjectOverviewRadarDataType;
  statisticsDatas: ProjectStatisticsDataType[];
  alreadyPrdStatisticDatas: ProjectStatisticDataType[];
  notPrdStatisticDatas: ProjectStatisticDataType[];
  rankingDatas: ProjectRankingDataType[];
  dispatch: Dispatch<any>;
  overviewLoading: boolean;
  lastWillPrdLoading: boolean;
  statisticsLoading: boolean;
  alreadyPrdStatisticLoading: boolean;
  notPrdStatisticLoading: boolean;
  rankingsLoading: boolean;
}

interface AnalysisState {
  currentTabKey: string;
  rangePickerValue: RangePickerValue;
}

type RangePickerValue = RangePickerProps<moment.Moment>['value'];

@connect((state: ConnectState) => ({
  overviewDatas: state.dashboards.overviewDatas,
  lastWillPrdDatas: state.dashboards.lastWillPrdDatas,
  statisticsDatas: state.dashboards.statisticsDatas,
  alreadyPrdStatisticDatas: state.dashboards.alreadyPrdStatisticDatas,
  notPrdStatisticDatas: state.dashboards.notPrdStatisticDatas,
  rankingDatas: state.dashboards.rankingDatas,
  overviewLoading: state.loading.effects['dashboards/fetchOverview'],
  lastWillPrdLoading: state.loading.effects['dashboards/fetchStatisticOfLastWillPrd'],
  statisticsLoading: state.loading.effects['dashboards/fetchStatistics'],
  alreadyPrdStatisticLoading: state.loading.effects['dashboards/fetchStatisticOfAlreadyPrd'],
  notPrdStatisticLoading: state.loading.effects['dashboards/fetchStatisticOfNotPrd'],
  rankingsLoading: state.loading.effects['dashboards/fetchRankings'],
}))
export default class Analysis extends React.PureComponent<AnalysisProps, AnalysisState> {

  state: AnalysisState = {
    currentTabKey: 'GBSP',
    rangePickerValue: getTimeDistance('year'),
  };

  reqRef: number = 0;
  // timeoutId: number = 0;
  intervalId: Object = {};

  componentDidMount() {
    const { dispatch } = this.props;
    this.reqRef = requestAnimationFrame(() => {
      dispatch(FETCH_OVERVIEW({
      }));
      dispatch(FETCH_LASTWILLPRD_STATISTICS({
      }));
      dispatch(FETCH_STATISTICS({
      }));
      dispatch(FETCH_ALREADYPRD_STATISTICS({
      }));
      dispatch(FETCH_NOTPRD_STATISTICS({
      }));
      dispatch(FETCH_RANKINGS({
      }));
    });
    // 添加定时任务
    this.intervalId = setInterval(() => {
      console.info('启动定时任务FETCH_LASTWILLPRD_STATISTICS');
      dispatch(FETCH_LASTWILLPRD_STATISTICS({
      }));
    }, 1000*60*3);
  }

  componentWillUnmount() {
    const { dispatch } = this.props;
    dispatch({
      type: 'dashboardAndanalysis/clear',
    });
    cancelAnimationFrame(this.reqRef);
    // clearTimeout(this.timeoutId);
    if (this.intervalId) {
      console.info('关闭定时任务FETCH_LASTWILLPRD_STATISTICS');
      clearTimeout(this.intervalId as NodeJS.Timeout);
    }
  }

  handleTabChange = (key: string) => {
    this.setState({
      currentTabKey: key,
    });
  };

  tiphHandleClick = (kind: string, cqStatus: string, sysCode: string) => {
    console.info(kind, cqStatus, sysCode);
  }

  handleRangePickerChange = (rangePickerValue: RangePickerValue) => {
    const { dispatch } = this.props;
    this.setState({
      rangePickerValue,
    });
    dispatch(FETCH_STATISTICS({
    }));
  };

  selectDate = (type: 'today' | 'week' | 'month' | 'year') => {
    const { dispatch } = this.props;
    this.setState({
      rangePickerValue: getTimeDistance(type),
    });
    dispatch(FETCH_STATISTICS({
    }));
  };

  isActive = (type: DateTypeEnum) => {
    const { rangePickerValue } = this.state;
    if (!rangePickerValue) {
      return '';
    }
    const value = getTimeDistance(type);
    if (!value) {
      return '';
    }
    if (!rangePickerValue[0] || !rangePickerValue[1]) {
      return '';
    }
    if (
      rangePickerValue[0].isSame(value[0] as moment.Moment, 'day') &&
      rangePickerValue[1].isSame(value[1] as moment.Moment, 'day')
    ) {
      return styles.currentDate;
    }
    return '';
  };

  rankingItemClick = (type: string) => {
    console.info('rankingItemClick', type);
  }

  render() {
    const { rangePickerValue } = this.state;
    const { overviewDatas, overviewLoading,
      lastWillPrdDatas, lastWillPrdLoading,
      alreadyPrdStatisticDatas, alreadyPrdStatisticLoading,
      notPrdStatisticDatas, notPrdStatisticLoading,
      rankingDatas, rankingsLoading } = this.props;
    console.info('rankingDatas', rankingDatas);
    let rankingDataMap = {};
    if (rankingDatas) {
      rankingDatas.map((item) => {
        const { kind, rankingData } = item;
        rankingDataMap[kind] = rankingData;
      });
    }
    const { lastWillPrdDate } = lastWillPrdDatas;
    const cqTaskTitle = `最近一次投产(${lastWillPrdDate})集群数据分布`;
    const dateTimeStr = getDateTime();

    return (
      <PageContainer
        extra={dateTimeStr}
      >
        <Space direction='vertical' size='middle' style={{ 'display': 'flex' }}>
          <ProCard split={'vertical'}>
            <ProCard title="项目任务数据" colSpan="60%">
              <ProjectOverviewCard
                loading={overviewLoading}
                overviewDatas={overviewDatas}
              />
            </ProCard>
            <ProCard title={cqTaskTitle} loading={lastWillPrdLoading}>
              <ProjectOverviewRadarCard
                loading={lastWillPrdLoading}
                radarDatas={lastWillPrdDatas}
              />
            </ProCard>
          </ProCard>
          <ProCard split={'vertical'}>
            <ProCard title="已投产需求数据统计" colSpan="50%" loading={alreadyPrdStatisticLoading}>
              <ProjectStatisticLineCard
                statisticDatas={alreadyPrdStatisticDatas}
              />
            </ProCard>
            <ProCard title="未投产需求数据统计" loading={notPrdStatisticLoading}>
              <ProjectStatisticLineCard
                statisticDatas={notPrdStatisticDatas}
              />
            </ProCard>
          </ProCard>
          <ProCard split={'vertical'}>
            <ProCard title="项目数据排名"
              extra={
                <div className={styles.salesExtraWrap}>
                  <div className={styles.salesExtra}>
                    <a className={this.isActive('week')} onClick={() => this.selectDate('week')}>
                      本周
                    </a>
                    <a className={this.isActive('month')} onClick={() => this.selectDate('month')}>
                      本月
                    </a>
                    <a className={this.isActive('year')} onClick={() => this.selectDate('year')}>
                      本年
                    </a>
                  </div>
                  <RangePicker
                    value={rangePickerValue}
                    onChange={this.handleRangePickerChange}
                    style={{ width: 256 }}
                  />
                </div>
              }
              loading={rankingsLoading}
            >
              <ProCard bordered>
                <ProjectRankingCard kind='alreadyPrdTradeTask' rankingDatas={rankingDataMap['alreadyPrdTradeTask']} />
              </ProCard>
              <ProCard bordered>
                <ProjectRankingCard kind='alreadyPrdWorkload' rankingDatas={rankingDataMap['alreadyPrdWorkload']} />
              </ProCard>
              <ProCard bordered>
                <ProjectRankingCard kind='notPrdTradeTask' rankingDatas={rankingDataMap['notPrdTradeTask']} />
              </ProCard>
              <ProCard bordered>
                <ProjectRankingCard kind='notPrdWorkload' rankingDatas={rankingDataMap['notPrdWorkload']} />
              </ProCard>
            </ProCard>
          </ProCard>
        </Space>
      </PageContainer>
    );
  }
}