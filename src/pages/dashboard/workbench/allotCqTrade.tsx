import React from 'react';
import { Space, Modal, Card, message } from 'antd';
import { connect, Dispatch, history } from 'umi';
import { Location } from 'history-with-query';
import { PageContainer } from '@ant-design/pro-layout';
import ProCard from '@ant-design/pro-card';
import AButton from '@/components/SelfComp/AButton';
import AGrid, { AGridButtonCallBackModel } from '@/components/SelfComp/AGrid';
import { ConnectState } from '@/models/connect';
import { decodeData } from '@/utils/commons';
import { DataItem } from '@/models/common';
import { CQ_TRADE_INIT, CQ_ADD_TRADE, TRADE_TASK, ALLOT_CQ_TRADE, PREALLOC_TRADE_TASK, UPDATE_TRADE_TASK } from '@/actions/workbench';
import CqBaseInfoForm from './components/CqBaseInfoForm';
import CqTradeInfoForm from './components/CqTradeInfoForm';
import TradeTaskModal from './components/TradeTaskModal';
import { CqBaseInfo, CqTradeInfo, TradeItem, TradeTaskItem } from './data';
import { CqUndoTaskRecord } from '../data';

interface AllotCqTradeProps {
  dispatch: Dispatch;
  location: Location;
  auditorData: DataItem[];
  developerData: DataItem[];
  testerData: DataItem[];
  tradeTaskNameData: DataItem[];
  tradeData: TradeItem[];
  tradeList: CqTradeInfo[];
  // tradeTaskList: TradeTaskItem[];
  // tradeTaskTotal: number;
  loading: boolean;
  /** 初始化参数查询 */
  queryLoading: boolean;
  /** 查询交易对应的开发任务 */
  queryTaskLoading: boolean;
  taskHandleLoading: boolean;
}

interface AllotCqTradeState {
  /** 交易列表集合，保存当面页面通过按钮添加的交易 */
  localTradeList: CqTradeInfo[];
  // 表格选择的数据
  selectedRows: CqTradeInfo[];
  selectedRowKeys: React.Key[];
  tradeTaskModalVisible: boolean;
  tradeTaskModalTitle: string;
  tradeTaskList: TradeTaskItem[];
  tradeTaskTotal: number;
}

// Partial 将所有属性变成可选
// type CqUndoTaskRecordEx = Partial<CqUndoTaskRecord>;
let formData: CqUndoTaskRecord = {
  userCode: '',
  cqId: '',
  cqCode: '',
  cqName: '',
  subSystem: '',
  clstCode: '',
  appCode: '',
  taskId: '',
  taskName: '',
  taskCreateTime: '',
  status: '',
  actInstId: '',
  actTaskId: '',
}

/**
 * 需求分配交易任务
 */
@connect((state: ConnectState) => ({
  auditorData: state.users.auditorData,
  developerData: state.users.developerData,
  testerData: state.users.testerData,
  tradeTaskNameData: state.systems.tradeTaskNameData,
  tradeData: state.trades.rows,
  tradeList: state.workbenchs.tradeList,
  loading: state.loading.effects['workbenchs/allotCqTrade'],
  queryLoading: state.loading.effects['workbenchs/fetchCqTradeInitParams'] || state.loading.effects['workbenchs/cqAddTrade'],
  queryTaskLoading: state.loading.effects['workbenchs/fetchTradeTasks'],
  taskHandleLoading: state.loading.effects['workbenchs/preAllocTradeTask'] || state.loading.effects['workbenchs/updateTradeTask'],
}))
export default class AllotCqTrade extends React.PureComponent<AllotCqTradeProps, AllotCqTradeState> {

  state: AllotCqTradeState = {
    localTradeList: [],
    selectedRows: [],
    selectedRowKeys: [],
    tradeTaskModalVisible: false,
    tradeTaskModalTitle: '',
    tradeTaskList: [],
    tradeTaskTotal: 0,
  };

  /**
   * 静态函数，根据新传入的props来映射到state。
   * 该函数必须有返回值。当props传入的内容不需要影响state，就必须返回一个null
   */
  static getDerivedStateFromProps(nextProps: AllotCqTradeProps, prevState: AllotCqTradeState) {
    console.info('getDerivedStateFromProps');
    const { localTradeList } = prevState;
    if (localTradeList && localTradeList.length > 0) {
      console.info('state有值以state为准');
      return prevState;
    }
    const { tradeList } = nextProps;
    if (tradeList && tradeList.length > 0) {
      const { localTradeList, ...res} = prevState;
      return {
        localTradeList: [...tradeList],
        ...res
      };
    }
    return prevState;
  }

  columns = [
    {
      title: '交易功能码',
      dataIndex: 'funcCode',
      width: 200
    },
    {
      title: '交易名称',
      dataIndex: 'funcName',
    },
    {
      title: '交易路径',
      dataIndex: 'funcAction',
    },
    {
      title: '预分配任务数量',
      dataIndex: 'taskNum',
      width: 200
    },
  ];

  componentDidMount() {
    console.info('AllotCqTrade.componentDidMount');
    // 获取页面传值
    const { location } = this.props;
    const { query } = location;
    console.info(query);
    console.info(typeof query);
    if (!query) {
      message.warning('非法访问');
      history.replace('/user/login');
      return;
    }
    if (Object.keys(query).length === 0) {
      message.warning('非法访问');
      history.replace('/user/login');
      return;
    }
    if (query) {
      const { e } = query;
      if (!e) {
        message.warning('非法访问');
        history.replace('/user/login');
        return;
      }
      const decodeObj: CqUndoTaskRecord = decodeData(e as string);
      formData = {
        ...decodeObj
      };
      console.info(formData);
      const { cqId, cqCode } = formData;
      if (!cqCode) {
        console.log('非法访问');
        Modal.warn({
          title: '警告',
          content: '非法访问',
          okText: '确定',
          // 不满足条件时，直接返回上一页面
          onOk: () => this.goBack()
        });
      } else {
        // 满足条件，开始查询页面参数
        const { dispatch } = this.props;
        dispatch(CQ_TRADE_INIT({
          pageSize: 100,
          pageNum: 1,
          cqId
        }));
      }
    }
  }

  componentWillUnmount(): void {
    // this.setState({
    //   localTradeList: [],
    //   selectedRows: [],
    //   selectedRowKeys: [],
    //   tradeTaskModalVisible: false,
    //   tradeTaskModalTitle: '',
    // });
  }

  /**
   * 退出按钮
   */
  goBack = () => {
    history.goBack();
  }

  addTrade2Table = async (record: CqTradeInfo) => {
    // 交易功能码栏位的值添加到表格中
    // 如果交易已添加到表格中，则不允许重复添加
    const { tradeId } = record;
    const { localTradeList } = this.state;
    let flag = false;
    for (let i = 0; i < localTradeList.length; i += 1) {
      const tmpItem =  localTradeList[i];
      if (tmpItem.tradeId === tradeId) {
        flag = true;
        break;
      }
    }
    if (flag) {
      const { funcCode } = record;
      message.warn(`交易[${funcCode}]已添加至表格中，请勿重复添加`);
      return false;
    }
    // 与服务端通讯添加交易，但状态为中间态
    const { dispatch } = this.props;
    // 上送任务id和实例id是为了保障任务信息的完整性，不被篡改和代理任务
    const { actTaskId, actInstId, cqId } = formData;
    const values = {
      actTaskId,
      actInstId,
      cqId,
      tradeId
    };
    // 获取当前登录用户，作为任务处理人
    const res = await dispatch(CQ_ADD_TRADE({
      ...values,
    }));
    if (res) {
      localTradeList.push(record);
      this.setState({
        localTradeList: [...localTradeList]
      });
      return true;
    }
    return false;
  }

  handleSubmit = () => {
    console.log('Received values of form: ');
    const { localTradeList } =this.state;
    // 判断是否有交易没有分配交易任务
    let flag = true;
    let tmpFuncCode;
    console.info(localTradeList);
    for (let i = 0; i < localTradeList.length; i += 1) {
      const tmpItem =  localTradeList[i];
      if (tmpItem.taskNum === 0) {
        flag = false;
        tmpFuncCode = tmpItem.funcCode;
        break;
      }
    }
    if (!flag) {
      message.warn(`交易[${tmpFuncCode}]未分配开发任务，不可提交`);
      return;
    }
    console.info('AllotCqTrade.handleSubmit2');
    // 准备数据，调用回调函数提交服务端处理
    this.allotCqTrade();
  }

  allotCqTrade = async () => {
    // 与服务端通讯
    const { dispatch } = this.props;
    const { actTaskId, actInstId, cqId } = formData;
    const values = {
      actTaskId,
      actInstId,
      cqId
    };
    // 获取当前登录用户，作为任务处理人
    const res = await dispatch(ALLOT_CQ_TRADE({
      ...values,
    }));
    if (res) {
      // 是否重置表单
      message.success('需求分配交易任务成功', 1)
      .then(() => this.onExit());
    }
  }

  /**
   * 退出按钮
   */
  onExit = () => {
    history.goBack();
  }

  /**
   * 表格勾选回调函数
   * @param {*} rows 表格选中数据集合
   */
  onSelectRow = (keys: React.Key[], rows: CqTradeInfo[]) => {
    this.setState({
      selectedRows: rows,
      selectedRowKeys: keys
    });
  };

  /**
   * 页码改变的回调，参数是改变后的页码及每页条数
   * @param page 改变后的页码，上送服务器端
   * @param pageSize 每页数据条数
   */
  onPageNumAndSizeChange = (page: number, pageSize: number) => {
    console.log(page, pageSize);
  };

  /**
   * 工具栏左边的按钮
   * @returns 按钮集合
   */
  renderLeftButton = () => {
    const { queryTaskLoading } = this.props;
    return (
      <>
        <AButton code='task' pageCode='cq-trade' name='分配任务' onClick={this.openTradeTaskModal} loading={queryTaskLoading} />
      </>
    );
  }

  /**
   * 处理按钮点击回调事件
   * @param {*} payload 数据包
   */
  handleBtnCallBack = (callBackModel: AGridButtonCallBackModel<CqTradeInfo>) => {
    // btn 按钮
    // keys 表格勾选数组
    const { btn, keys } = callBackModel;
    console.info(btn);
    console.info(keys);
    const { alias } = btn;
    // 删除
    if (alias === 'delete') {
      // 调用删除服务，删除勾选数据
      console.info(keys);
      this.deleteTrades(keys);
      return;
    }
  }

  /**
   * 打开任务分配窗口
   */
  openTradeTaskModal = async () => {
    // 表格选中内容
    console.info('openTradeTaskModal');
    const { selectedRowKeys } = this.state;
    if (selectedRowKeys.length === 0) {
      message.warn('请先选择一条数据!');
      return;
    }
    if (selectedRowKeys.length > 1) {
      message.warn('只能选择一条数据!');
      return;
    }
    const { cqId } = formData;
    // 获取交易id
    const tradeId = selectedRowKeys[0];
    console.info('tradeId', tradeId);
    // 查询交易已分配任务列表
    const { dispatch } = this.props;
    const res = await dispatch(TRADE_TASK({
      cqId,
      tradeId,
      pageSize: 10,
      pageNum: 1
    }));
    console.info(res);
    if (res) {
      // 这里试试dispatch的回调方法
      // 只有查询成功才会弹窗
      const { selectedRows } = this.state;
      const { funcCode } = selectedRows[0];
      const tradeTaskModalTitle = `交易${funcCode}任务窗口`;
      const { total, rows } = res;
      this.setState({
        tradeTaskModalVisible: true,
        tradeTaskModalTitle,
        tradeTaskList: [...rows],
        tradeTaskTotal: total,
      });
    }
  }

  deleteTrades = (keys: React.Key[]) => {
    console.info('deleteTrades', keys);
  }

  /**
   * 给交易分配开发任务，只有通讯成功才会关闭窗口及将按钮设置为可点击
   */
  handleTradeTaskModalOk = (taskNum: number) => {
    console.info('handleTradeTaskModalOk');
    // 更新交易已分配任务数量
    // 修改本页面表格中的已分配任务数量
        // 待定
        // 是否将更新任务数量放到弹窗确定事件中
    const { selectedRows } = this.state;
    const currentTrade = selectedRows[0];
    this.updateTaskNum(currentTrade, taskNum);
    this.setState({
      tradeTaskModalVisible: false,
      tradeTaskList: [],
      tradeTaskTotal: 0,
      // 取消勾选
      selectedRows: [],
      selectedRowKeys: [],
    });
  }

  /**
   * 弹窗取消事件，无动作，仅清空弹窗数据
   */
  handleTradeTaskModalCancel = () => {
    console.info('handleTradeTaskModalCancel');
    this.setState({
      tradeTaskModalVisible: false,
      tradeTaskList: [],
      tradeTaskTotal: 0,
    });
  }

  tradeTaskHandle = async (handleType: string, records: TradeTaskItem | TradeTaskItem[]) => {
    // 与服务端通讯添加交易，但状态为中间态
    const { dispatch } = this.props;
    const { selectedRows } = this.state;
    const { cqId } = formData;
    const currentTrade = selectedRows[0];
    const { tradeId, funcCode } = currentTrade;
    console.info('tradeTaskHandle', handleType, records);
    console.info('tradeTaskHandle', typeof records);
    if (handleType === 'add' && typeof records === 'object') {
      const record = records as TradeTaskItem;
      const values = {
        cqId,
        tradeId,
        funcCode,
        ...record
      };
      // 获取当前登录用户，作为任务处理人
      const res = await dispatch(PREALLOC_TRADE_TASK({
        ...values,
      }));
      if (res) {
        // 修改本页面表格中的已分配任务数量
        // 待定
        // 是否将更新任务数量放到弹窗确定事件中
        // const { flag } = record;
        // if (flag === 'add') {
        //   const { taskNum } = currentTrade;
        //   this.updateTaskNum(currentTrade, taskNum + 1);
        // } else if (flag === 'delete') {
        //   const { taskNum } = currentTrade;
        //   this.updateTaskNum(currentTrade, taskNum - 1);
        // }
        return true;
      }
    }
    return false;
  }

  /**
   * 更新交易已分配任务数
   * @param currentTrade 当前交易
   * @param taskNum 任务数
   */
  updateTaskNum = (currentTrade: CqTradeInfo, taskNum: number) => {
    const { tradeId } = currentTrade;
    const { localTradeList } = this.state;
    let index = -1;
    const len = localTradeList.length;
    for(let i = 0;i < len; i += 1){
      const item = localTradeList[i];
      if(tradeId === item.tradeId){
        index = i;
        break;
      }
    }
    const newRecord: CqTradeInfo = {
      ...currentTrade,
      taskNum,
    }
    console.info('符合条件的数组下标为', index);
    localTradeList.splice(index, 1, newRecord);
    console.info('准备提交的数据', localTradeList);
    this.setState({
      localTradeList: [...localTradeList]
    });
  }

  render() {
    console.log('AllotCqTrade.render');
    const { auditorData, tradeData, loading, queryLoading } = this.props;
    const { tradeTaskNameData, developerData, testerData, taskHandleLoading } = this.props;
    const { localTradeList, tradeTaskModalVisible, tradeTaskModalTitle, tradeTaskList, tradeTaskTotal } = this.state;
    const code = 'cq-trade';
    const tradeTotal = 10;
    const rowKey = (record: CqTradeInfo) => record.tradeId;
    const pkField = 'tradeId';
    console.log(tradeTaskModalVisible);
    const { cqCode, cqName, subSystem, clstCode, appCode, userCode } = formData;
    const baseFormData: CqBaseInfo = {
      cqCode,
      cqName,
      subSystem,
      clstCode,
      appCode,
      auditor: userCode
    };
    console.info(localTradeList);

    return (
      <PageContainer title='需求分配交易任务'>
        <Space direction='vertical' size='middle' style={{ display: 'flex' }}>
          <ProCard
            title='需求基本信息'
          >
            <CqBaseInfoForm
              colon={false}
              loading={false}
              auditorData={auditorData}
              formData={baseFormData}
            />
          </ProCard>
          <ProCard title='分配交易任务'>
            <CqTradeInfoForm
              colon={false}
              loading={loading}
              queryLoading={queryLoading}
              tradeData={tradeData}
              addTrade2Table={this.addTrade2Table}
              onSubmit={this.handleSubmit}
              onCancel={this.onExit}
            />
            <Card title='交易列表' style={{ marginBottom: 0 }}>
              <AGrid
                code={code}
                actionColumnFixed='right'
                btnCallBack={this.handleBtnCallBack}
                delConfirmMsg='确定删除选中记录？若交易已分配开发任务，将一并删除！'
                renderLeftButton={this.renderLeftButton}
                columns={this.columns}
                rowKey={rowKey}
                pkField={pkField}
                dataSource={localTradeList}
                loading={queryLoading}
                total={tradeTotal}
                scroll={{x:1000}}
                onSelectRow={this.onSelectRow}
                onPageNumAndSizeChange={this.onPageNumAndSizeChange}
              />
            </Card>
          </ProCard>
        </Space>
        {
          !tradeTaskModalVisible ? null :
          <TradeTaskModal
            colon={false}
            modalTitle={tradeTaskModalTitle}
            modalWidth={1100}
            modalVisible={tradeTaskModalVisible}
            loading={false}
            tradeTaskNameData={tradeTaskNameData}
            developerData={developerData}
            testerData={testerData}
            tradeTaskList={tradeTaskList}
            tradeTaskTotal={tradeTaskTotal}
            onHandlerOK={this.handleTradeTaskModalOk}
            onHandlerCancel={this.handleTradeTaskModalCancel}
            taskHandleLoading={taskHandleLoading}
            tradeTaskHandle={this.tradeTaskHandle}
          />
        }
      </PageContainer>
    );
  }
}