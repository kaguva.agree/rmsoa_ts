import React from 'react';
import { Space, Modal, message } from 'antd';
import { connect, Dispatch, history } from 'umi';
import { Location } from 'history-with-query';
import { PageContainer } from '@ant-design/pro-layout';
import ProCard from '@ant-design/pro-card';
import { ConnectState } from '@/models/connect';
import { decodeData } from '@/utils/commons';
import { DataItem } from '@/models/common';
import { CQ_DOC_INIT, FILL_CQ_DOC } from '@/actions/workbench';
import CqBaseInfoForm from './components/CqBaseInfoForm';
import CqDetailInfoForm from './components/CqDetailInfoForm';
import CqDocInfoForm from './components/CqDocInfoForm';
import { CqBaseInfo, CqDetailInfo, CqDocInfo } from './data';

interface FillCqDocInfoProps {
  dispatch: Dispatch;
  location: Location;
  auditorData: DataItem[];
  designerData: DataItem[];
  loading: boolean;
}

let formData: any = {};

/**
 * 录入需求文档信息
 */
@connect((state: ConnectState) => ({
  auditorData: state.users.auditorData,
  designerData: state.users.designerData,
  loading: state.loading.effects['workbenchs/fillCqDocInfo'],
}))
export default class FillCqDocInfo extends React.PureComponent<FillCqDocInfoProps> {

  componentDidMount() {
    console.info('FillCqDocInfo.componentDidMount');
    // 获取页面传值
    const { location } = this.props;
    const { query } = location;
    console.info(query);
    console.info(typeof query);
    if (!query) {
      message.warning('非法访问');
      history.replace('/user/login');
      return;
    }
    if (Object.keys(query).length === 0) {
      message.warning('非法访问');
      history.replace('/user/login');
      return;
    }
    if (query) {
      const { e } = query;
      if (!e) {
        message.warning('非法访问');
        history.replace('/user/login');
        return;
      }
      const decodeObj = decodeData(e as string);
      console.info(decodeObj);
      formData = {
        ...decodeObj
      };
      console.info(formData);
      const { cqCode } = formData;
      if (!cqCode) {
        console.log('非法访问');
        Modal.warn({
          title: '警告',
          content: '非法访问',
          okText: '确定',
          // 不满足条件时，直接返回上一页面
          onOk: () => this.goBack()
        });
      } else {
        // 满足条件，开始查询页面参数
        const { dispatch } = this.props;
        dispatch(CQ_DOC_INIT({}));
      }
    }
  }

  /**
   * 退出按钮
   */
  goBack = () => {
    history.goBack();
  }

  handleSubmit = (record: CqDocInfo) => {
    console.log('Received values of form: ', record);
    // const queryParams = this.props.location.query;
    this.fillCqDocInfo(record);
  }

  fillCqDocInfo = async (record: CqDocInfo) => {
    // 与服务端通讯
    const { dispatch } = this.props;
    const { actTaskId, actInstId, cqId } = formData;
    const values = {
      actTaskId,
      actInstId,
      cqId,
      ...record
    };
    // 获取当前登录用户，作为任务处理人
    const res = await dispatch(FILL_CQ_DOC({
      ...values,
    }));
    if (res) {
      // 是否重置表单
      message.success('需求文档信息录入成功', 1)
      .then(() => this.onExit());
    }
  }

  /**
   * 退出按钮
   */
  onExit = () => {
    history.goBack();
  }

  render() {
    console.log('FillCqDocInfo.render');
    const { auditorData, designerData, loading } = this.props;
    const baseFormData: CqBaseInfo = {
      ...formData
    };
    const detailFormData: CqDetailInfo = {
      cqCode: '123',
      designerA: 'zhuyulin',
      designerB: 'zhuyulin',
      devStartDate: '2024-01-01',
      devEndDate: '2024-01-02',
      sitStartDate: '2024-01-03',
      sitEndDate: '2024-01-04',
      uatStartDate: '2024-01-05',
      uatEndDate: '2024-01-06',
      prdDate: '2024-05-28'
    }
    return (
      <PageContainer title='录入需求文档信息'>
        <Space direction='vertical' size='middle' style={{ display: 'flex' }}>
          <ProCard
            title='需求基本信息'
          >
            <CqBaseInfoForm
              colon={false}
              auditorData={auditorData}
              formData={baseFormData}
            />
          </ProCard>
          <ProCard
            title='需求详细信息'
            collapsible
            defaultCollapsed
          >
            <CqDetailInfoForm
              colon={false}
              flag='view'
              formData={detailFormData}
              designerData={designerData}
            />
          </ProCard>
          <ProCard title='需求文档信息'>
            <CqDocInfoForm
              colon={false}
              loading={loading}
              formData={formData}
              onSubmit={this.handleSubmit}
              onCancel={this.onExit}
            />
          </ProCard>
        </Space>
      </PageContainer>
    );
  }
}