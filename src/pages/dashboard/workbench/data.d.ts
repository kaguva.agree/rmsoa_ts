export interface CqBaseInfo {
  /** 需求编号 */
  cqCode: string;
  /** 需求名称 */
  cqName: string;
  /** 分配系统 */
  subSystem: string;
  /** 分配集群 */
  clstCode: string;
  /** 分配应用 */
  appCode: string;
  /** 主开人员 */
  auditor: string;
}

export interface CqDetailInfo {
  cqCode: string;
  designerA: string;
  designerB: string;
  devStartDate: string;
  devEndDate: string;
  sitStartDate: string;
  sitEndDate: string;
  uatStartDate: string;
  uatEndDate: string;
  prdDate: string;
}

export interface CqDocInfo {
  cqCode: string;
  /** 需求文件路径 */
  srdPath: string;
  /** 概设文件路径 */
  odsPath: string;
  /** 详设文件路径 */
  ddsPath: string;
  /** 接口分析结果路径 */
  oarPath: string;
  /** 接口分析结果路径 */
  oarPath: string;
  /** 数据库设计文档路径 */
  dbdPath: string;
  /** 需求工作量 */
  workload: string;
}

export interface CqTradeInfo {
  tradeId: string;
  /** 交易功能码 */
  funcCode: string;
  /** 交易名称 */
  funcName: string;
  /** 交易路径 */
  funcAction: string;
  /** 已分配任务数 */
  taskNum: number;
}

/**
 * 交易
 */
export interface TradeItem {
  /** 交易id */
  tradeId: string;
  /** 交易码 */
  funcCode: string;
  /** 交易码 */
  funcName: string;
  /** 交易码 */
  funcAction: string;
  /** 交易所属子系统 */
  subSystem?: string;
  /** 交易负责人 */
  owner: string;
  /** 交易所属集群 */
  clstCode: string;
  /** 交易所属集群名 */
  clstName: string;
  /** 交易所属应用 */
  appCode: string;
  /** 交易所属应用名 */
  appName: string;
  /** 状态，0-正常，1-失效 */
  status: string;
}

/**
 * 交易任务类型
 * @param taskHash 任务唯一标识
 * @param handleType 任务处理标识，add edit delete
 */
export interface TradeTaskItem {
  taskId?: string;
  taskType: string;
  taskName: string;
  taskPhase: string;
  startDate: string;
  endDate: string;
  developer: string;
  tester: string;
  comment: string;
  taskHash: string;
  handleType?: string;
}