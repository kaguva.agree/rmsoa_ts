import React from 'react';
import { Space, Modal, message } from 'antd';
import { connect, Dispatch, history } from 'umi';
import { Location } from 'history-with-query';
import { PageContainer } from '@ant-design/pro-layout';
import ProCard from '@ant-design/pro-card';
import { ConnectState } from '@/models/connect';
import { decodeData } from '@/utils/commons';
import { DataItem } from '@/models/common';
import { CQ_DETAIL_INIT, FILL_CQ_DETAIL } from '@/actions/workbench';
import CqBaseInfoForm from './components/CqBaseInfoForm';
import CqDetailInfoForm from './components/CqDetailInfoForm';
import { CqDetailInfo } from './data';

interface FillCqDetailInfoProps {
  dispatch: Dispatch;
  location: Location;
  auditorData: DataItem[];
  designerData: DataItem[];
  loading: boolean;
}

let formData: any = {};

/**
 * 录入需求详细信息
 */
@connect((state: ConnectState) => ({
  auditorData: state.users.auditorData,
  designerData: state.users.designerData,
  loading: state.loading.effects['workbenchs/fillCqDetailInfo'],
}))
export default class FillCqDetailInfo extends React.PureComponent<FillCqDetailInfoProps> {

  componentDidMount() {
    console.info('FillCqDetailInfo.componentDidMount');
    // 获取页面传值
    const { location } = this.props;
    const { query } = location;
    console.info(query);
    console.info(typeof query);
    if (!query) {
      message.warning('非法访问，请检查数据');
      history.replace('/user/login');
      return;
    }
    if (Object.keys(query).length === 0) {
      message.warning('非法访问，请检查数据');
      history.replace('/user/login');
      return;
    }
    if (query) {
      const { e } = query;
      if (!e) {
        message.warning('非法访问，请检查数据');
        history.replace('/user/login');
        return;
      }
      try {
        const decodeObj = decodeData(e as string);
        console.info(decodeObj);
        formData = {
          ...decodeObj
        };
        console.info(formData);
      } catch (error) {
        console.error('参数解密失败');
        console.error(error);
        message.warning('非法访问，请检查数据');
        history.replace('/user/login');
        return;
      }
      const { cqCode } = formData;
      if (!cqCode) {
        Modal.warn({
          title: '警告',
          content: '非法访问，请检查数据',
          okText: '确定',
          // 不满足条件时，直接返回上一页面
          onOk: () => this.goBack()
        });
      } else {
        // 满足条件，开始查询页面参数
        const { dispatch } = this.props;
        dispatch(CQ_DETAIL_INIT({}));
      }
    }
  }

  /**
   * 退出按钮
   */
  goBack = () => {
    history.goBack();
  }

  handleSubmit = (record: CqDetailInfo) => {
    console.log('Received values of form: ', record);
    // const queryParams = this.props.location.query;
    this.fillCqDetailInfo(record);
  }

  fillCqDetailInfo = async (record: CqDetailInfo) => {
    // 与服务端通讯
    const { dispatch } = this.props;
    const { actTaskId, actInstId, cqId } = formData;
    const values = {
      actTaskId,
      actInstId,
      cqId,
      ...record
    };
    // 获取当前登录用户，作为任务处理人
    const res = await dispatch(FILL_CQ_DETAIL({
      ...values,
    }));
    if (res) {
      // 是否重置表单
      message.success('需求详细信息录入成功', 1)
      .then(() => this.onExit());
    }
  }

  /**
   * 退出按钮
   */
  onExit = () => {
    history.goBack();
  }

  render() {
    console.log('FillCqDetailInfo.render');
    const { auditorData, designerData, loading } = this.props;
    return(
      <PageContainer title='录入需求详细信息'>
        <Space direction='vertical' size='middle' style={{ display: 'flex' }}>
          <ProCard
            title='需求基本信息'
          >
            <CqBaseInfoForm
              colon={false}
              loading={false}
              auditorData={auditorData}
              formData={formData}
            />
          </ProCard>
          <ProCard title='需求详细信息'>
            <CqDetailInfoForm
              colon={false}
              loading={loading}
              formData={formData}
              designerData={designerData}
              onSubmit={this.handleSubmit}
              onCancel={this.onExit}
            />
          </ProCard>
        </Space>
      </PageContainer>
    );
  }
}