import React from 'react';
import { Form, Row, Col, Input, InputNumber, Button, message } from 'antd';
import { FormInstance } from 'antd/es/form/Form';
import { formLayout, basicFormItemLangLayout } from '@/utils/commons';
import { FormProps } from '@/models/FormModal';
import { CqDocInfo } from '../data';

type CqDocInfoFormProps<T> = FormProps<T> & {
}

interface CqDocInfoFormState {
}

const FormItem = Form.Item;

export default class CqDocInfoForm extends React.PureComponent<CqDocInfoFormProps<CqDocInfo>, CqDocInfoFormState> {

  formRef = React.createRef<FormInstance>();

  workloadChange = (value: number | string | null) => {
    if (value) {
      const valueStr = value + '';
      if (valueStr.indexOf('.') >= 0) {
        message.warning('需求工作量只能为正整数');
      }
    }
  }

  /**
   * 表单提交
   */
  handleSubmit = () => {
    // 收集表单数据
    // 传递回父组件
    const fieldsValue = this.formRef.current!.getFieldsValue();
    console.info(fieldsValue);
    const values = {
      ...fieldsValue
    };
    console.log('Received values of form: ', values);
    const { onSubmit } = this.props;
    if (onSubmit) {
      onSubmit({
        ...values
      });
    }
  }

  handleQuit = () => {
    // 先清除form表单
    this.formRef.current!.resetFields();
    const { onCancel } = this.props;
    if (onCancel) {
      onCancel();
    }
  }

  render() {
    console.log('CqDocInfoForm.render');
    const { colon, loading, formData } = this.props;
    // 8-16布局
    // 只有cqCode有值时，才会渲染form表单
    // 这里的写法cqCode &&
    const { cqCode } = formData;
    // 8-16布局
    const formItemLangLayout = basicFormItemLangLayout;

    return(
      <React.Fragment>
        {cqCode &&
        <Form
          layout={formLayout}
          ref={this.formRef}
          onFinish={this.handleSubmit}
        >
          <Row>
            <Col span={12}>
              <FormItem label="需求文件路径" name='srdPath' {...formItemLangLayout} colon={colon}
                rules={[
                  { required: true, message: '需求文件路径必输' }
                ]}
              >
                <Input />
              </FormItem>
            </Col>
          </Row>
          <Row>
            <Col span={12}>
              <FormItem label="概设文件路径" name='odsPath' {...formItemLangLayout} colon={colon}
                rules={[
                  { required: true, message: '概设文件路径必输' }
                ]}
              >
                <Input />
              </FormItem>
            </Col>
          </Row>
          <Row>
            <Col span={12}>
              <FormItem label="详设文件路径" name='ddsPath' {...formItemLangLayout} colon={colon}
                rules={[
                  { required: true, message: '详设文件路径必输' }
                ]}
              >
                <Input />
              </FormItem>
            </Col>
          </Row>
          <Row>
            <Col span={12}>
              <FormItem label="接口分析结果路径" name='oarPath' {...formItemLangLayout} colon={colon}>
                <Input />
              </FormItem>
            </Col>
          </Row>
          <Row>
            <Col span={12}>
              <FormItem label="数据库设计文档路径" name='dbdPath' {...formItemLangLayout} colon={colon}>
                <Input />
              </FormItem>
            </Col>
          </Row>
          <Row>
            <Col span={12}>
              <FormItem label="需求工作量" name='workload' {...formItemLangLayout} colon={colon}
                rules={[
                  { required: true, message: '需求工作量必输' },
                ]}
              >
                <InputNumber
                  min={3}
                  precision={0}
                  onChange={this.workloadChange}
                  style={{ width: '100%' }}
                  addonAfter='人天'
                />
              </FormItem>
            </Col>
          </Row>
          <Row justify='center'>
            <Col>
              <FormItem>
                <Button type="primary" loading={loading} htmlType="submit">
                  提交
                </Button>
                <Button style={{ marginLeft: 8 }} onClick={this.handleQuit}>返回</Button>
              </FormItem>
            </Col>
          </Row>
        </Form>}
      </React.Fragment>
    );
  }
}