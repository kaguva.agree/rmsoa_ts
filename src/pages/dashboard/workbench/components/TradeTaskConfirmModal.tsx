import React from 'react';
import { Modal, Tabs, Form, Row, Col, Input, DatePicker, Steps, Button, message, StepProps } from 'antd';
import { FormInstance } from 'antd/es/form/Form';
import moment from 'moment';
import ASelect from '@/components/SelfComp/ASelect';
import { DataItem } from '@/models/common';
import { FormModalProps } from '@/models/FormModal';
import { modalFormItemLayout, maxModalFormItemLayout } from '@/utils/commons';
import { TradeTaskConfirmItem, TradeTaskCommentItem, TradeTaskHandleRecord } from '../../data';

type TradeTaskConfirmProps<T> = FormModalProps<T> & {
  testerData: DataItem[];
  developerData: DataItem[];
}

const { TabPane } = Tabs;
const FormItem = Form.Item;
const { TextArea } = Input;
const { RangePicker } = DatePicker;
const dateFormatPattern = 'YYYY-MM-DD';

/**
 * 交易分配开发任务相关窗口组件
 */
export default class TradeTaskConfirmModal extends React.PureComponent<TradeTaskConfirmProps<TradeTaskConfirmItem>> {

  formRef = React.createRef<FormInstance>();

  componentDidMount() {
    console.info('TradeTaskConfirmModal.componentDidMount');
  }

  onCancel = () => {
    // 先清除form表单
    this.formRef.current!.resetFields();
    const { onHandlerCancel } = this.props;
    if (onHandlerCancel) {
      onHandlerCancel();
    }
  }

  /**
   * 弹窗关闭不会触发componentWillUnmount，因此弹窗的清理放在afterClose中
   */
  afterClose = () => {
    console.info('TradeTaskConfirmModal.afterClose');
    this.setState({
      localTradeTaskConfirmList: [],
      localTradeTaskConfirmTotal: 0,
      selectedRows: [],
      selectedRowKeys: [],
      operaType: 'add',
    });
  }

  /**
   * 确认任务
   */
  approveTaskHandler = () => {
    this.formRef.current!.validateFields().then(fieldsValue => {
      console.info(fieldsValue);
      const { confirmMsg } = fieldsValue;
      this.tradeTaskHandler(confirmMsg, true);
    }).catch();
  }

  /**
   * 驳回任务
   */
  rejectTaskHandler = () => {
    this.formRef.current!.validateFields().then(fieldsValue => {
      console.info(fieldsValue);
      const { confirmMsg } = fieldsValue;
      this.tradeTaskHandler(confirmMsg, false);
    }).catch();
  }

  /**
   * 交易任务处理函数
   * @param confirmMsg 任务处理附言
   * @param agree 确认还是驳回
   */
  tradeTaskHandler = (confirmMsg: string, agree: boolean) => {
    const { formData } = this.props;
    const { taskId, status, actInstId, actTaskId } = formData;
    const values: TradeTaskHandleRecord = {
      taskId,
      status,
      actInstId,
      actTaskId,
      confirmMsg,
      agree
    };
    console.log('Received values of form: ', values);
    const { onHandlerOK } = this.props;
    if (onHandlerOK) {
      onHandlerOK(values);
    }
  }

  renderSteps = (data: TradeTaskCommentItem[]) => {
    const newData = data.map(item => {
      // console.log(item);
      const { userCode, time, comment } = item;
      return {
        title: comment,
        description: `${userCode} ${message} ${time}`
      }
    });
    return newData;
  }

  render() {
    console.info("TradeTaskConfirmModal.render");
    const { modalTitle, modalWidth, modalVisible, loading } = this.props;
    const formLayout = 'horizontal';
    const { colon, formData } = this.props;
    const { developerData, testerData } = this.props;
    console.info(formData, developerData, testerData);
    // 状态为1时，没有驳回按钮
    const { status, startDate, endDate, hisList = [] } = formData;
    const taskDate = [moment(startDate, dateFormatPattern), moment(endDate, dateFormatPattern)];
    const commentStepItems: StepProps[] = this.renderSteps(hisList);

    return (
      <Modal
        title={modalTitle}
        destroyOnClose
        afterClose={this.afterClose}
        width={modalWidth}
        open={modalVisible}
        confirmLoading={loading}
        onCancel={this.onCancel}
        footer={[
          <>
            <Button key='tradeTaskConfirmModalApprove' type='primary' onClick={this.approveTaskHandler} loading={loading}>确认</Button>
            { status !== '1' ? <Button key='tradeTaskConfirmModalReject' type='primary' onClick={this.rejectTaskHandler} loading={loading}>驳回</Button> : null}
            <Button key='tradeTaskConfirmModalCancel' onClick={this.onCancel}>取消</Button>
          </>
        ]}>
        <Tabs defaultActiveKey="approveView">
          <TabPane tab="审批" key="approveView">
            <Form layout={formLayout} ref={this.formRef}>
              <Row>
                <Col span={24}>
                  <FormItem label="任务名称" name='taskName' {...maxModalFormItemLayout} colon={colon}
                    initialValue={ formData.taskName }
                  >
                    <Input disabled />
                  </FormItem>
                </Col>
              </Row>
              <Row>
                <Col span={12}>
                  <FormItem label="任务起止时间" name='taskDate'  {...modalFormItemLayout} colon={colon}
                    initialValue={taskDate}
                  >
                    <RangePicker format={dateFormatPattern} disabled />
                  </FormItem>
                </Col>
              </Row>
              <Row>
                <Col span={12}>
                  <FormItem label="开发人员" name='developer'  {...modalFormItemLayout} colon={colon}
                    initialValue={ formData.developer }
                  >
                    <ASelect dataSource={developerData} disabled />
                  </FormItem>
                </Col>
                <Col span={12}>
                  <FormItem label="测试人员" name='tester'  {...modalFormItemLayout} colon={colon}
                    initialValue={ formData.tester }
                  >
                    <ASelect dataSource={testerData} disabled />
                  </FormItem>
                </Col>
              </Row>
              <Row>
                <Col span={24}>
                  <FormItem label="备注/附件CC路径" name='comment' {...maxModalFormItemLayout} colon={colon}
                    initialValue={ formData.comment }
                  >
                    <TextArea disabled/>
                  </FormItem>
                </Col>
              </Row>
              <Row>
                <Col span={24}>
                  <FormItem label="附言" name='confirmMsg' {...maxModalFormItemLayout} colon={colon}
                    rules={[
                      { max: 100, message: '附言最多允许输入100个字符' },
                      { required: true, message: '附言必输' }
                    ]}>
                    <TextArea showCount maxLength={100} />
                  </FormItem>
                </Col>
              </Row>
            </Form>
          </TabPane>
          <TabPane tab="历史信息" key="historyView">
            { hisList && hisList.length > 0 ? <Steps direction='horizontal' size='small' items={commentStepItems} /> : '暂无历史流程信息' }
          </TabPane>
        </Tabs>
      </Modal>
    );
  }
}