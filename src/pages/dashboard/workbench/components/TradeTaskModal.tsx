import React from 'react';
import { Modal, Form, Row, Col, Input, DatePicker, Card, Button, message } from 'antd';
import { FormInstance } from 'antd/es/form/Form';
import moment from 'moment';
import AButton from '@/components/SelfComp/AButton';
import AGrid, { AGridButtonCallBackModel } from '@/components/SelfComp/AGrid';
import ASelect, { ASelectOptionType } from '@/components/SelfComp/ASelect';
import { DataItem, FlagEnum } from '@/models/common';
import { FormModalProps } from '@/models/FormModal';
import { formLayout, modalFormItemLayout, maxModalFormItemLayout, getItemValue } from '@/utils/commons';
import { TradeTaskItem } from '../data';

type TradeTaskProps<T> = FormModalProps<T> & {
  testerData: DataItem[];
  developerData: DataItem[];
  tradeTaskNameData: DataItem[];
  tradeTaskList: TradeTaskItem[];
  tradeTaskTotal: number;
  flag?: FlagEnum;
  tradeTaskHandle: (handleType: string, record: TradeTaskItem | TradeTaskItem[]) => Promise<boolean>;
  taskHandleLoading: boolean;
}
type TradeTaskPropsEx<T> = Omit<TradeTaskProps<T>, 'formData'>;

interface TradeTaskState {
  /** 交易列表集合，保存当面页面通过按钮添加的交易 */
  localTradeTaskList: TradeTaskItem[];
  localTradeTaskTotal: number;
  // 表格选择的数据
  selectedRows: TradeTaskItem[];
  selectedRowKeys: React.Key[];
  /** add edit */
  operaType: FlagEnum;
}

const FormItem = Form.Item;
const TextArea = Input.TextArea;
const { RangePicker } = DatePicker;
const dateFormatPattern = 'YYYY-MM-DD';

/**
 * 交易分配开发任务相关窗口组件
 */
export default class TradeTaskModal extends React.PureComponent<TradeTaskPropsEx<TradeTaskItem>, TradeTaskState> {

  formRef = React.createRef<FormInstance>();

  state: TradeTaskState = {
    localTradeTaskList: [],
    localTradeTaskTotal: 0,
    selectedRows: [],
    selectedRowKeys: [],
    operaType: 'add',
  };

  /**
   * 静态函数，根据新传入的props来映射到state。
   * 该函数必须有返回值。当props传入的内容不需要影响state，就必须返回一个null
   */
  static getDerivedStateFromProps(nextProps: TradeTaskPropsEx<TradeTaskItem>, prevState: TradeTaskState) {
    console.info('getDerivedStateFromProps');
    const { localTradeTaskList } = prevState;
    const { tradeTaskList, tradeTaskTotal } = nextProps;
    console.info(localTradeTaskList);
    if (localTradeTaskList && localTradeTaskList.length > 0) {
      console.info('state有值以state为准');
      return prevState;
    }
    if (tradeTaskList && tradeTaskList.length > 0) {
      const { localTradeTaskList, localTradeTaskTotal, ...res} = prevState;
      return {
        localTradeTaskList: [...tradeTaskList],
        localTradeTaskTotal: tradeTaskTotal,
        ...res
      };
    }
    return prevState;
  }

  componentDidMount() {
    console.info('TradeTaskModal.componentDidMount');
  }

  columns = [
    {
      title: '任务名称',
      dataIndex: 'taskName',
      width: 150,
      ellipsis: true
    },
    {
      title: '任务阶段',
      dataIndex: 'taskPhase',
      width: 100,
    },
    {
      title: '开始时间',
      dataIndex: 'startDate',
      width: 120
    },
    {
      title: '结束时间',
      dataIndex: 'endDate',
      width: 120
    },
    {
      title: '开发人员',
      dataIndex: 'developer',
      width: 120
    },
    {
      title: '测试人员',
      dataIndex: 'tester',
      width: 120
    },
    {
      title: '备注/附件CC路径',
      dataIndex: 'comment',
      width: 300,
      ellipsis: true
    }
  ];

  onOk = () => {
    // 判断表单是否有正在分配的任务
    // 没有，则判断表格是否有数据
    // 没有，则提示请先分配任务在提交
    // 有，返回父页面当前任务数量信息等
    Modal.confirm({
      title: '确定',
      content: '确定提交任务吗？请检查交易任务分配是否有错误及遗漏！',
      onOk: () => this.submitTradeTask()
    });
  };

  submitTradeTask = () => {
    const { onHandlerOK } = this.props;
    if (onHandlerOK) {
      // 获取当前任务总数
      const { localTradeTaskTotal } = this.state;
      onHandlerOK(localTradeTaskTotal);
    }
  }

  onCancel = () => {
    // 先清除form表单
    this.formRef.current!.resetFields();
    const { onHandlerCancel } = this.props;
    if (onHandlerCancel) {
      onHandlerCancel();
    }
  }

  /**
   * 弹窗关闭不会触发componentWillUnmount，因此弹窗的清理放在afterClose中
   */
  afterClose = () => {
    console.info('TradeTaskModal.afterClose');
    this.setState({
      localTradeTaskList: [],
      localTradeTaskTotal: 0,
      selectedRows: [],
      selectedRowKeys: [],
      operaType: 'add',
    });
  }

  taskTypeOnChange = (value: string, option: ASelectOptionType | ASelectOptionType[]) => {
    // console.info(value);
    // console.info(option);
    const { tradeTaskNameData } = this.props;
    const taskName = getItemValue(tradeTaskNameData, value);
    this.formRef.current!.setFieldsValue({
      taskName
    });
  }

  /**
   * 点击表格中的修改按钮，反显表格数据到表单中
   */
  modifyTaskHandler = () => {
    const { selectedRowKeys } = this.state;
    if (selectedRowKeys.length === 0) {
      message.warn('请先选择一条数据!');
      return;
    }
    if (selectedRowKeys.length > 1) {
      message.warn('只能选择一条数据!');
      return;
    }
    const { selectedRows } = this.state;
    const record: TradeTaskItem = selectedRows[0];
    console.log('modifyTaskHandler', record);
    this.setDataToForm(record);
  }

  setDataToForm = (record: TradeTaskItem) => {
    // 根据页面传值，将栏位赋默认值
    const { startDate, endDate, ...rest } = record;
    this.formRef.current!.setFieldsValue({
      taskDate: [moment(startDate, dateFormatPattern), moment(endDate, dateFormatPattern)],
      ...rest
    });
    this.setState({
      operaType: 'edit'
    });
  }

  deleteTaskHandler = () => {
    const { selectedRowKeys } = this.state;
    if (selectedRowKeys.length === 0) {
      message.warn('至少选择一条数据!');
      return;
    }
    const { selectedRows } = this.state;
    const record: TradeTaskItem = selectedRows[0];
    console.log('deleteTaskHandler', record);
    Modal.confirm({
      title: '确认',
      content: '确定删除该任务吗',
      onOk: () => this.deleteTasks(selectedRows)
    });
  }

  /**
   * 删除任务
   */
  deleteTasks = (records: TradeTaskItem[]) => {
    // 调父页面方法，删除任务
    const { tradeTaskHandle } = this.props;
    if (tradeTaskHandle) {
      tradeTaskHandle('delete', records).then(res => {
        if (res) {
          // 服务器通讯通讯成功后才修改表格
          const { localTradeTaskList, localTradeTaskTotal } = this.state;
          const newTradeTaskList = localTradeTaskList;
          const len = newTradeTaskList.length;
          for (let k = 0; k < len; k += 1) {
            const record = records[k];
            const { taskHash } = record;
            let index = -1;
            for (let i = 0; i < len; i += 1) {
              const item = newTradeTaskList[i];
              if (taskHash === item.taskHash) {
                index = i;
                break;
              }
            }
            console.info('符合条件的数组下标为', index);
            newTradeTaskList.splice(index, 1);
          }
          this.setState({
            localTradeTaskList: [...newTradeTaskList],
            localTradeTaskTotal: localTradeTaskTotal - 1,
            operaType: 'add',
          });
        }
      });
    }
  }

  /**
   * 生成一个4位随机数
   *
   * @returns 4位随机数
   */
  randomStr = (): string => {
    return (((1 + Math.random()) * 0x10000) | 0).toString(16).substring(1);
  }

  /**
   * 添加或修改表单数据
   * 默认为添加，当点击了表格中的修改按钮，则反显表格数据到表单中
   */
  addTaskToTable = () => {
    const fieldsValue = this.formRef.current!.getFieldsValue();
    console.info(fieldsValue);
    let { taskHash } = fieldsValue;
    const { taskDate, ...rest } = fieldsValue;
    const { operaType } = this.state;
    console.info('表单原始数据', fieldsValue, operaType);
    if (!taskHash) {
      // 添加时设置一个hash值
      taskHash = this.randomStr();
    }
    const values = {
      ...rest,
      taskHash,
      startDate: taskDate[0].format(dateFormatPattern),
      endDate: taskDate[1].format(dateFormatPattern),
    };
    // 新增
    if (operaType === 'add') {
      this.addTask(values);
    } else if (operaType === 'edit') {
      this.updateTask(values);
    }
  }

  onReset = () => {
    // 先清除form表单
    this.formRef.current!.resetFields();
    this.setState({
      operaType: 'add'
    });
  }

  /**
   * 新增任务
   * @param record 表单数据
   */
  addTask = (formData: any) => {
    // 调父页面方法，新增任务
    const { tradeTaskHandle } = this.props;
    if (tradeTaskHandle) {
      const record: TradeTaskItem = {
        ...formData,
      };
      tradeTaskHandle('add', record).then(res => {
        if (res) {
          // 服务器通讯通讯成功后才添加进表格
          const { localTradeTaskList, localTradeTaskTotal } = this.state;
          const newTradeTaskList = [...localTradeTaskList];
          newTradeTaskList.push(formData);
          console.info('准备提交的数据', newTradeTaskList);
          this.setState({
            localTradeTaskList: [...newTradeTaskList],
            localTradeTaskTotal: localTradeTaskTotal + 1,
            operaType: 'add',
          });
          this.formRef.current!.resetFields();
        }
      });
    }
  }

  /**
   * 修改任务
   * @param record 表单数据
   */
  updateTask = (formData: any) => {
    // 调父页面方法，修改任务
    const { tradeTaskHandle } = this.props;
    if (tradeTaskHandle) {
      const record: TradeTaskItem = {
        ...formData,
      };
      tradeTaskHandle('edit', record).then(res => {
        if (res) {
          // 服务器通讯通讯成功后才修改表格
          const { taskHash } = formData;
          const { localTradeTaskList } = this.state;
          const newTradeTaskList = [...localTradeTaskList];
          // 按照hash查找对应条目，然后修改
          const len = newTradeTaskList.length;
          let index = -1;
          for (let i = 0; i < len; i += 1) {
            const item = newTradeTaskList[i];
            if (taskHash === item.taskHash) {
              index = i;
              break;
            }
          }
          console.info('符合条件的数组下标为', index);
          newTradeTaskList.splice(index, 1, formData);
          console.info('准备提交的数据', newTradeTaskList);
          this.setState({
            localTradeTaskList: [...newTradeTaskList],
            operaType: 'add',
          });
          this.formRef.current!.resetFields();
        }
      });
    }
  }

  /**
   * 查看时，展示交易信息，不展示任务表单
   * @returns 交易信息栏位
   */
  getTradeContent = () => {
    const { colon } = this.props;
    return (
      <React.Fragment>
        <Row>
          <Col span={12}>
            <FormItem label="交易功能码" name='funcCode' {...modalFormItemLayout} colon={colon}>
              <Input disabled />
            </FormItem>
          </Col>
        </Row>
        <Row>
          <Col span={24}>
            <FormItem label="交易名称" name='funcName' {...maxModalFormItemLayout} colon={colon}>
              <Input disabled />
            </FormItem>
          </Col>
        </Row>
        <Row>
          <Col span={24}>
            <FormItem label="交易路径" name='funcAction' {...maxModalFormItemLayout} colon={colon}>
              <Input disabled />
            </FormItem>
          </Col>
        </Row>
      </React.Fragment>
    );
  }

  /**
   * 新增，修改任务时，展示任务表单
   * @returns 交易信息栏位
   */
  getTaskContent = () => {
    const { colon, developerData, testerData, tradeTaskNameData } = this.props;
    const taskPhaseData: DataItem[] = [
      {
        key: 'DEV',
        value: '开发测试'
      },
      {
        key: 'SIT',
        value: '系统集成测试'
      },
      {
        key: 'UAT',
        value: '用户验收测试'
      },
    ];
    return (
      <>
        <Row>
          <Col span={12}>
            <FormItem label="任务类型" name='taskType' {...modalFormItemLayout} colon={colon}
              rules={[
                { required: true, message: '任务类型必输' },
              ]}>
              <ASelect dataSource={tradeTaskNameData} type='1' onChange={this.taskTypeOnChange} />
            </FormItem>
          </Col>
          <Col span={12}>
            <FormItem label="" name='taskHash' colon={colon}>
              <Input type='hidden'/>
            </FormItem>
          </Col>
        </Row>
        <Row>
          <Col span={24}>
            <FormItem label="任务名称" name='taskName' {...maxModalFormItemLayout} colon={colon}
              rules={[
                { required: true, message: '任务名称必输' },
                { max: 15, message: '任务名称最多允许输入15个字符' }
              ]}>
              <Input />
            </FormItem>
          </Col>
        </Row>
        <Row>
          <Col span={12}>
            <FormItem label="任务阶段" name='taskPhase' {...modalFormItemLayout} colon={colon}
              rules={[
                {required: true, message: '任务阶段必输' }
              ]}>
              <ASelect dataSource={taskPhaseData} />
            </FormItem>
          </Col>
          <Col span={12}>
            <FormItem label="任务起止时间" name='taskDate'  {...modalFormItemLayout} colon={colon}
              rules={[
                {required: true, message: '任务起止时间必输' }
              ]}>
              <RangePicker format={dateFormatPattern} />
            </FormItem>
          </Col>
        </Row>
        <Row>
          <Col span={12}>
            <FormItem label="开发人员" name='developer'  {...modalFormItemLayout} colon={colon}
              rules={[
                {required: true, message: '开发人员必输' }
              ]}>
              <ASelect dataSource={developerData} />
            </FormItem>
          </Col>
          <Col span={12}>
            <FormItem label="测试人员" name='tester'  {...modalFormItemLayout} colon={colon}
              rules={[
                {required: true, message: '测试人员必输' }
              ]}>
              <ASelect dataSource={testerData} />
            </FormItem>
          </Col>
        </Row>
        <Row>
          <Col span={24}>
            <FormItem label="备注/附件CC路径" name='comment' {...maxModalFormItemLayout} colon={colon}
              rules={[
                { max: 100, message: '备注/附件CC路径最多允许输入100个字符' }
              ]}>
              <TextArea showCount maxLength={100} />
            </FormItem>
          </Col>
        </Row>
      </>
    );
  }

  /**
   * 工具栏左边的按钮
   * @returns 按钮集合
   */
  renderLeftButton = () => {
    const { loading } = this.props;
    return (
      <>
        <AButton code='editTask' pageCode='cq-trade' name='修改任务' onClick={this.modifyTaskHandler} loading={loading} />
        <AButton code='deleteTask' pageCode='cq-trade' name='删除任务' onClick={this.deleteTaskHandler} loading={loading} />
      </>
    );
  }

  /**
   * 处理按钮点击回调事件
   * @param {*} payload 数据包
   */
  handleBtnCallBack = (callBackModel: AGridButtonCallBackModel<TradeTaskItem>) => {
    // btn 按钮
    // keys 表格勾选数组
    const { btn, keys, rows } = callBackModel;
    console.info(btn);
    console.info(keys);
    const { alias } = btn;
    console.info('handleBtnCallBack', rows);
    // 修改
    if (alias === 'edit') {
      const { rows } = callBackModel;
      const record: TradeTaskItem = rows[0];
      this.setDataToForm(record);
      return;
    }
    // 删除
    if (alias === 'delete') {
      // 调用删除服务，删除勾选数据
      console.info(keys);
      this.deleteTasks(rows);
      return;
    }
  }

  /**
   * 表格勾选回调函数
   * @param {*} rows 表格选中数据集合
   */
  onSelectRow = (keys: React.Key[], rows: TradeTaskItem[]) => {
    console.info(keys);
    console.info(rows);
    this.setState({
      selectedRows: rows,
      selectedRowKeys: keys
    });
  };

  /**
   * 页码改变的回调，参数是改变后的页码及每页条数
   * @param page 改变后的页码，上送服务器端
   * @param pageSize 每页数据条数
   */
  onPageNumAndSizeChange = (page: number, pageSize: number) => {
    console.log(page, pageSize);
  };

  render() {
    console.info("TradeTaskModal.render");
    const { modalTitle, modalWidth, modalVisible, loading, flag, taskHandleLoading } = this.props;
    const { localTradeTaskList, localTradeTaskTotal, operaType } = this.state;
    const code = 'trade-task';
    const rowKey = (record: TradeTaskItem) => `${record.taskId}-${record.taskHash}`;
    const pkField = 'taskHash';
    const buttonName = operaType === 'add' ? '新增任务': '修改任务';

    return (
      <Modal
        title={modalTitle}
        destroyOnClose
        afterClose={this.afterClose}
        width={modalWidth}
        open={modalVisible}
        confirmLoading={loading}
        onOk={this.onOk}
        onCancel={this.onCancel}>
        <Form layout={formLayout} ref={this.formRef} onFinish={this.addTaskToTable}>
          {
            flag === 'view' ?
            this.getTradeContent(): this.getTaskContent()
          }
          <Row justify='center'>
            <Col>
              <FormItem>
                {
                  flag !== 'view' ?
                    <>
                      <Button type="primary" htmlType="submit" loading={taskHandleLoading}>{buttonName}</Button>
                    </> : null
                }
                <Button style={{ marginLeft: 8 }} onClick={this.onReset}>重置</Button>
              </FormItem>
            </Col>
          </Row>
        </Form>
        <Card title='任务列表' style={{ marginBottom: 0 }}>
          <AGrid
            code={code}
            actionColumnFixed={'right'}
            btnCallBack={this.handleBtnCallBack}
            columns={this.columns}
            rowKey={rowKey}
            pkField={pkField}
            dataSource={localTradeTaskList}
            loading={loading}
            total={localTradeTaskTotal}
            scroll={{x:1000}}
            onSelectRow={this.onSelectRow}
            onPageNumAndSizeChange={this.onPageNumAndSizeChange}
          />
        </Card>
      </Modal>
    );
  }
}