import React from 'react';
import { Form, Row, Col, Input, Button, message } from 'antd';
import { FormInstance } from 'antd/es/form/Form';
import isEqual from 'lodash/isEqual';
import memoizeOne from 'memoize-one';
import ASelect, { ASelectOptionType } from '@/components/SelfComp/ASelect';
import { formLayout, basicFormItemLangLayout, maxFormItemLayout } from '@/utils/commons';
import { DataItem } from '@/models/common';
import { FormProps } from '@/models/FormModal';
import { CqTradeInfo, TradeItem } from '../data';

type CqTradeInfoFormProps<T> = FormProps<T> & {
  /** 交易列表 */
  tradeData: TradeItem[];
  /** 正在查询交易下拉框 */
  queryLoading: boolean;
  addTrade2Table: (record: CqTradeInfo) => Promise<boolean>;
}

type CqTradeInfoFormPropsEx<T> = Omit<CqTradeInfoFormProps<T>, 'formData'>;

const FormItem = Form.Item;
let tradeId: string;

export default class CqTradeInfoForm extends React.PureComponent<CqTradeInfoFormPropsEx<CqTradeInfo>> {

  formRef = React.createRef<FormInstance>();

  /**
   * 交易功能码栏位改变事件
   * 清除交易功能码栏位时，同时清除交易名称和交易路径两个栏位
   */
  funcCodeChange = (value: string, option: ASelectOptionType | ASelectOptionType[]) => {
    // 查询该功能码对应的交易信息
    console.log('funcCodeChange');
    console.log(option);
    if (value) {
      const { tradeData } = this.props;
      for (let i = 0; i < tradeData.length; i += 1) {
        const tmpItem =  tradeData[i];
        if (tmpItem.funcCode === value) {
          const { funcName, funcAction } = tmpItem;
          tradeId = tmpItem.tradeId;
          this.formRef.current!.setFieldsValue({
            funcName,
            funcAction
          });
          break;
        }
      }
    } else {
      this.formRef.current!.resetFields([
        'funcName','funcAction'
      ]);
    }
  }

  /**
   * 将选择的交易添加到表格中
   */
  addTradeToTable = () => {
    // 交易功能码栏位的值添加到表格中
    // 如果交易功能码，交易名称，交易路径为则不允许添加
    console.log(this.formRef.current);
    const funcCode = this.formRef.current!.getFieldValue('funcCode');
    const funcName = this.formRef.current!.getFieldValue('funcName');
    const funcAction = this.formRef.current!.getFieldValue('funcAction');
    console.log(funcCode);
    console.log(funcName);
    console.log(funcAction);
    if (!funcCode) {
      message.warning('请先选择交易');
    } else if (!funcName || !funcAction) {
      message.warning('交易信息不全，请联系系统负责人');
    } else {
      const { addTrade2Table } = this.props;
      if (addTrade2Table) {
        // 添加交易时，默认已分配任务数量为0
        const record: CqTradeInfo = {
          tradeId,
          funcCode,
          funcName,
          funcAction,
          taskNum: 0,
        };
        addTrade2Table(record).then(res => {
          if (res) {
            // 添加至表格后，清空交易三栏位
            this.formRef.current!.resetFields([
              'funcCode','funcName','funcAction'
            ]);
          }
        });
      }
    }
  }

  /**
   * 表单提交
   */
  handleSubmit = () => {
    // 收集表单数据
    // 传递回父组件
    // 将交易及交易任务提交服务器端
    console.info('AllotCqTrade.handleSubmit');
    const { onSubmit } = this.props;
    if (onSubmit) {
      onSubmit();
    }
  }

  handleQuit = () => {
    // 先清除form表单
    this.formRef.current!.resetFields();
    const { onCancel } = this.props;
    if (onCancel) {
      onCancel();
    }
  }

  getFuncData = (tradeData: TradeItem[]) => {
    console.info('getFuncData集合转换');
    let funcCodeData: DataItem[] = [];
    if (tradeData) {
      tradeData.forEach((item: TradeItem) => {
        const funcItem: DataItem = {
          key: item.funcCode,
          value: item.funcName
        }
        funcCodeData.push(funcItem);
      });
    }
    return funcCodeData;
  }

  memoizeOneGetFuncData = memoizeOne(this.getFuncData, isEqual);

  render() {
    console.log('CqTradeInfoForm.render');
    const { colon, loading, queryLoading, tradeData } = this.props;
    // 8-16布局
    // 8-16布局
    const formItemLangLayout = basicFormItemLangLayout;
    const funcCodeData = this.memoizeOneGetFuncData(tradeData);

    return (
      <React.Fragment>
        <Form
          layout={formLayout}
          ref={this.formRef}
          onFinish={this.handleSubmit}
        >
          <Row>
            <Col span={12}>
              <Form.Item label="交易功能码" {...formItemLangLayout} colon={colon}>
                <Row gutter={8}>
                  <Col span={12}>
                    <Form.Item
                      name="funcCode"
                      noStyle
                    >
                      <ASelect dataSource={funcCodeData} type='1' onChange={this.funcCodeChange} />
                    </Form.Item>
                  </Col>
                  <Col span={12}>
                    <Button loading={queryLoading} onClick={this.addTradeToTable} type='primary'>添加交易</Button>
                  </Col>
                </Row>
              </Form.Item>
            </Col>
          </Row>
          <Row>
            <Col span={24}>
              <FormItem label="交易名称" name='funcName' {...maxFormItemLayout} colon={colon}>
                <Input disabled />
              </FormItem>
            </Col>
          </Row>
          <Row>
            <Col span={24}>
              <FormItem label="交易路径" name='funcAction' {...maxFormItemLayout} colon={colon}>
                <Input disabled />
              </FormItem>
            </Col>
          </Row>
          <Row justify='center'>
            <Col>
              <FormItem>
                <Button type="primary" loading={loading} htmlType="submit">
                  提交
                </Button>
                <Button style={{ marginLeft: 8 }} onClick={this.handleQuit}>返回</Button>
              </FormItem>
            </Col>
          </Row>
        </Form>
      </React.Fragment>
    );
  }
}