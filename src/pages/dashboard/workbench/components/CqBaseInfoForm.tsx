import React from 'react';
import { Form, Row, Col, Input, Cascader } from 'antd';
import { FormInstance } from 'antd/es/form/Form';
import ASelect from '@/components/SelfComp/ASelect';
import { formLayout, basicFormItemLayout, basicFormItemLangLayout } from '@/utils/commons';
import { FormProps } from '@/models/FormModal';
import { DataItem } from '@/models/common';
import { CqBaseInfo } from '../data';
import { CascaderOption } from '@/pages/cq/data';

type CqBaseInfoFormProps<T> = FormProps<T> & {
  auditorData: DataItem[];
}
type CqBaseInfoFormPropsEx<T> = Omit<CqBaseInfoFormProps<T>, 'onSubmit'>;

interface CqBaseInfoFormState {
}

const FormItem = Form.Item;

export default class CqBaseInfoForm extends React.PureComponent<CqBaseInfoFormPropsEx<CqBaseInfo>, CqBaseInfoFormState> {

  formRef = React.createRef<FormInstance>();

  render() {
    console.log('CqBaseInfoForm.render');
    const { colon, formData, auditorData } = this.props;
    // 8-8布局
    const formItemLayout = basicFormItemLayout;
    // 8-16布局
    // 只有cqCode有值时，才会渲染form表单
    // 这里的写法cqCode &&
    const { cqCode, subSystem, clstCode, appCode } = formData;
    // 8-16布局
    const formItemLangLayout = basicFormItemLangLayout;
    const options: CascaderOption[] = [
      {
        value: 'GBSP',
        label: '通用业务服务平台',
        children: [
          {
            value: 'JRZF',
            label: '三方支付',
            children: [
              {
                value: 'VAFCOS',
                label: 'CROS业务',
              }
            ],
          },
          {
            value: 'SJTD',
            label: '运营管理',
            children: [
              {
                value: 'SYSYTD',
                label: '预填单业务',
              }
            ],
          },
        ],
      },
      {
        value: 'AFA',
        label: '中间业务系统',
        children: [
          {
            value: 'DLAP',
            label: '代理业务',
            children: [
              {
                value: 'ABOP',
                label: '批量业务',
              }
            ],
          },
          {
            value: 'ZFAP',
            label: '支付业务',
            children: [
              {
                value: 'BEPS',
                label: '小额支付',
              },
              {
                value: 'HVPS',
                label: '大额支付',
              }
            ],
          },
        ],
      },
    ];
    const system = [subSystem, clstCode, appCode];
    console.info(system);

    return (
      <React.Fragment>
        {cqCode &&
        <Form layout={formLayout} ref={this.formRef}>
          <Row>
            <Col span={12}>
              <FormItem label="需求编号" name='cqCode' {...formItemLayout} colon={colon}
                rules={[
                  { required: true, message: '需求编号必输' },
                  /**
                  { pattern: new RegExp(/^[A-Za-z][A-Za-z0-9]{16,16}$/, "g"), message: '需求编号只能以字母开头，16位的A-Za-z0-9组成' }
                  */
                ]}
                initialValue={ formData.cqCode }
              >
                <Input placeholder='必须为16位长度，只包含A-Za-z0-9'  disabled />
              </FormItem>
            </Col>
          </Row>
          <Row>
            <Col span={12}>
              <FormItem label="需求名称" name='cqName' {...formItemLangLayout} colon={colon}
                rules={[
                  { required: true, message: '需求名称必输' }
                ]}
                initialValue={ formData.cqName }
              >
                <Input disabled />
              </FormItem>
            </Col>
          </Row>
          <Row>
            <Col span={12}>
              <FormItem label="所属系统应用" name='system' {...formItemLangLayout} colon={colon}
                rules={[
                  { required: true, message: '所属系统应用必输' },
                ]}
                initialValue={ system }
              >
                <Cascader options={options} disabled />
              </FormItem>
            </Col>
          </Row>
          <Row>
            <Col span={12}>
              <FormItem label="审核人(主开)" name='auditor' {...formItemLayout} colon={colon}
                rules={[
                  { required: true, message: '审核人(主开)必输' },
                ]}
                initialValue={ formData.auditor }
              >
                <ASelect dataSource={auditorData} disabled />
              </FormItem>
            </Col>
          </Row>
        </Form>}
      </React.Fragment>
    );
  }
}