import React from 'react';
import { Form, Row, Col, Switch, DatePicker, Button } from 'antd';
import { FormInstance } from 'antd/es/form/Form';
import moment from 'moment';
import ASelect from '@/components/SelfComp/ASelect';
import { formLayout, basicFormItemLayout } from '@/utils/commons';
import { devDateDisableFunc, sitDateDisableFunc, uatDateDisableFunc, transform2moment } from '@/utils/utils';
import { FormProps } from '@/models/FormModal';
import { DataItem } from '@/models/common';
import { CqDetailInfo } from '../data';

type CqDetailInfoFormProps<T> = FormProps<T> & {
  designerData: DataItem[];
  flag?: string;
}

interface CqDetailInfoFormState {
  needDesignerFlag: boolean
}

const FormItem = Form.Item;
const { RangePicker } = DatePicker;
const dateFormatPattern = 'YYYY-MM-DD';
/**
 * 录入需求详细信息
 */
export default class CqDetailInfoForm extends React.PureComponent<CqDetailInfoFormProps<CqDetailInfo>, CqDetailInfoFormState> {

  formRef = React.createRef<FormInstance>();

  state: CqDetailInfoFormState = {
    needDesignerFlag: true,
  }

  /**
   * 是否需要设计人员
   * @param checked boolean型，表示是否需要设计人员，true表示需要，false表示不需要
   */
  needDesignerChange = (checked: boolean) => {
    // 不需要，则隐藏设计人员栏位，并清空栏位值
    if (!checked) {
      this.setState({
        needDesignerFlag: false,
      });
      this.formRef.current!.resetFields([
        'designerA',
        'designerB'
      ]);
    }else{
      this.setState({
        needDesignerFlag: true,
      });
    }
  }

  /**
   * 将遍历日期框中的所有日期，然后根据条件判断当前遍历的日期是否被禁用，条件判断返回true时，禁用，如果不满足条件则不禁用
   * @param currentDate moment类型
   */
  devDateDisableFunc = (currentDate: moment.Moment) => {
    // 拿到SIT日期
    // 拿到UAT日期
    // 如果SIT不为空，则可选择日期最多到SIT开始日期，
    // 如果SIT为空，UAT不为空，则可选择日期最多到UAT开始日期，
    // 如果SIT和UAT均为空，则无限制
    const sitDate = this.formRef.current!.getFieldValue('sitDate');
    const uatDate = this.formRef.current!.getFieldValue('uatDate');
    return devDateDisableFunc(currentDate, sitDate, uatDate);
  }

  /**
   * 将遍历日期框中的所有日期，然后根据条件判断当前遍历的日期是否被禁用，条件判断返回true时，禁用，如果不满足条件则不禁用
   * @param currentDate moment类型
   */
  sitDateDisableFunc = (currentDate: moment.Moment) => {
    // 拿到DEV日期
    // 拿到UAT日期
    // 如果DEV不为空，UAT为空，则可选择日期从DEV结束日期开始，
    // 如果DEV不为空，UAT不为空，则可选择日期从DEV结束日期开始，到UAT开始日期结束
    // 如果DEV为空，UAT为空，则无限制，
    // 如果DEV为空，UAT不为空，则可选择日期到UAT开始日期结束
    const devDate = this.formRef.current!.getFieldValue('devDate');
    const uatDate = this.formRef.current!.getFieldValue('uatDate');
    return sitDateDisableFunc(devDate, currentDate, uatDate);
  }

  /**
   * 将遍历日期框中的所有日期，然后根据条件判断当前遍历的日期是否被禁用，条件判断返回true时，禁用，如果不满足条件则不禁用
   * @param currentDate moment类型
   */
  uatDateDisableFunc = (currentDate: moment.Moment) => {
    // 拿到DEV日期
    // 拿到SIT日期
    // 如果SIT不为空，则可选择日期从SIT结束日期开始，
    // 如果SIT为空，DEV不为空，则可选择日期从DEV结束日期开始，
    // 如果DEV和SIT均为空，则无限制
    const devDate = this.formRef.current!.getFieldValue('devDate');
    const sitDate = this.formRef.current!.getFieldValue('sitDate');
    return uatDateDisableFunc(devDate, sitDate, currentDate);
  }

  /**
   * 表单提交
   */
  handleSubmit = () => {
    // 收集表单数据
    // 传递回父组件
    const fieldsValue = this.formRef.current!.getFieldsValue();
    console.info(fieldsValue);
    const { needDesigner, devDate, sitDate, uatDate, prdDate, ...rest } = fieldsValue;
    const values = {
      devStartDate: devDate[0].format(dateFormatPattern),
      devEndDate: devDate[1].format(dateFormatPattern),
      sitStartDate: sitDate[0].format(dateFormatPattern),
      sitEndDate: sitDate[1].format(dateFormatPattern),
      uatStartDate: uatDate[0].format(dateFormatPattern),
      uatEndDate: uatDate[1].format(dateFormatPattern),
      prdDate: prdDate.format(dateFormatPattern),
      ...rest
    };
    console.log('Received values of form: ', values);
    const { onSubmit } = this.props;
    if (onSubmit) {
      onSubmit({
        ...values
      });
    }
  }

  handleQuit = () => {
    // 先清除form表单
    this.formRef.current!.resetFields();
    const { onCancel } = this.props;
    if (onCancel) {
      onCancel();
    }
  }

  render() {

    console.log('CqDetailInfoForm.render');
    const { colon, loading, flag, formData } = this.props;
    const { needDesignerFlag } = this.state;
    console.log('CqDetailInfoForm', formData);
    // 从服务器获取
    const { designerData } = this.props;
        // 8-8布局
    const formItemLayout = basicFormItemLayout;
        // 8-16布局
    // 只有cqCode有值时，才会渲染form表单
    // 这里的写法cqCode &&
    const { cqCode } = formData;
    let disabled = false;
    if (flag === 'view') {
      disabled = true;
    }
    const devStart = transform2moment(formData.devStartDate, dateFormatPattern);
    const devEnd = transform2moment(formData.devEndDate, dateFormatPattern);
    const sitStart = transform2moment(formData.sitStartDate, dateFormatPattern);
    const sitEnd = transform2moment(formData.sitEndDate, dateFormatPattern);
    const uatStart = transform2moment(formData.uatStartDate, dateFormatPattern);
    const uatEnd = transform2moment(formData.uatEndDate, dateFormatPattern);

    return(
      <React.Fragment>
        {cqCode &&
        <Form layout={formLayout}
          ref={this.formRef}
          onFinish={this.handleSubmit}
        >
          <Row>
            <Col span={12}>
              <FormItem label="是否需要设计人员" name='needDesigner' {...formItemLayout} colon={colon} valuePropName='checked'
                initialValue={needDesignerFlag}
              >
                <Switch checkedChildren='需要' unCheckedChildren="不需要" onChange={this.needDesignerChange} disabled={disabled} />
              </FormItem>
            </Col>
          </Row>
          {
            !needDesignerFlag ? null :
            <Row>
              <Col span={12}>
                <FormItem label="设计人A" name='designerA' {...formItemLayout} colon={colon}
                  rules={[
                    { required: true, message: '设计人A必输' },
                  ]}
                  initialValue={formData.designerA}
                >
                  <ASelect dataSource={designerData} disabled={disabled} />
                </FormItem>
              </Col>
              <Col span={12}>
                <FormItem label="设计人B" name='designerB' {...formItemLayout} colon={colon}
                  initialValue={formData.designerB}
                >
                  <ASelect dataSource={designerData} disabled={disabled} />
                </FormItem>
              </Col>
            </Row>
          }
          <Row>
            <Col span={12}>
              <FormItem label="DEV起止时间" name='devDate' {...formItemLayout} colon={colon}
                rules={[
                  { required: true, message: 'DEV起止时间必输' },
                ]}
              >
                {
                  disabled ?
                  <RangePicker format={dateFormatPattern} defaultValue={[devStart, devEnd]} disabled={[disabled, disabled]} />
                  :
                  <RangePicker format={dateFormatPattern} disabledDate={this.devDateDisableFunc} />
                }
              </FormItem>
            </Col>
            <Col span={12}>
              <FormItem label="SIT起止时间" name='sitDate' {...formItemLayout} colon={colon}
                rules={[
                  { required: true, message: 'SIT起止时间必输' },
                ]}
              >
                {
                  disabled ?
                  <RangePicker format={dateFormatPattern} defaultValue={[sitStart, sitEnd]} disabled={[disabled, disabled]} />
                  :
                  <RangePicker format={dateFormatPattern} disabledDate={this.sitDateDisableFunc} />
                }
              </FormItem>
            </Col>
          </Row>
          <Row>
            <Col span={12}>
              <FormItem label="UAT起止时间" name='uatDate' {...formItemLayout} colon={colon}
                rules={[
                  { required: true, message: 'UAT起止时间必输' },
                ]}
              >
                {
                  disabled ?
                  <RangePicker format={dateFormatPattern} defaultValue={[uatStart, uatEnd]} disabled={[disabled, disabled]} />
                  :
                  <RangePicker format={dateFormatPattern} disabledDate={this.uatDateDisableFunc} />
                }
              </FormItem>
            </Col>
            <Col span={12}>
              <FormItem label="投产日期" name='prdDate' {...formItemLayout} colon={colon}
                rules={[
                  { required: true, message: '投产日期必输' },
                ]}
              >
                {
                  disabled ?
                  <DatePicker format={dateFormatPattern} defaultValue={transform2moment(formData.prdDate, dateFormatPattern)} style={{ width: '100%' }} disabled={disabled} />
                  :
                  <DatePicker format={dateFormatPattern} style={{ width: '100%' }} />
                }
              </FormItem>
            </Col>
          </Row>
          {
            disabled ? null:
            <Row justify='center'>
              <Col>
                <FormItem>
                  <Button type="primary" loading={loading} htmlType="submit">
                    提交
                  </Button>
                  <Button style={{ marginLeft: 8 }} onClick={this.handleQuit}>返回</Button>
                </FormItem>
              </Col>
            </Row>
          }
        </Form>}
      </React.Fragment>
    );
  }
}