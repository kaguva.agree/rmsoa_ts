import React from 'react';
import { message, Space, Card, Tabs, Button } from 'antd';
import { ColumnsType } from 'antd/lib/table';
import { connect, Dispatch, history } from 'umi';
import { PageContainer } from '@ant-design/pro-layout';
import AGrid from '@/components/SelfComp/AGrid';
import { ConnectState } from '@/models/connect';
import { DataItem } from '@/models/common';
import { getCurrentLoginUser } from '@/utils/authority';
import { encodeData, getItemValue } from '@/utils/commons';
import { getDateTime } from '@/utils/utils';
import { WORKBENCH_INIT, FETCH_CQUNDOTASK, FETCH_TRADEUNDOTASK, UPDATE_STATE, TRADE_TASK_HIS, HANDLE_TRADE_TASK } from '@/actions/workbench';
import { CqUndoTaskRecord, CqUndoTaskType, TradeTaskHandleRecord, TradeUndoTaskType } from './data.d';
import WorkbenchStatisticsCard from './components/WorkbenchStatisticsCard';
import MoreBtn from './components/MoreBtn';
import TradeTaskConfirmModal from './workbench/components/TradeTaskConfirmModal';
import { TradeTaskConfirmItem } from './data';

interface WorkbenchProps {
  dispatch: Dispatch;
  cqUndoTaskList: CqUndoTaskType[];
  tradeUndoTaskList: TradeUndoTaskType[];
  // 需求待办任务总数
  cqUndoTaskNum: number;
  // 交易待办任务总数
  tradeUndoTaskNum: number;
  // 今日已完成需求任务总数
  todayCqDoTaskNum: string;
  // 今日已完成交易任务总数
  todayTradeDoTaskNum: string;
  cqUndoTaskTotal: string;
  tradeUndoTaskTotal: string;
  cqTaskCurrentPageNum: string,
  tradeTaskCurrentPageNum: string,
  pageSize: string;
  cqUndoTaskLoading: boolean;
  tradeUndoTaskLoading: boolean;
  taskHandleLoading: boolean;
  cqUndoTaskTypeData: DataItem[];
  tradeUndoTaskTypeData: DataItem[];
  developerData: DataItem[];
  testerData: DataItem[];
}

interface WorkbenchState {
  currentTabKey: string;
  pageSize: number;
  tradeTaskConfirmModalVisible: boolean;
  tradeTaskConfirmModalTitle: string;
  formData: TradeTaskConfirmItem;
}

const { TabPane } = Tabs;
const EMPTY: TradeTaskConfirmItem = {
  taskId: '',
  status: '',
  taskName: '',
  developer: '',
  tester: '',
  comment: '',
  startDate: '',
  endDate: '',
  actInstId: '',
	actTaskId: '',
  hisList: []
}

/**
 * 工作台
 */
@connect((state: ConnectState) => ({
  cqUndoTaskList: state.workbenchs.cqUndoTaskList,
  cqUndoTaskNum: state.workbenchs.cqUndoTaskNum,
  cqUndoTaskTypeData: state.systems.cqUndoTaskTypeData,
  tradeUndoTaskList: state.workbenchs.tradeUndoTaskList,
  tradeUndoTaskNum: state.workbenchs.tradeUndoTaskNum,
  tradeUndoTaskTypeData: state.systems.tradeUndoTaskTypeData,
  developerData: state.users.developerData,
  testerData: state.users.testerData,
  cqUndoTaskLoading: state.loading.effects['workbenchs/queryCqUndoTaskList'],
  tradeUndoTaskLoading: state.loading.effects['workbenchs/queryTradeUndoTaskList'],
  taskHandleLoading: state.loading.effects['workbenchs/handleTradeTask'],
}))
export default class Workbench extends React.PureComponent<WorkbenchProps, WorkbenchState> {

  state: WorkbenchState = {
    currentTabKey: 'cqUndoTask',
    pageSize: 10,
    tradeTaskConfirmModalVisible: false,
    tradeTaskConfirmModalTitle: '',
    formData: EMPTY,
  };

  componentDidMount() {
    console.info('Workbench.componentDidMount');
    this.fetchWorkbenchInitParams();
    this.fetchCqUndoTaskList(1, 10);
    this.fetchTradeUndoTaskList(1, 10);
  }

  componentWillUnmount() {
    console.log('Workbench WILL UNMOUNT!');
    // 将state里的数据清理一下
    const result = {
      cqTaskList: [],
      tradeTaskList: [],
      cqUndoTaskNum: 0,
      tradeUndoTaskNum: 0,
      todayCqDoTaskNum: 0,
      todayTradeDoTaskNum: 0,
      cqTaskTotal: 0,
      tradeTaskTotal: 0,
      activeKey: 'cqTask',
      tradeTaskModalVisible: false,
      tradeTaskModalTitle: '',
      formData: [],
      taskType: '',
      pageSize: 10,
      cqTaskCurrentPageNum: 1,
      tradeTaskCurrentPageNum: 1,
      tradeList: [],
    }
    const { dispatch } = this.props;
    dispatch(UPDATE_STATE({
      ...result
    }));
  }

  cqUndoTaskColumns: ColumnsType<CqUndoTaskType> = [
    {
      title: '操作',
      dataIndex: 'oper',
      align: 'center',
      width: 150,
      render: (text: string, record: CqUndoTaskType, index: number) => {
        const { cqUndoTaskTypeData } = this.props;
        const { status } = record;
        return (
          <MoreBtn<CqUndoTaskType>
            record={record}
            current={status}
            menuItems={cqUndoTaskTypeData}
            menuItemClick={this.cqUndoTaskMenuItemClick}
          />
        );
      },
    },
    {
      title: '需求编号',
      dataIndex: 'cqCode',
      width: 150
    },
    {
      title: '需求名称',
      dataIndex: 'cqName',
      width: 300,
      ellipsis: true
    },
    {
      title: '所属系统',
      dataIndex: 'subSystem',
      width: 120
    },
    {
      title: '所属集群',
      dataIndex: 'clstCode',
      width: 120
    },
    {
      title: '所属应用',
      dataIndex: 'appCode',
      width: 120
    },
    {
      title: '任务名称',
      dataIndex: 'taskName',
    },
    {
      title: '任务创建时间',
      dataIndex: 'taskCreateTime',
    },
  ];

  tradeUndoTaskColumns: ColumnsType<TradeUndoTaskType> = [
    {
      title: '操作',
      dataIndex: 'oper',
      align: 'center',
      width: 150,
      render: (text: string, record: TradeUndoTaskType, index: number) => {
        const { tradeUndoTaskTypeData } = this.props;
        const { status } = record;
        return (
          <MoreBtn<TradeUndoTaskType>
            record={record}
            current={status}
            menuItems={tradeUndoTaskTypeData}
            menuItemClick={this.tradeUndoTaskMenuItemClick}
          />
        );
      },
    },
    {
      title: '交易功能码',
      dataIndex: 'funcCode',
      width: 150,
    },
    {
      title: '交易任务名称',
      dataIndex: 'taskName',
      width: 300,
      ellipsis: true
    },
    {
      title: '需求编号',
      dataIndex: 'cqCode',
      width: 150
    },
    {
      title: '开始时间',
      dataIndex: 'startDate',
      width: 120
    },
    {
      title: '结束时间',
      dataIndex: 'endDate',
      width: 120
    },
    {
      title: '开发人员',
      dataIndex: 'developer',
      width: 120
    },
    {
      title: '测试人员',
      dataIndex: 'tester',
      width: 120
    },
    {
      title: '任务创建时间',
      dataIndex: 'taskCreateTime',
    },
  ];

  /**
   * 需求代办任务点击函数
   * @param key 菜单key
   * @param record 菜单所在表格记录行
   */
  cqUndoTaskMenuItemClick = (key: string, record: CqUndoTaskType) => {
    console.info('menuItemClick', key, record);
    console.info('cqUndoTaskTableHandler');
    const { status } = record;
    let url = '404';
    switch(status)
    {
      case '1':
        // 录入需求详细信息
        url = '/dashboard/workbench/fillCqDetailInfo';
        break;
      case '2':
        // 录入文档信息
        url = '/dashboard/workbench/fillCqDocInfo';
        break;
      case '3':
        // 分配交易
        url = '/dashboard/workbench/allotCqTrade';
        break;
      default:
        // 404
        // url = ''
    }
    // 当前任务的处理人
    const userCode = getCurrentLoginUser();
    const params: CqUndoTaskRecord = {
      ...record,
      userCode
    }
    const e = encodeData(params);
    history.push({
      pathname: url,
      query: {
        e
      }
    });
  }

  /**
   * 交易代办任务点击函数
   * @param key 菜单key
   * @param record 菜单所在表格记录行
   */
  tradeUndoTaskMenuItemClick = (key: string, record: TradeUndoTaskType) => {
    console.info('menuItemClick', key, record);
    console.info('tradeUndoTaskTableHandler');
    console.info(record);
    this.openTradeTaskConfirmModal(record);
  }

  /**
   * 打开交易任务确认窗口
   */
  openTradeTaskConfirmModal = async (record: TradeUndoTaskType) => {
    const { taskId } = record;
    // 需要查询出任务的历史记录
    // 查询成功，才会弹窗
    const { dispatch } = this.props;
    const res = await dispatch(TRADE_TASK_HIS({
      taskId
    }));
    console.info(res);
    if (res) {
      const { hisList } = res;
      const { funcCode, taskName, developer, tester, startDate, endDate, comment, status, actInstId, actTaskId } = record;
      const formData: TradeTaskConfirmItem = {
        taskId,
        status,
        taskName,
        developer,
        tester,
        comment,
        startDate,
        endDate,
        actInstId,
        actTaskId,
        hisList
      };
      const tradeTaskConfirmModalTitle = this.getTradeTaskConfirmModalTitle(funcCode, status);
      this.setState({
        tradeTaskConfirmModalTitle,
        tradeTaskConfirmModalVisible: true,
        formData
      });
    }
  }

  /**
   * 获取交易任务窗口标题
   */
  getTradeTaskConfirmModalTitle = (funcCode: string, status: string) => {
    const { tradeUndoTaskTypeData } = this.props;
    const modalTitle = getItemValue(tradeUndoTaskTypeData, status);
    return `交易${funcCode}${modalTitle}窗口`;
  }

  fetchWorkbenchInitParams = () => {
    const { dispatch } = this.props;
    dispatch(WORKBENCH_INIT({}));
  }

  /**
   * 分页查询需求待办任务
   * @param pageNum 页码
   * @param pageSize 每页记录数
   */
  fetchCqUndoTaskList = (pageNum: number, pageSize: number) => {
    const userCode = getCurrentLoginUser();
    const { dispatch } = this.props;
    dispatch(FETCH_CQUNDOTASK({
      userCode,
      pageNum,
      pageSize
    }));
  }

  /**
   * 分页查询交易待办任务
   * @param pageNum 页码
   * @param pageSize 每页记录数
   */
  fetchTradeUndoTaskList = (pageNum: number, pageSize: number) => {
    const userCode = getCurrentLoginUser();
    const { dispatch } = this.props;
    dispatch(FETCH_TRADEUNDOTASK({
      userCode,
      pageNum,
      pageSize
    }));
  }

  /**
   * 页面统计数据面板点击函数
   * @param boxType 面板类型，对应下方不同Tab的页签
   */
  cardClick = (boxType: string) => {
    const { currentTabKey } = this.state;
    console.info('Workbench.onClick', boxType, currentTabKey);
    this.changeTabs(boxType);
  }

  /**
   * 切换TAB页
   */
  changeTabs = (key: string) => {
    // 改变Tab
    console.log(`changeTabs=${key}`);
    const { currentTabKey } =this.state;
    if (key !== currentTabKey) {
      this.setState({
        currentTabKey: key
      });
    }
  }

  /**
   * 刷新任务按钮事件
   */
  refreshUndoTaskList = () => {
    this.fetchCqUndoTaskList(1, 10);
    this.fetchTradeUndoTaskList(1, 10);
  }

  /**
   * 页码改变的回调，参数是改变后的页码及每页条数
   * @param page 改变后的页码，上送服务器端
   * @param pageSize 每页数据条数
   */
  onPageNumAndSizeChange = (page: number, pageSize: number) => {
    console.log(page, pageSize);
    const { currentTabKey } = this.state;
    if (currentTabKey === 'cqUndoTask') {
      this.setState({
        pageSize
      }, () => {
        this.fetchCqUndoTaskList(page, pageSize);
      });
    } else if (currentTabKey === 'tradeUndoTask') {
      this.setState({
        pageSize
      }, () => {
        this.fetchTradeUndoTaskList(page, pageSize);
      });
    }
  };

  /**
   * 确认任务，驳回任务，只有通讯成功才会关闭窗口
   */
  handleTradeTaskConfirmModalOk = (record: TradeTaskHandleRecord) => {
    console.info('handleTradeTaskConfirmModalOk', record);
    // 异步
    this.tradeTaskHandler(record);
  }

  /**
   * 使用await直接调用service
   * @param {*} record 数据
   * @returns 交易任务处理结果
   */
  tradeTaskHandler = async (record: TradeTaskHandleRecord) => {
    const userCode = getCurrentLoginUser();
    const { dispatch } = this.props;
    const res = await dispatch(HANDLE_TRADE_TASK({
      userCode,
      ...record
    }));
    console.info(res);
    if (res) {
      const { agree } = record;
      if (agree) {
        message.info('任务确认成功');
      } else {
        message.info('任务驳回成功');
      }
      this.setState({
        tradeTaskConfirmModalVisible: false,
      }, () => {
        // 重新查询
        this.fetchTradeUndoTaskList(1, 10);
      });
    }
  }

  /**
   * 弹窗取消事件，无动作，仅清空弹窗数据
   */
  handleTradeTaskConfirmModalCancel = () => {
    console.info('handleTradeTaskModalCancel');
    this.setState({
      tradeTaskConfirmModalVisible: false,
    });
  }

  render() {
    console.info('TaskCenter.render');
    // 需求任务数据
    const { cqUndoTaskList, cqUndoTaskNum, cqUndoTaskLoading } = this.props;
    const { tradeUndoTaskList, tradeUndoTaskNum, tradeUndoTaskLoading, taskHandleLoading } = this.props;
    const { developerData, testerData } = this.props;
    const { currentTabKey, tradeTaskConfirmModalVisible, tradeTaskConfirmModalTitle, formData } = this.state;
    const cqUndoTaskCode = 'cqUndoTask';
    const tradeUndoTaskCode = 'tradeUndoTask';
    const cqUndoRowKey = (record: CqUndoTaskType) => record.actTaskId;
    const tradeUndoRowKey = (record: TradeUndoTaskType) => record.actTaskId;
    const pkField = 'actTaskId';
    const dateTimeStr = getDateTime();

    return (
      <PageContainer
        extra={dateTimeStr}
      >
        <Space direction='vertical' size='middle' style={{ display: 'flex' }}>
          <WorkbenchStatisticsCard
            cqUndoTaskNum={cqUndoTaskNum}
            todayCqDoTaskNum={0}
            tradeUndoTaskNum={tradeUndoTaskNum}
            todayTradeDoTaskNum={0}
            cqTaskLoading={cqUndoTaskLoading}
            tradeTaskLoading={tradeUndoTaskLoading}
            onClick={this.cardClick}
          />
          <Card title='待办任务列表'
            extra={<Button type="link" onClick={() => this.refreshUndoTaskList()}>刷新任务</Button>}
          >
            <Tabs
              defaultActiveKey="cqUndoTask"
              onChange={this.changeTabs}
              activeKey={currentTabKey}
            >
              <TabPane tab="需求待办任务" key="cqUndoTask">
                <AGrid
                  code={cqUndoTaskCode}
                  noActionColumn={true}
                  columns={this.cqUndoTaskColumns}
                  rowKey={cqUndoRowKey}
                  pkField={pkField}
                  dataSource={cqUndoTaskList}
                  loading={cqUndoTaskLoading}
                  total={cqUndoTaskNum}
                  onPageNumAndSizeChange={this.onPageNumAndSizeChange}
                />
              </TabPane>
              <TabPane tab="交易待办任务" key="tradeUndoTask">
                <AGrid
                  code={tradeUndoTaskCode}
                  noActionColumn={true}
                  columns={this.tradeUndoTaskColumns}
                  rowKey={tradeUndoRowKey}
                  pkField={pkField}
                  dataSource={tradeUndoTaskList}
                  loading={tradeUndoTaskLoading}
                  total={tradeUndoTaskNum}
                  onPageNumAndSizeChange={this.onPageNumAndSizeChange}
                />
              </TabPane>
            </Tabs>
          </Card>
        </Space>
        {
          !tradeTaskConfirmModalVisible ? null :
          <TradeTaskConfirmModal
            colon={false}
            modalTitle={tradeTaskConfirmModalTitle}
            modalWidth={1100}
            modalVisible={tradeTaskConfirmModalVisible}
            loading={taskHandleLoading}
            formData={formData}
            developerData={developerData}
            testerData={testerData}
            onHandlerOK={this.handleTradeTaskConfirmModalOk}
            onHandlerCancel={this.handleTradeTaskConfirmModalCancel}
          />
        }
      </PageContainer>
    );
  }
}