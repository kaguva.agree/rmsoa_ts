import { RoleItem } from "../role/data";

/**
 * 用户
 */
export interface UserItem {
  /** 用户id */
  userId: string;
  /** 用户账号 */
  userCode: string;
  /** 姓名 */
  userName: string;
  /** 状态 */
  userStates?:string;
  /** 性别 */
  sex?: string;
  /** 电子邮箱 */
  email?: string;
  /** 座机 */
  telephone?: string;
  /** 手机号码 */
  mobile: string;
  /** 座位号 */
  seatNo?: string;
  /** 电脑编号 */
  computerNo?: string;
  /** 上一次登录时间 */
  loginTime?: string;
  /** 上一次登录IP */
  loginHost?: string;
  /** 虚拟机桌面账号 */
  virtualCode: string;
}

/**
 * 用户分配角色
 */
export interface AssignRoleItem {
  /** 用户id */
  userId: string;
  /** 角色id集合 */
  roleIds: string[];
}

/**
 * 重置用户密码
 */
export interface ResetUserPassItem {
  /** 用户id */
  userId: string;
}

/**
 * 用户修改密码
 */
export interface UpdateUserPassItem {
  /** 用户id */
  userId: string;
  /** 旧密码 */
  oldPass: string;
  /** 新密码 */
  newPass: string;
}

/**
 * 分配角色弹窗所需要的值
 */
export interface AssignUserRoleData {
  userId: React.Key;
  allRoleData: RoleData[];
  assignedRoleData: string[];
}

export interface RoleData {
  key: string;
  title: string;
  description: string;
}

