import React from 'react';
import { Modal,  message, Button } from 'antd';
import { connect, Dispatch } from 'umi';
import { PageContainer } from '@ant-design/pro-layout';
import ProCard from '@ant-design/pro-card';
import AGrid, { AGridButtonCallBackModel } from '@/components/SelfComp/AGrid';
import AButton from '@/components/SelfComp/AButton';
import { ConnectState } from '@/models/connect';
import { DataItem, FlagEnum } from '@/models/common';
import { USER_LIST, USER_ADD, USER_UPDATE, USER_DELETE, USER_ROLE, USER_ASSIGN_ROLE, USER_RESET_PASS, UPDATE_STATE } from '@/actions/user';
import UserQueryForm from './components/UserQueryForm';
import AddUserModal from './components/AddUserModal';
import UpdateUserModal from './components/UpdateUserModal';
import AssignRoleModal from './components/AssignRoleModal';
import { AssignRoleItem, AssignUserRoleData, UserItem } from './data';

interface UserListProps {
  dispatch: Dispatch;
  userList: UserItem[],
  pageSize: number;
  total: number,
  loading: boolean;
  addLoading: boolean;
  updateLoading: boolean;
  previewLoading: boolean;
  assignLoading: boolean;
  resetLoading: boolean;
  sexData: DataItem[];
  statusData: DataItem[];
}

interface UserListState {
  addModalVisible: boolean;
  userFormData: UserItem;
  updateModalVisible: boolean;
  assignRoleModalVisible: boolean;
  // 表格选择的数据
  selectedRows: UserItem[];
  selectedRowKeys: React.Key[];
  queryParam?: UserItem;
  flag: FlagEnum;
  pageSize: number;
  importModalVisible: false,
  fileList: [],
  roleFormData: AssignUserRoleData;
}

const EMPTY: UserItem = {
  userId: '',
  userCode: '',
  userName: '',
  userStates: '',
  sex: '',
  email: '',
  telephone: '',
  mobile: '',
  seatNo: '',
  computerNo: '',
  loginTime: '',
  loginHost: '',
  virtualCode: ''
}

const EMPTY_ASSIGN_ROLE: AssignUserRoleData = {
  userId: '',
  allRoleData: [],
  assignedRoleData: []
}

/**
 * 应用管理，管理项目中的系统应用，以便职责分明
 */
@connect((state: ConnectState) => ({
  userList: state.users.rows,
  roleList: state.roles.rows,
  total: state.users.total,
  pageSize: state.users.pageSize,
  sexData: state.systems.sexData,
  statusData: state.systems.statusData,
  loading: state.loading.effects['users/fetchAllUser'],
  addLoading: state.loading.effects['users/addUser'],
  updateLoading: state.loading.effects['users/updateUser'],
  previewLoading: state.loading.effects['users/fetchRolesByUser'],
  assignLoading: state.loading.effects['users/assignRolesToUser'],
}))
export default class UserList extends React.PureComponent<UserListProps, UserListState> {

  state: UserListState = {
    addModalVisible: false,
    userFormData: EMPTY,
    updateModalVisible: false,
    assignRoleModalVisible: false,
    // 表格选择的数据
    selectedRows: [],
    selectedRowKeys: [],
    queryParam: EMPTY,
    flag: 'edit',
    pageSize: 10,
    importModalVisible: false,
    fileList: [],
    roleFormData: EMPTY_ASSIGN_ROLE,
  };

  columns = [
    {
      title: '用户姓名',
      dataIndex: 'userName',
      width: 120
    },
    {
      title: '用户账号',
      dataIndex: 'userCode',
      width: 150
    },
    {
      title: '性别',
      dataIndex: 'sex',
      width: 80
    },
    {
      title: '状态',
      dataIndex: 'userStates',
      width: 100
    },
    {
      title: '电子邮箱',
      dataIndex: 'email',
      width: 200
    },
    {
      title: '手机号码',
      dataIndex: 'mobile',
      width: 150
    },
    {
      title: '虚拟机账号',
      dataIndex: 'virtualCode',
      width: 150
    },
    {
      title: '电脑编号',
      dataIndex: 'computerNo',
      width: 150
    },
    {
      title: '座位号',
      dataIndex: 'seatNo',
      width: 150
    },
    {
      title: '座机号',
      dataIndex: 'telephone',
      width: 150
    },
  ];

  componentDidMount() {
    console.log('UserList.componentDidMount');
  }

  componentWillUnmount() {
    console.log('UserList WILL UNMOUNT!');
    // 清空state
    const { dispatch } = this.props;
    console.info(dispatch);
    dispatch(UPDATE_STATE({
      rows: [],
      total: 0
    }));
  }

    /**
   * 查询满足条件的数据字典
   */
  handleSubmitEx = (record: UserItem) => {
    console.info('UserList.handleSubmitEx');
    console.info(record);
    const queryParam = {
      ...record
    };
    // 查询条件保存起来，方便点击页码时使用
    // setState回调方法，保证先更新state
    this.setState({
      queryParam
    }, () => {
      const { pageSize } = this.state;
      this.fetchAllUser(1, pageSize);
    });
  }

  handleReset = () => {
    // 是否清空表格
  }

  openAddUserModal = () => {
    this.setState({ addModalVisible: true });
  }

  handleAddModalOk = async (record: UserItem) => {
    console.log(record);
    const { dispatch } = this.props;
    console.info(dispatch);
    const res = await dispatch(USER_ADD(record));
    console.info(res);
    if (res) {
      this.setState({
        addModalVisible: false
      });
      const { pageSize } = this.props;
      this.fetchAllUser(1, pageSize);
    }
  }

  handleAddModalCancel = () => {
    this.setState({ addModalVisible: false });
  };

  openUpdateUserModal = (record: UserItem[], flag: FlagEnum) => {
    // 表格选中内容
    console.info('openUpdateUserModal');
    console.info(record);
    const row = record[0];
    const userFormData = {
      ...row,
    };
    this.setState({
      userFormData,
      updateModalVisible: true,
      flag
    });
  }

  /**
   * 更新面板的确定事件
   * @param {*} record 更新表单
   */
  handleUpdateModalOk = async (record: UserItem) => {
    console.log('handleUpdateModalOk');
    console.log(record);
    const { userFormData, flag } = this.state;
    console.info(userFormData);
    if (flag === 'edit') {
      const { dispatch } = this.props;
      const res = await dispatch(USER_UPDATE(record));
      console.info(res);
      if (res) {
        this.setState({
          updateModalVisible: false,
          userFormData: EMPTY
        });
        const { pageSize } = this.props;
        this.fetchAllUser(1, pageSize);
      }
    } else {
      this.setState({
        updateModalVisible: false,
        userFormData: EMPTY
      });
    }
  }

  handleUpdateModalCancel = () => {
    this.setState({
      updateModalVisible: false,
      userFormData: EMPTY
    });
  };

  /**
   * 分页查询系统参数信息
   * @param {*} pageNum 页码
   * @param {*} pageSize 每页记录条数
   */
  fetchAllUser = (pageNum: number, pageSize: number) => {
    const { queryParam } = this.state;
    const { dispatch } = this.props;
    dispatch(USER_LIST({
      ...queryParam,
      pageSize,
      pageNum
    }));
  }

  /**
   * 删除用户，暂时只支持单笔删除
   * @param userId 用户id
   */
  deleteUsers = async (userId: string) => {
    const { dispatch } = this.props;
    const res = await dispatch(USER_DELETE(userId));
    if (res) {
      const { pageSize } = this.props;
      const { queryParam } = this.state;
      dispatch(USER_LIST({
        ...queryParam,
        pageSize,
        pageNum: 1,
      }));
    }
  }

  /**
   * 处理按钮点击回调事件
   * @param {*} payload 数据包
   */
  handleBtnCallBack = (callBackModel: AGridButtonCallBackModel<UserItem>) => {
    console.log('handleBtnCallBack');
    console.log(callBackModel);
    // btn 按钮
    // keys 表格勾选数组
    const { btn } = callBackModel;
    const { alias } = btn;
    // 新增
    if (alias === 'add') {
      this.openAddUserModal();
      return;
    }
    // 修改
    if (alias === 'edit') {
      const { rows } = callBackModel;
      this.openUpdateUserModal(rows, 'edit');
      return;
    }
    // 删除
    if (alias === 'delete') {
      const { keys } = callBackModel;
      // 调用删除服务，删除勾选数据
      const userId = keys[0];
      this.deleteUsers(userId as string);
      return;
    }
    // 查看
    if (alias === 'view') {
      const { rows } = callBackModel;
      this.openUpdateUserModal(rows, 'view');
      return;
    }
  }

  /**
   * 表格勾选回调函数
   * @param {*} rows 表格选中数据集合
   */
  onSelectRow = (keys: React.Key[], rows: UserItem[]) => {
    this.setState({
      selectedRows: rows,
      selectedRowKeys: keys
    });
  };

  /**
   * 页码改变的回调，参数是改变后的页码及每页条数
   * @param page 改变后的页码，上送服务器端
   * @param pageSize 每页数据条数
   */
  onPageNumAndSizeChange = (page: number, pageSize: number) => {
    console.log(page, pageSize);
    this.setState({
      pageSize
    }, () => {
      this.fetchAllUser(page, pageSize);
    });
  };

  renderLeftButton = () => {
    const { previewLoading, resetLoading } = this.props;
    return (
      <>
        <AButton type='primary' code='assignRole' pageCode='user-list' name='分配角色' onClick={this.openAssignRoleModal} loading={previewLoading} />
        <AButton danger code='resetPass' pageCode='user-list' name='重置密码' onClick={this.handleResetUserPassword} loading={resetLoading} />
      </>
    );
  }

  /**
   * 角色授权，打开角色授权窗口
   */
  openAssignRoleModal = async () => {
    // 表格选中内容
    console.info('openAssignRoleModal');
    const { selectedRowKeys } = this.state;
    if (selectedRowKeys.length === 0) {
      message.warn('请先选择一条数据!');
      return;
    }
    if (selectedRowKeys.length > 1) {
      message.warn('只能选择一条数据!');
      return;
    }
    // 获取用户id
    const userId = selectedRowKeys[0];
    console.info('userId', userId);
    const { dispatch } = this.props;
    const res = await dispatch(USER_ROLE({
      userId
    }));
    if (res) {
      // 查询菜单权限树，并根据勾选角色ID对应已分配的权限
      // 这里试试dispatch的回调方法
      // 只有查询成功才会弹窗
      const { result } = res;
      const { allRoleData, assignedRoleData } = result;
      const roleFormData = {
        userId,
        allRoleData,
        assignedRoleData
      };
      this.setState({
        roleFormData,
        assignRoleModalVisible: true
      });
    }
  }

  handleAssignModalOk = async (record: AssignRoleItem) => {
    console.log('handleAssignModalOk');
    console.log(record);
    const { dispatch } = this.props;
    const res = await dispatch(USER_ASSIGN_ROLE(record));
    if (res) {
      this.setState({
        assignRoleModalVisible: false,
        roleFormData: EMPTY_ASSIGN_ROLE
      });
    }
  }

  handleAssignModalCancel = () => {
    this.setState({
      assignRoleModalVisible: false,
      roleFormData: EMPTY_ASSIGN_ROLE
    });
  }

  handleResetUserPassword = () => {
    const { selectedRowKeys } = this.state;
    if (selectedRowKeys.length <= 0) {
      message.warn('请至少选择一条数据!');
      return;
    }
    Modal.confirm({
      title: '重置确认',
      content: '确定重置选中用户密码？',
      okText: '确定',
      okType: 'danger',
      cancelText: '取消',
      onOk: () => this.resetUserPassword(selectedRowKeys),
      onCancel() {},
    });
    return;
  }

  resetUserPassword = async (rows: React.Key[]) => {
    console.log('resetUserPassword');
    const { dispatch } = this.props;
    const res = await dispatch(USER_RESET_PASS(rows));
    console.log(res);
    if (res) {
      message.success('用户密码重置成功');
    }
  }

  renderRightButton = () => {
    return (
      <>
        <Button>导入</Button>
        <Button>导出</Button>
      </>
    );
  }

  render() {
    console.info('UserList.render');
    const code = 'user-list';
    const { userList, loading, total, addLoading, updateLoading, assignLoading, sexData, statusData } = this.props;
    const { userFormData, roleFormData } = this.state;
    const { addModalVisible, updateModalVisible, assignRoleModalVisible, flag } = this.state;
    const rowKey = (record: UserItem) => {
      return record.userId;
    }
    const pkField = 'userId';

    return (
      <PageContainer>
        <ProCard title="用户列表" headerBordered>
          <UserQueryForm
            colon={false}
            loading={loading}
            onSubmit={this.handleSubmitEx}
            onReset={this.handleReset}
          />
          <AGrid
            code={code}
            btnCallBack={this.handleBtnCallBack}
            renderLeftButton={this.renderLeftButton}
            renderRightButton={this.renderRightButton}
            columns={this.columns}
            rowKey={rowKey}
            pkField={pkField}
            dataSource={userList}
            loading={loading}
            total={total}
            onSelectRow={this.onSelectRow}
            onPageNumAndSizeChange={this.onPageNumAndSizeChange}
          />
        </ProCard>
        <AddUserModal
          colon={false}
          modalTitle='新增用户'
          modalWidth={1000}
          modalVisible={addModalVisible}
          loading={addLoading}
          sexData={sexData}
          statusData={statusData}
          onHandlerOK={this.handleAddModalOk}
          onHandlerCancel={this.handleAddModalCancel}
        />
        <UpdateUserModal
          colon={false}
          modalTitle=''
          modalWidth={1000}
          modalVisible={updateModalVisible}
          flag={flag}
          loading={updateLoading}
          formData={userFormData}
          sexData={sexData}
          statusData={statusData}
          onHandlerOK={this.handleUpdateModalOk}
          onHandlerCancel={this.handleUpdateModalCancel}
        />
        <AssignRoleModal
          colon={false}
          modalTitle='分配角色'
          modalWidth={450}
          modalVisible={assignRoleModalVisible}
          loading={assignLoading}
          formData={roleFormData}
          onHandlerOK={this.handleAssignModalOk}
          onHandlerCancel={this.handleAssignModalCancel}
        />
      </PageContainer>
    );
  }
}