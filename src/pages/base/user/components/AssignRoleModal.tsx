import React from 'react';
import { Modal, Transfer } from 'antd';
import { FormModalProps } from '@/models/FormModal';
import { AssignUserRoleData } from '../data';
import { TransferDirection } from 'antd/lib/transfer';

type AssignRoleProps<T> = FormModalProps<T> & {
}

type AssignRoleState =  {
  targetKeys: string[]
}

export default class AssignRoleModal extends React.PureComponent<AssignRoleProps<AssignUserRoleData>, AssignRoleState> {

  state: AssignRoleState = {
    targetKeys: [],
  }

  componentDidMount() {
    console.info('AssignRoleModal.componentDidMount');
  }

  onOk = () => {
    const { formData } = this.props;
    const { userId } = formData;
    const { targetKeys } = this.state;
    const { onHandlerOK } = this.props;
    if (onHandlerOK) {
      const submitData = {
        userId,
        roleIds: [...targetKeys]
      }
      onHandlerOK({
        ...submitData
      });
    }
  };

  onCancel = () => {
    const {onHandlerCancel} = this.props;
    // 先清除form表单
    if (onHandlerCancel) {
      onHandlerCancel();
    }
  }

  afterClose = () => {
    console.info('afterClose');
    this.setState({
      targetKeys: [],
    });
  }

  onChange = (targetKeys: string[], direction: TransferDirection, moveKeys: string[]) => {
    console.log('targetKeys:', targetKeys);
    console.log('direction:', direction);
    console.log('moveKeys:', moveKeys);
    // setTargetKeys(nextTargetKeys);
    this.setState({
      targetKeys: [...targetKeys]
    });
  };

  onSelectChange = (sourceSelectedKeys: string[], targetSelectedKeys: string[]) => {
    console.log('sourceSelectedKeys:', sourceSelectedKeys);
    console.log('targetSelectedKeys:', targetSelectedKeys);
    // setSelectedKeys([...sourceSelectedKeys, ...targetSelectedKeys]);
  };

  render() {
    console.info("AssignRoleModal.render");
    const { formData, modalWidth, modalVisible, modalTitle, loading } = this.props;
    const { allRoleData, assignedRoleData } = formData;
    const { targetKeys } = this.state;
    // 处理已分配角色的问题
    // 如果 assignedRoleData 有值，则不看 state 中的 targetKeys
    // 如果 assignedRoleData 没有值，则看 state 中的 targetKeys
    let tmpTargetKeys: string[] = [];
    // 优先看 checkedKeys
    if (targetKeys && targetKeys.length > 0) {
      tmpTargetKeys = [...targetKeys];
    } else if (assignedRoleData && assignedRoleData.length > 0) {
      tmpTargetKeys = [...assignedRoleData];
      // 若子节点全部存在，则父节点会自动勾选
      // tmpCheckedKeys.push('23');
      // tmpCheckedKeys.push('24');
      // tmpCheckedKeys.push('25');
      // tmpCheckedKeys.push('31');
    } else {
      // tmpCheckedKeys = [...checkedKeys];
    }

    return (
      <Modal
        title={modalTitle}
        destroyOnClose
        width={modalWidth}
        open={modalVisible}
        confirmLoading={loading}
        onOk={this.onOk}
        onCancel={this.onCancel}
        afterClose={this.afterClose}>
        <Transfer
          dataSource={allRoleData}
          titles={['全部角色', '已分配角色']}
          targetKeys={tmpTargetKeys}
          render={item => item.title}
          pagination
          onChange={this.onChange}
          onSelectChange={this.onSelectChange}
        />
      </Modal>
    );
  }
}