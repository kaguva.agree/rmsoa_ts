import React from 'react';
import { Form, Input } from 'antd';
import { basicFormItemLangLayout } from '@/utils/commons';
import QueryFilterForm from '@/components/SelfComp/QueryFilterForm';
import { QueryFormProps } from '@/models/FormModal';

export type UserQueryProps = QueryFormProps & {
}

const FormItem = Form.Item;

class UserQueryForm extends React.PureComponent<UserQueryProps> {

  render() {
    const { colon, loading, onSubmit, onReset } = this.props;
    const formItemLayout = basicFormItemLangLayout;

    return (
      <QueryFilterForm
        colon={colon}
        loading={loading}
        onSubmit={onSubmit}
        onReset={onReset}
      >
        <FormItem label="用户姓名" name='userName' {...formItemLayout} colon={colon}>
          <Input placeholder='支持模糊查询'/>
        </FormItem>
        <FormItem label="用户账号" name='userCode' {...formItemLayout} colon={colon}>
          <Input />
        </FormItem>
      </QueryFilterForm>
    );
  }
}

export default UserQueryForm;