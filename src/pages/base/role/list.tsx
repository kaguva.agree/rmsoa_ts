import React from 'react';
import { connect, Dispatch } from 'umi';
import { message } from 'antd';
import { PageContainer } from '@ant-design/pro-layout';
import ProCard from '@ant-design/pro-card';
import AGrid, { AGridButtonCallBackModel } from '@/components/SelfComp/AGrid';
import AButton from '@/components/SelfComp/AButton';
import { ConnectState } from '@/models/connect';
import { FlagEnum } from '@/models/common';
import { ROLE_LIST, ROLE_ADD, ROLE_UPDATE, ROLE_DELETE, MENU_TREE, GRANT_AUTH } from '@/actions/role';
import RoleQueryForm from './components/RoleQueryForm';
import AddRoleModal from './components/AddRoleModal';
import UpdateRoleModal from './components/UpdateRoleModal';
import GrantAuthModal from './components/GrantAuthModal';
import { AuthTreeData, GrantAuthItem, RoleItem } from './data';

interface RoleListProps {
  dispatch: Dispatch;
  roleList: RoleItem[],
  total: number,
  loading: boolean;
  pageSize: number;
  addLoading: boolean;
  updateLoading: boolean;
  previewLoading: boolean;
  grantLoading: boolean;
}

interface RoleListState {
  addModalVisible: boolean;
  roleFormData: RoleItem;
  updateModalVisible: boolean;
  // 表格选择的数据
  selectedRows: RoleItem[];
  selectedRowKeys: React.Key[];
  queryParam?: RoleItem;
  flag: FlagEnum;
  roleAuthModalVisible: boolean;
  authTreeData: AuthTreeData;
}

const EMPTY: RoleItem = {
  roleId: '',
  roleCode: '',
  roleName: '',
  roleDesc: '',
}

const EMPTY_TREE: AuthTreeData = {
  roleId: '',
  grantKeys: [],
  menuTree: [],
}

@connect((state: ConnectState) => ({
  pageSize: state.roles.pageSize,
  total: state.roles.total,
  roleList: state.roles.rows,
  loading: state.loading.effects['roles/fetchAllRole'],
  addLoading: state.loading.effects['roles/addRole'],
  updateLoading: state.loading.effects['roles/updateRole'],
  previewLoading: state.loading.effects['roles/fetchMenuTreeByRole'],
  grantLoading: state.loading.effects['roles/grantAuths'],
}))
export default class RoleList extends React.PureComponent<RoleListProps, RoleListState> {

  state: RoleListState = {
    addModalVisible: false,
    roleFormData: EMPTY,
    updateModalVisible: false,
    // 表格选择的数据
    selectedRows: [],
    selectedRowKeys: [],
    queryParam: EMPTY,
    roleAuthModalVisible: false,
    authTreeData: EMPTY_TREE,
    flag: 'edit',
  };

  columns = [
    {
      title: '角色编码',
      dataIndex: 'roleCode',
      width: 150
    },
    {
      title: '角色名称',
      dataIndex: 'roleName',
      width: 200
    },
    {
      title: '角色描述',
      dataIndex: 'roleDesc',
    }
  ];

  constructor(props: RoleListProps) {
    super(props);
    console.log('RoleList.constructor');
  }

  componentDidMount() {
    console.info('RoleList.componentDidMount');
    // 默认查询第一页
    const { pageSize } = this.props;
    this.fetchAllRole(1, pageSize);
  }

  /**
   * 打开角色新增窗口
   */
  openAddRoleModal = () => {
    this.setState({ addModalVisible: true });
  }

  /**
   * 修改角色，打开角色修改窗口
   */
  openUpdateRoleModal = (records: RoleItem[], flag: string) => {
    // 表格选中内容
    console.info('openUpdateRoleModal');
    console.info(records);
    const row = records[0];
    const roleFormData = {
      ...row,
      flag
    };
    console.info(roleFormData);
    this.setState({
      roleFormData,
      updateModalVisible: true
    });
  }

  handleAddModalOk = async (record: RoleItem) => {
    console.log(record);
    const { dispatch } = this.props;
    const res = await dispatch(ROLE_ADD(record));
    console.info(res);
    if (res) {
      this.setState({
        addModalVisible: false
      });
      const { pageSize } = this.props;
      this.fetchAllRole(1, pageSize);
    }
  }

  handleAddModalCancel = () => {
    this.setState({ addModalVisible: false });
  };

  handleUpdateModalOk = async (record: RoleItem) => {
    console.log('handleUpdateModalOk');
    console.log(record);
    const { flag } = this.state;
    if (flag === 'edit') {
      const { dispatch } = this.props;
      const res = await dispatch(ROLE_UPDATE(record));
      console.info(res);
      if (res) {
        this.setState({
          updateModalVisible: false,
          roleFormData: EMPTY
        });
        const { pageSize } = this.props;
        this.fetchAllRole(1, pageSize);
      }
    } else {
      this.setState({
        updateModalVisible: false,
        roleFormData: EMPTY
      });
    }
  }

  handleUpdateModalCancel = () => {
    this.setState({
      updateModalVisible: false,
      roleFormData: EMPTY
    });
  }

  deleteRoles = async (keys:  React.Key[]) => {
    const { dispatch } = this.props;
    const res = await dispatch(ROLE_DELETE(keys));
    if (res) {
      const { pageSize } = this.props;
      this.fetchAllRole(1, pageSize);
    }
  }

  /**
   * 根据条件分页查询表格数据
   * @param {*} record 条件
   */
  handleFetchRole = (record: RoleItem) => {
    console.log(record);
    const queryParam = {
      ...record
    };
    // 查询条件保存起来，方便点击页码时使用
    this.setState({
      queryParam
    }, () => {
      const { pageSize } = this.props;
      this.fetchAllRole(1, pageSize);
    });
  }

  /**
   * 页码改变的回调，参数是改变后的页码及每页条数
   * @param page 改变后的页码，上送服务器端
   * @param pageSize 每页数据条数
   */
  onPageNumAndSizeChange = (pageNum: number, pageSize: number) => {
    console.log(pageNum, pageSize);
    this.fetchAllRole(pageNum, pageSize);
  };

  /**
   * 分页查询角色信息
   * @param {*} pageSize 每页记录条数
   * @param {*} pageNum 页码
   */
  fetchAllRole = (pageNum: number, pageSize: number) => {
    const { queryParam } = this.state;
    const { dispatch } = this.props;
    dispatch(ROLE_LIST({
      ...queryParam,
      pageSize,
      pageNum
    }));
  }

  /**
   * 处理按钮点击回调事件
   * @param {*} payload 数据包
   */
  handleBtnCallBack = (callBackModel: AGridButtonCallBackModel<RoleItem>) => {
    // btn 按钮
    // keys 表格勾选数组
    const { btn, keys } = callBackModel;
    console.info(btn);
    console.info(keys);
    const { alias } = btn;
    // 新增
    if (alias === 'add') {
      this.openAddRoleModal();
      return;
    }
    // 修改
    if (alias === 'edit') {
      const { rows } = callBackModel;
      this.openUpdateRoleModal(rows, 'edit');
      return;
    }
    // 删除
    if (alias === 'delete') {
      // 调用删除服务，删除勾选数据
      console.info(keys);
      this.deleteRoles(keys);
      return;
    }
    // 查看
    if (alias === 'view') {
      const { rows } = callBackModel;
      this.openUpdateRoleModal(rows, 'view');
      return;
    }
  }

  /**
   * 表格勾选回调函数
   * @param {*} rows 表格选中数据集合
   */
  onSelectRow = (keys: React.Key[], rows: RoleItem[]) => {
    this.setState({
      selectedRows: rows,
      selectedRowKeys: keys
    });
  };

  /**
   * 工具栏左边的按钮
   * @returns 按钮集合
   */
  renderLeftButton = () => {
    const { previewLoading } = this.props;
    return (
      <>
        <AButton code='grantAuth' pageCode='role-list' name='权限分配' onClick={this.openRoleAuthModal} loading={previewLoading} />
      </>
    );
  }

  /**
   * 角色授权，打开角色授权窗口
   */
  openRoleAuthModal = async () => {
    // 表格选中内容
    console.info('openRoleAuthModal');
    const { selectedRowKeys } = this.state;
    if (selectedRowKeys.length === 0) {
      message.warn('请先选择一条数据!');
      return;
    }
    if (selectedRowKeys.length > 1) {
      message.warn('只能选择一条数据!');
      return;
    }
    // 获取角色id
    const roleId = selectedRowKeys[0];
    console.info('roleId', roleId);
    const { dispatch } = this.props;
    const res = await dispatch(MENU_TREE({
      roleId
    }));
    // console.info(res);
    if (res) {
      // 查询菜单权限树，并根据勾选角色ID对应已分配的权限
      // 这里试试dispatch的回调方法
      // 只有查询成功才会弹窗
      const { result } = res;
      const { grantKeys, menuTree } = result;
      const authTreeData = {
        roleId,
        grantKeys,
        menuTree
      };
      this.setState({
        authTreeData,
        roleAuthModalVisible: true
      });
    }
  }

  /**
   * 授权窗口确认按钮
   * @param {*} record 授权树选中菜单列表
   */
  handleGrantModalOk = async (record: GrantAuthItem) => {
    console.log('handleGrantModalOk', record);
    // const { roleId, grantKeys } = record;
    // const value = {
    //   roleId,
    //   menuIds: [...grantKeys]
    // }
    const { dispatch } = this.props;
    const res = await dispatch(GRANT_AUTH(record));
    if (res) {
      this.setState({
        roleAuthModalVisible: false,
        authTreeData: EMPTY_TREE
      });
    }
  }

  handleGrantModalCancel = () => {
    console.log('handleGrantModalCancel');
    this.setState({
      roleAuthModalVisible: false,
      authTreeData: EMPTY_TREE
    });
  }

  render(){

    const code = 'role-list';
    const { roleList, loading, total, addLoading, updateLoading, grantLoading } = this.props;
    const { addModalVisible, updateModalVisible, roleFormData, roleAuthModalVisible, authTreeData, flag } = this.state;

    const rowKey = (record: RoleItem) => record.roleId;
    const pkField = 'roleId';

    return (
      <PageContainer>
        <ProCard title="角色列表" headerBordered>
          <RoleQueryForm colon={false} onSubmit={this.handleFetchRole} loading={loading}/>
          <AGrid
            code={code}
            btnCallBack={this.handleBtnCallBack}
            delConfirmMsg='确定删除选中记录？若有用户已分配该角色，将一并删除！'
            renderLeftButton={this.renderLeftButton}
            columns={this.columns}
            rowKey={rowKey}
            pkField={pkField}
            dataSource={roleList}
            loading={loading}
            total={total}
            onSelectRow={this.onSelectRow}
            onPageNumAndSizeChange={this.onPageNumAndSizeChange}
          />
        </ProCard>
        {
          !addModalVisible ? null :
          <AddRoleModal
            colon={false}
            modalTitle='新增角色'
            modalWidth={1000}
            modalVisible={addModalVisible}
            loading={addLoading}
            onHandlerOK={this.handleAddModalOk}
            onHandlerCancel={this.handleAddModalCancel}
          />
        }
        {
          !updateModalVisible ? null :
          <UpdateRoleModal
            colon={false}
            modalTitle=''
            modalWidth={1000}
            modalVisible={updateModalVisible}
            flag={flag}
            loading={updateLoading}
            formData={roleFormData}
            onHandlerOK={this.handleUpdateModalOk}
            onHandlerCancel={this.handleUpdateModalCancel}
          />
        }
        {
          !roleAuthModalVisible ? null :
          <GrantAuthModal
            colon={false}
            modalTitle='分配权限'
            modalWidth={600}
            modalVisible={roleAuthModalVisible}
            flag={flag}
            loading={grantLoading}
            formData={authTreeData}
            onHandlerOK={this.handleGrantModalOk}
            onHandlerCancel={this.handleGrantModalCancel}
          />
        }
      </PageContainer>
    );
  }
}