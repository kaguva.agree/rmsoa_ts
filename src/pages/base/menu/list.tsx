import React from 'react';
import { connect, Dispatch } from 'umi';
import { Space, Tag } from 'antd';
import type { ColumnsType } from 'antd/es/table';
import { PageContainer } from '@ant-design/pro-layout';
import ProCard from '@ant-design/pro-card';
import AGrid, { AGridButtonCallBackModel } from '@/components/SelfComp/AGrid';
import { ConnectState } from '@/models/connect';
import { DataItem, FlagEnum } from '@/models/common';
import { MENU_INIT, MENU_LIST, MENU_ADD, MENU_UPDATE, MENU_DELETE } from '@/actions/menu';
import { getItemValue } from '@/utils/commons';
import MenuQueryForm from './components/MenuQueryForm';
import AddMenuModal from './components/AddMenuModal';
import UpdateMenuModal from './components/UpdateMenuModal';
import { MenuItem, MenuSelectTreeItem } from './data';

interface MenuListProps {
  dispatch: Dispatch;
  menuList: MenuItem[],
  total: number,
  loading: boolean;
  pageSize: number;
  addLoading: boolean;
  updateLoading: boolean;
  menuTypeData: DataItem[];
  menuActionData: DataItem[];
  statusData: DataItem[];
  hideInMenuData: DataItem[];
}

interface MenuListState {
  addModalVisible: boolean;
  menuFormData: MenuItem;
  updateModalVisible: boolean;
  // 表格选择的数据
  selectedRows: MenuItem[];
  selectedRowKeys: React.Key[];
  queryParam?: MenuItem;
  flag: FlagEnum;
}

const EMPTY: MenuItem = {
  menuId: '',
  parentMenuId: '',
  menuCode: '',
  menuName: '',
  menuPath: '',
  menuType: '',
  hideInMenu: '',
  menuStates: '',
  menuAction: '',
  menuSort: '',
  menuAlias: '',
  children: []
}

@connect((state: ConnectState) => ({
  pageSize: state.menus.pageSize,
  menuList: state.menus.menuList,
  menuTypeData: state.menus.menuTypeData,
  menuActionData: state.menus.menuActionData,
  hideInMenuData: state.menus.hideInMenuData,
  loading: state.loading.effects['menus/fetchAllMenu'],
  addLoading: state.loading.effects['menus/addMenu'],
  updateLoading: state.loading.effects['menus/updateMenu'],
}))
export default class MenuList extends React.PureComponent<MenuListProps, MenuListState> {

  state: MenuListState = {
    addModalVisible: false,
    menuFormData: EMPTY,
    updateModalVisible: false,
    // 表格选择的数据
    selectedRows: [],
    selectedRowKeys: [],
    queryParam: EMPTY,
    flag: 'edit'
  };

  columns: ColumnsType<MenuItem> = [
    {
      title: '菜单名称',
      dataIndex: 'menuName',
      width: 180
    },
    {
      title: '菜单图标',
      dataIndex: 'menuIcon',
      width: 200

    },
    {
      title: '菜单编号',
      dataIndex: 'menuCode',
      width: 130
    },
    {
      title: '菜单别名',
      dataIndex: 'menuAlias',
      width: 100
    },
    {
      title: '菜单类型',
      dataIndex: 'menuType',
      render: (text: string, record: MenuItem, index: number) => this.menuTypeFunc(record),
      width: 80
    },
    {
      title: '菜单是否显示',
      dataIndex: 'hideInMenu',
      render: (text: string, record: MenuItem, index: number) => this.hideInMenuFunc(record),
      width: 120
    },
    {
      title: '路由地址',
      dataIndex: 'menuPath',
      width: 250,
      ellipsis: true
    },
    {
      title: '按钮功能',
      dataIndex: 'menuAction',
      render: (text: string, record: MenuItem, index: number) => this.menuActionFunc(record),
      width: 120
    },
    {
      title: '排序',
      dataIndex: 'menuSort',
      width: 80
    }
  ];

  componentDidMount() {
    console.info('MenuList.componentDidMount');
    // 查询本页面需所有参数
    const { dispatch } = this.props;
    dispatch(MENU_INIT({}));
  }

  menuTypeFunc = (record: MenuItem) => {
    const { menuTypeData } = this.props;
    const { menuType } = record;
    const value = getItemValue(menuTypeData, menuType);
    return <Tag color='processing'>{value}</Tag>
  }

  hideInMenuFunc = (record: MenuItem) => {
    const { hideInMenuData } = this.props;
    const { hideInMenu } = record;
    const value = getItemValue(hideInMenuData, hideInMenu);
    if (hideInMenu === '1') {
      return <Tag color='error'>{value}</Tag>;
    }
    return <Tag color='processing'>{value}</Tag>;
  }

  menuActionFunc = (record: MenuItem) => {
    const { menuActionData } = this.props;
    const { menuAction } = record;
    if (!menuAction) {
      return '';
    }
    const value = getItemValue(menuActionData, menuAction);
    return <Tag color='processing'>{value}</Tag>
  }

  /**
   * 打开菜单新增窗口
   */
  openAddMenuModal = () => {
    this.setState({ addModalVisible: true });
  }

  /**
   * 修改菜单，打开菜单修改窗口
   */
  openUpdateMenuModal = (record: MenuItem[], flag: FlagEnum) => {
    // 表格选中内容
    console.info('openUpdateMenuModal');
    console.info(record);
    const row = record[0];
    const menuFormData = {
      ...row,
    };
    this.setState({
      menuFormData,
      flag,
      updateModalVisible: true
    });
  }

  handleAddModalOk = async (record: MenuItem) => {
    console.log(record);
    const { dispatch } = this.props;
    const res = await dispatch(MENU_ADD(record));
    console.info(res);
    if (res) {
      this.setState({
        addModalVisible: false
      });
      const { pageSize } = this.props;
      this.fetchAllMenu(1, pageSize);
    }
  }

  handleAddModalCancel = () => {
    this.setState({ addModalVisible: false });
  };

  handleUpdateModalOk = async (record: MenuItem) => {
    console.log('handleUpdateModalOk');
    console.log(record);
    const { flag } = this.state;
    if (flag === 'edit') {
      const { dispatch } = this.props;
      const res = await dispatch(MENU_UPDATE(record));
      console.info(res);
      if (res) {
        this.setState({
          updateModalVisible: false,
          menuFormData: EMPTY
        });
        const { pageSize } = this.props;
        this.fetchAllMenu(1, pageSize);
      }
    } else {
      this.setState({
        updateModalVisible: false,
        menuFormData: EMPTY
      });
    }
  }

  handleUpdateModalCancel = () => {
    this.setState({
      updateModalVisible: false,
      menuFormData: EMPTY
    });
  };

  deleteMenus = async (keys:React.Key[]) => {
    const { dispatch } = this.props;
    const res = await dispatch(MENU_DELETE(keys));
    if (res) {
      const { pageSize } = this.props;
      this.fetchAllMenu(1, pageSize);
    }
  }

  /**
   * 根据条件分页查询表格数据
   * @param {*} record 条件
   */
  handleFetchMenu = (record: MenuItem) => {
    console.log(record);
    const queryParam = {
      ...record
    };
    // 查询条件保存起来，方便点击页码时使用
    this.setState({
      queryParam
    }, () => {
      const { pageSize } = this.props;
      this.fetchAllMenu(1, pageSize);
    });
  }

  /**
   * 查询菜单树信息
   * @param {*} pageSize 每页记录条数
   * @param {*} pageNum 页码
   */
  fetchAllMenu = (pageNum: number, pageSize: number) => {
    const { queryParam } = this.state;
    const { dispatch } = this.props;
    dispatch(MENU_LIST({
      ...queryParam,
      pageSize,
      pageNum
    }));
  }

  /**
   * 处理按钮点击回调事件
   * @param {*} payload 数据包
   */
  handleBtnCallBack = (callBackModel: AGridButtonCallBackModel<MenuItem>) => {
    // btn 按钮
    // keys 表格勾选数组
    const { btn, keys } = callBackModel;
    const { alias } = btn;
    // 新增
    if (alias === 'add') {
      this.openAddMenuModal();
      return;
    }
    // 修改
    if (alias === 'edit') {
      const { rows } = callBackModel;
      this.openUpdateMenuModal(rows, 'edit');
      return;
    }
    // 删除
    if (alias === 'delete') {
      // const { rows } = callBackModel;
      // 调用删除服务，删除勾选数据
      this.deleteMenus(keys);
      return;
    }
    // 查看
    if (alias === 'view') {
      const { rows } = callBackModel;
      this.openUpdateMenuModal(rows, 'view');
      return;
    }
  }

  /**
   * 表格勾选回调函数
   * @param {*} rows 表格选中数据集合
   */
  onSelectRow = (keys: React.Key[], rows: MenuItem[]) => {
    this.setState({
      selectedRows: rows,
      selectedRowKeys: keys
    });
  };

  /**
   * 根据全量菜单数据生成下拉框菜单树形数据，过滤掉按钮
   * @param {Array} menuList 全量菜单数据
   * @returns 下拉框菜单树形数据
   */
  generatorMenuTreeData = (menuList: MenuItem[] = []): MenuSelectTreeItem[] => {
    return menuList.map(menu => {
      if (menu.children) {
        const newMenu: MenuSelectTreeItem = {
          value: menu.menuId,
          title: menu.menuName,
          children: this.generatorMenuTreeData(menu.children.filter(childMenu => childMenu.menuType === '0' || childMenu.menuType === '1' || childMenu.menuType === '3'))
        };
        return newMenu;
      } else {
        const newMenu: MenuSelectTreeItem = {
          value: menu.menuId,
          title: menu.menuName
        };
        return newMenu;
      }
    });
  }

  render() {

    const code = 'menu-list';
    const { menuList, loading, menuTypeData, menuActionData, hideInMenuData, addLoading, updateLoading } = this.props;
    const { addModalVisible, updateModalVisible, menuFormData, flag } = this.state;

    // menuList过滤掉按钮，只保留菜单
    const menuTreeData = this.generatorMenuTreeData(menuList);

    const rowKey = (record: MenuItem) => record.menuId;
    const pkField = 'menuId';

    return (
      <PageContainer>
          <Space direction='vertical' size='middle' style={{ display: 'flex' }}>
            <ProCard title='查询条件' headerBordered >
              <MenuQueryForm colon={false} onSubmit={this.handleFetchMenu} loading={loading}/>
            </ProCard>
            <ProCard>
              <AGrid
                code={code}
                btnCallBack={this.handleBtnCallBack}
                columns={this.columns}
                rowKey={rowKey}
                pkField={pkField}
                dataSource={menuList}
                loading={loading}
                onSelectRow={this.onSelectRow}
                scroll={{x:1600}}
                actionColumnFixed='right'
              />
            </ProCard>
          </Space>
          {
            !addModalVisible ? null :
            <AddMenuModal
              menuTypeData={menuTypeData}
              menuActionData={menuActionData}
              hideInMenuData={hideInMenuData}
              menuTreeData={menuTreeData}
              colon={false}
              modalTitle='新增菜单'
              modalWidth={1000}
              modalVisible={addModalVisible}
              loading={addLoading}
              onHandlerOK={this.handleAddModalOk}
              onHandlerCancel={this.handleAddModalCancel}
            />
          }
          {
            !updateModalVisible ? null :
            <UpdateMenuModal
              menuTypeData={menuTypeData}
              menuActionData={menuActionData}
              hideInMenuData={hideInMenuData}
              menuTreeData={menuTreeData}
              colon={false}
              modalTitle=''
              modalWidth={1000}
              modalVisible={updateModalVisible}
              flag={flag}
              loading={updateLoading}
              formData={menuFormData}
              onHandlerOK={this.handleUpdateModalOk}
              onHandlerCancel={this.handleUpdateModalCancel}
            />
          }
      </PageContainer>
    );
  }
}