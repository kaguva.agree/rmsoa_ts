import { DefaultOptionType } from "antd/lib/select";

/**
 * 菜单
 */
export interface MenuItem {
  /** 菜单id */
  menuId: string;
  /** 父菜单id */
  parentMenuId: string;
  /** 菜单编码 */
  menuCode: string;
  /** 菜单名称 */
  menuName: string;
  /** 菜单图标 */
  menuIcon?: string;
  /** 菜单路径，当为按钮时为null */
  menuPath?: string;
  /** 权限类型，0-菜单，1-按钮 */
  menuType: string;
  /** 菜单是否隐藏，0-不隐藏，1-隐藏 */
  hideInMenu: string;
  /** 状态，0-正常，1-失效 */
  menuStates: string;
  /** 按钮位置 */
  menuAction: string;
  /** 排序 */
  menuSort: string;
  /** 按钮别名 */
  menuAlias: string;
  /** 子节点，菜单或按钮 */
  children?: MenuItem[];
}

/**
 * 用于下拉框树形结构
 */
export interface MenuSelectTreeItem extends DefaultOptionType {
  value: string;
  title: string;
  label?: string;
  children?: MenuSelectTreeItem[]
}