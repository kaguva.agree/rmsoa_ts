import React from 'react';
import { Form, Input } from 'antd';
import { basicFormItemLangLayout } from '@/utils/commons';
import QueryFilterForm from '@/components/SelfComp/QueryFilterForm';
import { QueryFormProps } from '@/models/FormModal';

export type MenuQueryProps = QueryFormProps & {
}

const FormItem = Form.Item;

class MenuQueryForm extends React.PureComponent<MenuQueryProps> {

  render() {
    const { colon, loading, onSubmit, onReset } = this.props;
    const formItemLayout = basicFormItemLangLayout;

    return (
      <QueryFilterForm
        colon={colon}
        loading={loading}
        onSubmit={onSubmit}
        onReset={onReset}
      >
        <FormItem label="菜单编码" name='menuCode' {...formItemLayout} colon={colon}>
          <Input />
        </FormItem>
        <FormItem label="菜单名称" name='menuName' {...formItemLayout} colon={colon}>
          <Input />
        </FormItem>
      </QueryFilterForm>
    );
  }
}

export default MenuQueryForm;