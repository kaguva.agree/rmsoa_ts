import { Effect } from 'umi';
import { message } from 'antd';
import { AppItem } from '@/pages/system/app/data';
import { fetchAllApp, addApp, updateApp, deleteApps } from '@/services/appApis';
import { querySysDict } from '@/services/systemApis';
import { BaseModelType, DataItem } from './common';

export interface AppModelState {
  rows?: AppItem[];
  total?: number;
  pageSize?: number;
  systemData?: DataItem[],
  clusterData?: DataItem[],
  statusData?: DataItem[],
}

export interface AppModelType extends BaseModelType {
  namespace: 'apps';
  state: AppModelState;
  effects: {
    fetchAppInitParams: Effect;
    fetchAllApp: Effect;
    addApp: Effect;
    updateApp: Effect;
    deleteApps: Effect;
  };
  // reducers: {
  //   updateState: Reducer<ParamModelState>;
  // };
}

const AppModel: AppModelType = {
  namespace: 'apps',

  state: {
    rows: [],
    total: 0,
    pageSize: 10,
    statusData: [
      {
        key: '0',
        value: '生效'
      },
      {
        key: '1',
        value: '失效'
      }
    ],
  },

  effects: {
    *fetchAppInitParams({ payload }, sagaEffects) {
      const { all, call, put } = sagaEffects;
      try {
        const [ systemResponse, clusterResponse, statusResponse ] = yield all([
          call(querySysDict, {
            dictName: 'system'
          }),
          call(querySysDict, {
            dictName: 'cluster'
          }),
          call(querySysDict, {
            dictName: 'status'
          })
        ]);
        console.info(systemResponse);
        console.info(clusterResponse);
        console.info(statusResponse);
        const systemSuccess =  systemResponse.success;
        const clusterSuccess =  clusterResponse.success;
        if (!systemSuccess || !clusterSuccess) {
          if (!systemSuccess) {
            message.error(`${systemResponse.errorCode}:${systemResponse.errorMsg}`); // 打印错误信息
          }
          if (!clusterSuccess) {
            message.error(`${clusterResponse.errorCode}:${clusterResponse.errorMsg}`); // 打印错误信息
          }
        } else {
          // const systemData = systemResponse.result.rows;
          // const clusterData = clusterResponse.result.rows;
          const result  = {
            systemData: [...systemResponse.result],
            clusterData: [...clusterResponse.result],
          };
          console.info(result);
          yield put({ type: 'updateState', payload: result });
        }
      } catch(e) {
        console.error(e);
        message.error('数据获取失败');
      }
    },
    *fetchAllApp({ payload }, sagaEffects) {
      const { call, put } = sagaEffects;
      try {
        // 获取服务端数据
        // call 第一个参数为一个函数，后面的参数为函数调用时的入参
        console.log(JSON.stringify(payload));
        const response = yield call(fetchAllApp, payload);
        // console.log(JSON.stringify(response));
        const { success } = response;
        if (success) {
          // 通讯成功取出数据
          const { result } = response;
          // 更新表格数据
          yield put({ type: 'updateState', payload: result });
        } else {
          // message.error(`${response.errorCode}:${response.errorMsg}`); // 打印错误信息
        }
      } catch(e) {
        console.info(e);
        message.error('数据获取失败'); // 打印错误信息
      }
    },
    *addApp({ payload }, sagaEffects) {
      const { call, put } = sagaEffects;
      try {
        // 获取服务端数据
        // call 第一个参数为一个函数，后面的参数为函数调用时的入参
        console.log(JSON.stringify(payload));
        const response = yield call(addApp, payload);
        // console.log(JSON.stringify(response));
        const { success } = response;
        if (success) {
          message.success('新增成功');
          return true;
          // yield put({ type: 'updateState', payload: {addModalVisible: false} });
          // // 刷新表格
          // const queryApps = {
          //   pageSize: 10,
          //   pageNum: 1
          // }
          // // put调用action，action对应effects
          // yield put({ type: 'fetchAllApp', payload: queryApps });
        } else {
          // message.error(`${response.errorCode}:${response.errorMsg}`); // 打印错误信息
          return false;
        }
      } catch(e) {
        console.info(e);
        message.error('新增失败'); // 打印错误信息
        return false;
      }
    },
    *updateApp({ payload }, sagaEffects) {
      const { call, put } = sagaEffects;
      try {
        // 获取服务端数据
        // call 第一个参数为一个函数，后面的参数为函数调用时的入参
        console.log(JSON.stringify(payload));
        const response = yield call(updateApp, payload);
        // console.log(JSON.stringify(response));
        const { success } = response;
        if (success) {
          message.success('更新成功');
          return true;
        } else {
          // message.error(`${response.errorCode}:${response.errorMsg}`); // 打印错误信息
          return false;
        }
      } catch(e) {
        console.info(e);
        message.error('更新失败'); // 打印错误信息
        return false;
      }
    },
    *deleteApps({ payload }, sagaEffects) {
      const { call, put } = sagaEffects;
      try {
        // 获取服务端数据
        // call 第一个参数为一个函数，后面的参数为函数调用时的入参
        console.log(JSON.stringify(payload));
        const response = yield call(deleteApps, payload);
        // console.log(JSON.stringify(response));
        // yield call(delay, 3000);
        const { success } = response;
        if (success) {
          message.success('删除成功');
          // 刷新表格
          return true;
        } else {
          // message.error(`${response.errorCode}:${response.errorMsg}`); // 打印错误信息
          return false;
        }
      } catch(e) {
        console.error(e);
        message.error('删除失败'); // 打印错误信息
        return false;
      }
    },
  },

  reducers: {
    updateState(state, { payload }) {
      console.info(payload);
      return {
        ...state,
        ...payload
      }
    },
  },
}

export default AppModel;