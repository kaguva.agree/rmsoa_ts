import { DashboardModelState } from './dashboard';
import { MenuDataItem, Settings as ProSettings } from '@ant-design/pro-layout';
import { GlobalModelState } from './global';
import { UserModelState } from './user';
import { StateType } from './login';
import { MenuModelState }from './menu';
import { ParamModelState } from './param';
import { AppModelState } from './apps';
import { DictModelState } from './dicts';
import { TradeModelState } from './trades';
import { MenuModelStateEx } from './menus';
import { RoleModelState } from './roles';
import { JobModelState } from './schedules';
import { SystemModelState } from './systems';
import { UserModelStateEx } from './users';
import { TaskModelState } from './tasks';
import { CqModelState } from './cqs';
import { DashboardModelState } from './cqs';
import { WorkbenchModelState } from './workbenchs';

export { GlobalModelState, UserModelState };

export interface Loading {
  global: boolean;
  effects: { [key: string]: boolean | undefined };
  models: {
    global?: boolean;
    menu?: boolean;
    setting?: boolean;
    user?: boolean;
    login?: boolean;
  };
}

export interface ConnectState {
  global: GlobalModelState;
  loading: Loading;
  settings: ProSettings;
  user: UserModelState;
  login: StateType;
  menu: MenuModelState;
  params: ParamModelState;
  apps: AppModelState;
  dicts: DictModelState;
  trades: TradeModelState;
  menus: MenuModelStateEx;
  roles: RoleModelState;
  schedules: JobModelState;
  systems: SystemModelState;
  users: UserModelStateEx;
  tasks: TaskModelState;
  cqs: CqModelState;
  dashboards: DashboardModelState;
  workbenchs: WorkbenchModelState;
}

export interface Route extends MenuDataItem {
  routes?: Route[];
}

export interface TradeResult {
  success: boolean;
  errorCode: string;
  errorMsg: string;
  result: any;
}
