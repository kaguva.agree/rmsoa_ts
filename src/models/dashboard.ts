import { Effect, Reducer } from 'umi';
import {
  ProjectOverviewDataType,
  ProjectRankingDataType,
  ProjectStatisticsDataType,
  ProjectStatisticDataType,
  ProjectOverviewRadarDataType,
} from '@/pages/dashboard/data';
import {
  fetchOverview,
  fetchStatisticOfLastWillPrd,
  fetchStatistics,
  fetchStatisticOfAlreadyPrd,
  fetchStatisticOfNotPrd,
  fetchRankings,
} from '@/services/dashboardApis';
import { TradeResult } from './connect';

export interface DashboardModelState {
  overviewDatas: ProjectOverviewDataType;
  lastWillPrdDatas: ProjectOverviewRadarDataType;
  statisticsDatas: ProjectStatisticsDataType[];
  alreadyPrdStatisticDatas: ProjectStatisticDataType[];
  notPrdStatisticDatas: ProjectStatisticDataType[];
  rankingDatas: ProjectRankingDataType[];
}

export interface DashboardModelType {
  namespace: string;
  state: DashboardModelState;
  effects: {
    fetchOverview: Effect;
    fetchStatisticOfLastWillPrd: Effect;
    fetchStatistics: Effect;
    fetchStatisticOfAlreadyPrd: Effect;
    fetchStatisticOfNotPrd: Effect;
    fetchRankings: Effect;
  };
  reducers: {
    save: Reducer<DashboardModelState>;
    clear: Reducer<DashboardModelState>;
  };
}

const EMPTY_LASTWILLPRDDATAS: ProjectOverviewRadarDataType = {
  lastWillPrdDate: '',
  total: 0,
  data: [],
};

const EMPTY_PROJECTOVERVIEWDATATYPE1: ProjectOverviewDataType = {
  systemCode: '',
  todaySitCqNum: 0,
  thisWeekSitCqNum: 0,
  nextWeekSitCqNum: 0,
  tenDaysSitCqNum: 0,
  todayPrdCqNum: 0,
  thisWeekPrdCqNum: 0,
  nextWeekPrdCqNum: 0,
  tenDaysPrdCqNum: 0,
  todayUndoCqTaskNum: 0,
  thisWeekUndoCqTaskNum: 0,
  nextWeekUndoCqTaskNum: 0,
  expiredUndoCqTaskNum: 0,
  todayUndoTradeTaskNum: 0,
  thisWeekUndoTradeTaskNum: 0,
  nextWeekUndoTradeTaskNum: 0,
  expiredUndoTradeTaskNum: 0,
}

const initState: DashboardModelState = {
  overviewDatas: EMPTY_PROJECTOVERVIEWDATATYPE1,
  lastWillPrdDatas: EMPTY_LASTWILLPRDDATAS,
  statisticsDatas: [],
  alreadyPrdStatisticDatas: [],
  notPrdStatisticDatas: [],
  rankingDatas: [],
};

const DashboardModel: DashboardModelType = {
  namespace: 'dashboards',
  state: initState,
  effects: {
    *fetchOverview({ payload }, sagaEffects) {
      const { call, put } = sagaEffects;
      const response: TradeResult = yield call(fetchOverview, payload);
      console.info(response);
      const { success } = response;
      if (success) {
        const { result } = response;
        yield put({
          type: 'save',
          payload: result,
        });
      }
    },
    *fetchStatisticOfLastWillPrd({ payload }, sagaEffects) {
      const { call, put } = sagaEffects;
      const response: TradeResult = yield call(fetchStatisticOfLastWillPrd, payload);
      console.info(response);
      const { success } = response;
      if (success) {
        const { result } = response;
        yield put({
          type: 'save',
          payload: result,
        });
      }
    },
    *fetchStatistics({ payload }, sagaEffects) {
      const { call, put } = sagaEffects;
      const response: TradeResult = yield call(fetchStatistics, payload);
      const { success } = response;
      if (success) {
        const { result } = response;
        yield put({
          type: 'save',
          payload: result,
        });
      }
    },
    *fetchStatisticOfAlreadyPrd({ payload }, sagaEffects) {
      const { call, put } = sagaEffects;
      const response: TradeResult = yield call(fetchStatisticOfAlreadyPrd, payload);
      const { success } = response;
      if (success) {
        const { result } = response;
        const { statisticDatas } = result;
        yield put({
          type: 'save',
          payload: {
            alreadyPrdStatisticDatas: [...statisticDatas],
          },
        });
      }
    },
    *fetchStatisticOfNotPrd({ payload }, sagaEffects) {
      const { call, put } = sagaEffects;
      const response: TradeResult = yield call(fetchStatisticOfNotPrd, payload);
      const { success } = response;
      if (success) {
        const { result } = response;
        const { statisticDatas } = result;
        yield put({
          type: 'save',
          payload: {
            notPrdStatisticDatas: [...statisticDatas],
          },
        });
      }
    },
    *fetchRankings({ payload }, sagaEffects) {
      const { call, put } = sagaEffects;
      const response: TradeResult = yield call(fetchRankings, payload);
      const { success } = response;
      if (success) {
        const { result } = response;
        yield put({
          type: 'save',
          payload: result,
        });
      }
    },
  },

  reducers: {
    save(state, { payload }) {
      return {
        ...state,
        ...payload,
      };
    },
    clear() {
      return initState;
    },
  },
};

export default DashboardModel;
