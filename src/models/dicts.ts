import { Effect } from 'umi';
import { message } from 'antd';
import { DictItem } from '@/pages/system/dict/data';
import { fetchAllDict, addDict, updateDict, deleteDicts } from '@/services/dictApis';
import { BaseModelType, DataItem } from './common';

export interface DictModelState {
  rows?: DictItem[];
  total?: number;
  pageSize?: number;
  editFlagData?: DataItem[],
  statusData?: DataItem[],
}

export interface DictModelType extends BaseModelType {
  namespace: 'dicts';
  state: DictModelState;
  effects: {
    fetchAllDict: Effect;
    addDict: Effect;
    updateDict: Effect;
    deleteDicts: Effect;
  };
  // reducers: {
  //   updateState: Reducer<ParamModelState>;
  // };
}

const AppModel: DictModelType = {
  namespace: 'dicts',

  state: {
    rows: [],
    total: 0,
    pageSize: 10,
    editFlagData: [
      {
        key: '0',
        value: '可编辑'
      },
      {
        key: '1',
        value: '不可编辑'
      }
    ],
    statusData: [
      {
        key: '0',
        value: '生效'
      },
      {
        key: '1',
        value: '失效'
      }
    ],
  },

  effects: {
    *fetchAllDict({ payload }, sagaEffects) {
      const { call, put } = sagaEffects;
      try {
        // 获取服务端数据
        // call 第一个参数为一个函数，后面的参数为函数调用时的入参
        console.log(JSON.stringify(payload));
        const response = yield call(fetchAllDict, payload);
        // console.log(JSON.stringify(response));
        const { success } = response;
        if (success) {
          // 通讯成功取出数据
          const { result } = response;
          // 更新表格数据
          yield put({ type: 'updateState', payload: result });
        } else {
          // message.error(`${response.errorCode}:${response.errorMsg}`); // 打印错误信息
        }
      } catch(e) {
        console.info(e);
        message.error('数据获取失败'); // 打印错误信息
      }
    },
    *addDict({ payload }, sagaEffects) {
      const { call, put } = sagaEffects;
      try {
        // 获取服务端数据
        // call 第一个参数为一个函数，后面的参数为函数调用时的入参
        console.log(JSON.stringify(payload));
        const response = yield call(addDict, payload);
        // console.log(JSON.stringify(response));
        const { success } = response;
        if (success) {
          message.success('新增成功');
          return true;
          // yield put({ type: 'updateState', payload: {addModalVisible: false} });
          // // 刷新表格
          // const queryDicts = {
          //   pageSize: 10,
          //   pageNum: 1
          // }
          // // put调用action，action对应effects
          // yield put({ type: 'fetchAllDict', payload: queryDicts });
        } else {
          // message.error(`${response.errorCode}:${response.errorMsg}`); // 打印错误信息
          return false;
        }
      } catch(e) {
        console.info(e);
        message.error('新增失败'); // 打印错误信息
        return false;
      }
    },
    *updateDict({ payload }, sagaEffects) {
      const { call, put } = sagaEffects;
      try {
        // 获取服务端数据
        // call 第一个参数为一个函数，后面的参数为函数调用时的入参
        console.log(JSON.stringify(payload));
        const response = yield call(updateDict, payload);
        // console.log(JSON.stringify(response));
        const { success } = response;
        if (success) {
          message.success('更新成功');
          return true;
        } else {
          // message.error(`${response.errorCode}:${response.errorMsg}`); // 打印错误信息
          return false;
        }
      } catch(e) {
        console.info(e);
        message.error('更新失败'); // 打印错误信息
        return false;
      }
    },
    *deleteDicts({ payload }, sagaEffects) {
      const { call, put } = sagaEffects;
      try {
        // 获取服务端数据
        // call 第一个参数为一个函数，后面的参数为函数调用时的入参
        console.log(JSON.stringify(payload));
        const response = yield call(deleteDicts, payload);
        // console.log(JSON.stringify(response));
        // yield call(delay, 3000);
        const { success } = response;
        if (success) {
          message.success('删除成功');
          // 刷新表格
          return true;
        } else {
          // message.error(`${response.errorCode}:${response.errorMsg}`); // 打印错误信息
          return false;
        }
      } catch(e) {
        console.error(e);
        message.error('删除失败'); // 打印错误信息
        return false;
      }
    },
  },

  reducers: {
    updateState(state, { payload }) {
      console.info(payload);
      return {
        ...state,
        ...payload
      }
    },
  },
}

export default AppModel;