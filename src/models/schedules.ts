import { Effect } from 'umi';
import { message } from 'antd';
import { JobItem } from '@/pages/system/schedule/data';
import { fetchAllJob, addJob, updateJob, deleteJobs, runJob, pauseJob, resumeJob } from '@/services/scheduleApis';
import { querySysDict } from '@/services/systemApis';
import { BaseModelType, DataItem } from './common';

export interface JobModelState {
  rows?: JobItem[];
  total?: number;
  pageSize?: number;
  statusData?: DataItem[],
}

export interface JobModelType extends BaseModelType {
  namespace: 'schedules';
  state: JobModelState;
  effects: {
    fetchJobInitParams: Effect;
    fetchAllJob: Effect;
    addJob: Effect;
    updateJob: Effect;
    deleteJobs: Effect;
    runJob: Effect;
    pauseJob: Effect;
    resumeJob: Effect;
  };
}

const JobModel: JobModelType = {
  namespace: 'schedules',

  state: {
    rows: [],
    total: 0,
    pageSize: 10,
    statusData: [
      {
        key: '0',
        value: '生效'
      },
      {
        key: '1',
        value: '失效'
      }
    ],
  },

  effects: {
    *fetchJobInitParams({ payload }, sagaEffects) {
      const { all, call, put } = sagaEffects;
      try {
        const [ jobStatusResponse, jobConcurrentResponse, jobMisfirePolicyResponse ] = yield all([
          call(querySysDict, {
            dictName: 'jobStatus'
          }),
          call(querySysDict, {
            dictName: 'jobConcurrent'
          }),
          call(querySysDict, {
            dictName: 'jobMisfirePolicy'
          })
        ]);
        console.info(jobStatusResponse);
        console.info(jobConcurrentResponse);
        console.info(jobMisfirePolicyResponse);
        const jobStatusSuccess =  jobStatusResponse.success;
        const jobConcurrentSuccess =  jobConcurrentResponse.success;
        const jobMisfirePolicySuccess =  jobMisfirePolicyResponse.success;
        if (jobStatusSuccess && jobConcurrentSuccess && jobMisfirePolicySuccess) {
          const result  = {
            jobStatusData: [...jobStatusResponse.result],
            jobConcurrentData: [...jobConcurrentResponse.result],
            jobMisfirePolicyData: [...jobMisfirePolicyResponse.result],
          };
          console.info(result);
          yield put({ type: 'systems/updateState', payload: result });
        }
      } catch(e) {
        console.error(e);
        message.error('数据获取失败');
      }
    },
    *fetchAllJob({ payload }, sagaEffects) {
      const { call, put } = sagaEffects;
      try {
        // 获取服务端数据
        // call 第一个参数为一个函数，后面的参数为函数调用时的入参
        console.log(JSON.stringify(payload));
        const response = yield call(fetchAllJob, payload);
        // console.log(JSON.stringify(response));
        const { success } = response;
        if (success) {
          // 通讯成功取出数据
          const { result } = response;
          // 更新表格数据
          yield put({ type: 'updateState', payload: result });
        }
      } catch(e) {
        console.info(e);
        message.error('数据获取失败'); // 打印错误信息
      }
    },
    *addJob({ payload }, sagaEffects) {
      const { call } = sagaEffects;
      try {
        // 获取服务端数据
        // call 第一个参数为一个函数，后面的参数为函数调用时的入参
        console.log(JSON.stringify(payload));
        const response = yield call(addJob, payload);
        // console.log(JSON.stringify(response));
        const { success } = response;
        if (success) {
          message.success('新增成功');
          return true;
        } else {
          return false;
        }
      } catch(e) {
        console.info(e);
        message.error('新增失败'); // 打印错误信息
        return false;
      }
    },
    *updateJob({ payload }, sagaEffects) {
      const { call } = sagaEffects;
      try {
        // 获取服务端数据
        // call 第一个参数为一个函数，后面的参数为函数调用时的入参
        console.log(JSON.stringify(payload));
        const response = yield call(updateJob, payload);
        // console.log(JSON.stringify(response));
        const { success } = response;
        if (success) {
          message.success('更新成功');
          return true;
        } else {
          return false;
        }
      } catch(e) {
        console.info(e);
        message.error('更新失败'); // 打印错误信息
        return false;
      }
    },
    *deleteJobs({ payload }, sagaEffects) {
      const { call } = sagaEffects;
      try {
        // 获取服务端数据
        // call 第一个参数为一个函数，后面的参数为函数调用时的入参
        console.log(JSON.stringify(payload));
        const response = yield call(deleteJobs, payload);
        // console.log(JSON.stringify(response));
        // yield call(delay, 3000);
        const { success } = response;
        if (success) {
          message.success('删除成功');
          // 刷新表格
          return true;
        } else {
          return false;
        }
      } catch(e) {
        console.error(e);
        message.error('删除失败'); // 打印错误信息
        return false;
      }
    },
    *runJob({ payload }, sagaEffects) {
      const { call } = sagaEffects;
      try {
        // 获取服务端数据
        // call 第一个参数为一个函数，后面的参数为函数调用时的入参
        console.log(JSON.stringify(payload));
        const response = yield call(runJob, payload);
        // console.log(JSON.stringify(response));
        const { success } = response;
        if (success) {
          message.success('运行成功');
          return true;
        } else {
          return false;
        }
      } catch(e) {
        console.info(e);
        message.error('运行失败'); // 打印错误信息
        return false;
      }
    },
    *pauseJob({ payload }, sagaEffects) {
      const { call } = sagaEffects;
      try {
        // 获取服务端数据
        // call 第一个参数为一个函数，后面的参数为函数调用时的入参
        console.log(JSON.stringify(payload));
        const response = yield call(pauseJob, payload);
        // console.log(JSON.stringify(response));
        const { success } = response;
        if (success) {
          message.success('暂停成功');
          return true;
        } else {
          return false;
        }
      } catch(e) {
        console.info(e);
        message.error('暂停失败'); // 打印错误信息
        return false;
      }
    },
    *resumeJob({ payload }, sagaEffects) {
      const { call } = sagaEffects;
      try {
        // 获取服务端数据
        // call 第一个参数为一个函数，后面的参数为函数调用时的入参
        console.log(JSON.stringify(payload));
        const response = yield call(resumeJob, payload);
        // console.log(JSON.stringify(response));
        const { success } = response;
        if (success) {
          message.success('恢复成功');
          return true;
        } else {
          return false;
        }
      } catch(e) {
        console.info(e);
        message.error('恢复失败'); // 打印错误信息
        return false;
      }
    },
  },

  reducers: {
    updateState(state, { payload }) {
      console.info(payload);
      return {
        ...state,
        ...payload
      }
    },
  },
}

export default JobModel;