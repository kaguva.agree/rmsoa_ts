export interface FormModalProps<T> {
  /** 表单提交函数 */
  onHandlerOK: Function;
  /** 表单重置回调函数 */
  onHandlerCancel?: Function;
  colon: boolean;
  loading: boolean;
  /** 表单数据 */
  formData: T;
  /** 表单弹窗标题 */
  modalTitle: string;
  /** 表单弹窗宽度 */
  modalWidth: number;
  /** 表单弹窗是否可见 */
  modalVisible: boolean;
}

export interface QueryFormProps {
  /** 表单提交函数 */
  onSubmit: Function;
  /** 表单重置回调函数 */
  onReset?: Function;
  colon: boolean;
  loading: boolean;
}

export interface FormDrawerProps<T> {
  /** 表单提交函数 */
  onHandlerOK?: Function;
  /** 表单重置回调函数 */
  onHandlerCancel: Function;
  colon: boolean;
  loading: boolean;
  /** 表单数据 */
  formData: T;
  /** 表单弹窗标题 */
  drawerTitle: string;
  /** 表单弹窗宽度 */
  drawerWidth: number;
  /** 表单弹窗是否可见 */
  drawerVisible: boolean;
}

export interface FormProps<T> {
  /** 表单提交函数 */
  onSubmit?: Function;
  /** 表单重置回调函数 */
  onCancel?: Function;
  colon: boolean;
  loading?: boolean;
  /** 表单数据 */
  formData: T;
}