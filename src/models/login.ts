import { stringify } from 'querystring';
import { history, Reducer, Effect } from 'umi';
import { startsWith } from 'lodash';
import { fakeAccountLogin } from '@/services/login';
import { deleteCurrentLoginUserId, deleteToken, saveCurrentLoginUserId, saveToken, setAuthority, saveCurrentLoginUser } from '@/utils/authority';
import { getPageQuery } from '@/utils/utils';
import { message } from 'antd';

export interface StateType {
  status?: 'ok' | 'error';
  type?: string;
  currentAuthority?: 'user' | 'guest' | 'admin';
}

export interface LoginModelType {
  namespace: string;
  state: StateType;
  effects: {
    login: Effect;
    logout: Effect;
  };
  reducers: {
    changeLoginStatus: Reducer<StateType>;
  };
}

const Model: LoginModelType = {
  namespace: 'login',

  state: {
    status: undefined,
  },

  effects: {
    *login({ payload }, { call, put }) {
      const response = yield call(fakeAccountLogin, payload);
      console.info(response);
      const { success, result } = response;
      try {
        if (success) {
          const { status, token, userId, type, userName } = result;
          payload = {
            status,
            type,
          };
          message.success('🎉 🎉 🎉  登录成功！');
          saveToken(token);
          saveCurrentLoginUserId(userId);
          saveCurrentLoginUser(userName);
          yield put({
            type: 'changeLoginStatus',
            payload
          });
          const userPayload = {
            userid: userId
          };
          yield put({
            type: 'user/saveCurrentUser',
            payload: userPayload
          });
          // 处理URL
          console.info(window.location);
          const urlParams = new URL(window.location.href);
          const params = getPageQuery();
          let { redirect } = params as { redirect: string };
          if (redirect) {
            // 跳转参数不是http开头，则需要添加
            const { origin } = window.location;
            if (!startsWith(redirect, origin)) {
              redirect = origin + redirect;
            }
            console.info(redirect);
            const redirectUrlParams = new URL(redirect);
            if (redirectUrlParams.origin === urlParams.origin) {
              redirect = redirect.substr(urlParams.origin.length);
              if (redirect.match(/^\/.*#/)) {
                redirect = redirect.substr(redirect.indexOf('#') + 1);
              }
            } else {
              window.location.href = '/';
              return;
            }
          }
          history.replace(redirect || '/dashboard/analysis');
          // window.location.reload();
        } else {
          const { errorMsg } = response;
          payload = {
            status: 'error',
            type: 'account',
            errorMsg
          }
          yield put({
            type: 'changeLoginStatus',
            payload
          });
        }
      } catch (error) {
        deleteToken();
        deleteCurrentLoginUserId();
        console.error(error);
        message.error('登录失败！');
      }
    },

    logout() {
      const { redirect } = getPageQuery();
      // Note: There may be security issues, please note
      if (window.location.pathname !== '/user/login' && !redirect) {
        deleteToken();
        deleteCurrentLoginUserId();
        history.replace({
          pathname: '/user/login',
          // search: stringify({
          //   redirect: window.location.href,
          // }),
        });
      }
    },
  },

  reducers: {
    changeLoginStatus(state, { payload }) {
      setAuthority(payload.currentAuthority);
      return {
        ...state,
        status: payload.status,
        type: payload.type,
        errorMsg: payload.errorMsg,
      };
    },
  },
};

export default Model;
