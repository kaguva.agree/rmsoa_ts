import { Effect } from 'umi';
import { message } from 'antd';
import { RoleItem } from '@/pages/base/role/data';
import { fetchAllRole, addRole, updateRole, deleteRoles, grantAuths } from '@/services/roleApis';
import { fetchMenuTreeByRole } from '@/services/menuApis';
import { BaseModelType, DataItem } from './common';

export interface RoleModelState {
  rows?: RoleItem[];
  total?: number;
  pageSize?: number;
  statusData?: DataItem[],
}

export interface RoleModelType extends BaseModelType {
  namespace: 'roles';
  state: RoleModelState;
  effects: {
    fetchAllRole: Effect;
    addRole: Effect;
    updateRole: Effect;
    deleteRoles: Effect;
    fetchMenuTreeByRole: Effect;
    grantAuths: Effect;
  };
}

const RoleModel: RoleModelType = {
  namespace: 'roles',

  state: {
    rows: [],
    total: 0,
    pageSize: 10,
    statusData: [
      {
        key: '0',
        value: '生效'
      },
      {
        key: '1',
        value: '失效'
      }
    ],
  },

  effects: {
    *fetchAllRole({ payload }, sagaEffects) {
      const { call, put } = sagaEffects;
      try {
        // 获取服务端数据
        // call 第一个参数为一个函数，后面的参数为函数调用时的入参
        console.log(JSON.stringify(payload));
        const response = yield call(fetchAllRole, payload);
        // console.log(JSON.stringify(response));
        const { success } = response;
        if (success) {
          // 通讯成功取出数据
          const { result } = response;
          // 更新表格数据
          yield put({ type: 'updateState', payload: result });
        } else {
          // message.error(`${response.errorCode}:${response.errorMsg}`); // 打印错误信息
        }
      } catch(e) {
        console.info(e);
        message.error('数据获取失败'); // 打印错误信息
      }
    },
    *addRole({ payload }, sagaEffects) {
      const { call } = sagaEffects;
      try {
        // 获取服务端数据
        // call 第一个参数为一个函数，后面的参数为函数调用时的入参
        console.log(JSON.stringify(payload));
        const response = yield call(addRole, payload);
        // console.log(JSON.stringify(response));
        const { success } = response;
        if (success) {
          message.success('新增成功');
          return true;
        } else {
          return false;
        }
      } catch(e) {
        console.info(e);
        message.error('新增失败'); // 打印错误信息
        return false;
      }
    },
    *updateRole({ payload }, sagaEffects) {
      const { call } = sagaEffects;
      try {
        // 获取服务端数据
        // call 第一个参数为一个函数，后面的参数为函数调用时的入参
        console.log(JSON.stringify(payload));
        const response = yield call(updateRole, payload);
        // console.log(JSON.stringify(response));
        const { success } = response;
        if (success) {
          message.success('更新成功');
          return true;
        } else {
          return false;
        }
      } catch(e) {
        console.info(e);
        message.error('更新失败'); // 打印错误信息
        return false;
      }
    },
    *deleteRoles({ payload }, sagaEffects) {
      const { call } = sagaEffects;
      try {
        // 获取服务端数据
        // call 第一个参数为一个函数，后面的参数为函数调用时的入参
        console.log(JSON.stringify(payload));
        const response = yield call(deleteRoles, payload);
        const { success } = response;
        if (success) {
          message.success('删除成功');
          // 刷新表格
          return true;
        } else {
          return false;
        }
      } catch(e) {
        console.error(e);
        message.error('删除失败'); // 打印错误信息
        return false;
      }
    },
    *fetchMenuTreeByRole({ payload }, sagaEffects) {
      const { call } = sagaEffects;
      try {
        // 获取服务端数据
        // call 第一个参数为一个函数，后面的参数为函数调用时的入参
        console.log(JSON.stringify(payload));
        const response = yield call(fetchMenuTreeByRole, payload);
        const { success } = response;
        if (success) {
          return response;
        } else {
          return false;
        }
      } catch(e) {
        console.error(e);
        message.error('查询角色权限失败'); // 打印错误信息
        return false;
      }
    },
    *grantAuths({ payload }, sagaEffects) {
      const { call } = sagaEffects;
      try {
        // 获取服务端数据
        // call 第一个参数为一个函数，后面的参数为函数调用时的入参
        console.log(JSON.stringify(payload));
        const response = yield call(grantAuths, payload);
        // console.log(JSON.stringify(response));
        // yield call(delay, 3000);
        const { success } = response;
        if (success) {
          message.success('权限分配成功');
          return true;
        } else {
          return false;
        }
      } catch(e) {
        console.error(e);
        message.error('权限分配失败'); // 打印错误信息
        return false;
      }
    },
  },

  reducers: {
    updateState(state, { payload }) {
      console.info(payload);
      return {
        ...state,
        ...payload
      }
    },
  },
}

export default RoleModel;