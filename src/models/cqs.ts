import { Effect } from 'umi';
import { message } from 'antd';
import { CqItem } from '@/pages/cq/data';
import { addCqBaseInfo, fetchAllCq, fetchCqDetail, fetchCqTrades } from '@/services/cqApis';
import { querySysDict } from '@/services/systemApis';
import { fetchUsersByRole } from '@/services/userApis';
import { BaseModelType } from './common';

export interface CqModelState {
  rows?: CqItem[];
  total?: number;
  pageSize?: number;
}

export interface CqModelType extends BaseModelType {
  namespace: 'cqs';
  state: CqModelState;
  effects: {
    fetchCqAddInitParams: Effect;
    addCqBaseInfo: Effect;
    fetchCqListInitParams: Effect;
    fetchAllCq: Effect;
    fetchCqDetail: Effect;
    updateCqState: Effect;
  };
}

const CqModel: CqModelType = {
  namespace: 'cqs',

  state: {
    rows: [],
    total: 0,
    pageSize: 10,
  },

  effects: {
    *fetchCqAddInitParams({ payload }, sagaEffects) {
      const { all, call, put } = sagaEffects;
      try {
        const [ systemResponse, auditorResponse ] = yield all([
          call(querySysDict, {
            dictName: 'system'
          }),
          call(fetchUsersByRole, {
            roleCode: 'auditor'
          }),
        ]);
        console.info(systemResponse);
        console.info(auditorResponse);
        const systemSuccess =  systemResponse.success;
        const auditorSuccess =  auditorResponse.success;
        if (systemSuccess && auditorSuccess) {
          const result1  = {
            systemData: [...systemResponse.result],
          };
          yield put({ type: 'systems/updateState', payload: result1 });
          const result2  = {
            ...auditorResponse.result
          };
          yield put({ type: 'users/updateState', payload: result2 });
        }
      } catch(e) {
        console.error(e);
        message.error('数据获取失败');
      }
    },
    *addCqBaseInfo({ payload }, sagaEffects) {
      const { call } = sagaEffects;
      try {
        // 获取服务端数据
        // call 第一个参数为一个函数，后面的参数为函数调用时的入参
        const response = yield call(addCqBaseInfo, payload);
        const { success } = response;
        if (success) {
          message.success('新增成功');
          return true;
        } else {
          return false;
        }
      } catch(e) {
        console.info(e);
        message.error('新增失败'); // 打印错误信息
        return false;
      }
    },
    *fetchCqListInitParams({ payload }, sagaEffects) {
      const { all, call, put } = sagaEffects;
      try {
        const [ systemResponse, auditorResponse, cqStatusResponse ] = yield all([
          call(querySysDict, {
            dictName: 'system'
          }),
          call(fetchUsersByRole, {
            roleCode: 'auditor'
          }),
          call(querySysDict, {
            dictName: 'cqStatus'
          }),
        ]);
        console.info(systemResponse);
        console.info(auditorResponse);
        console.info(cqStatusResponse);
        const systemSuccess =  systemResponse.success;
        const auditorSuccess =  auditorResponse.success;
        const cqStatusSuccess =  cqStatusResponse.success;
        if (systemSuccess && auditorSuccess && cqStatusSuccess) {
          const result1  = {
            systemData: [...systemResponse.result],
            cqStatusData: [...cqStatusResponse.result],
          };
          yield put({ type: 'systems/updateState', payload: result1 });
          const result2  = {
            ...auditorResponse.result
          };
          yield put({ type: 'users/updateState', payload: result2 });
        }
      } catch(e) {
        console.error(e);
        message.error('数据获取失败');
      }
    },
    *fetchAllCq({ payload }, sagaEffects) {
      const { call, put } = sagaEffects;
      try{
        // 获取服务端数据
        // call 第一个参数为一个函数，后面的参数为函数调用时的入参
        console.log(JSON.stringify(payload));
        const response = yield call(fetchAllCq, payload);
        const { success } = response;
        if (success) {
          // 通讯成功取出数据
          const { result } = response;
          // 更新表格数据
          yield put({ type: 'updateState', payload: result });
        }
      } catch(e) {
        console.info(e);
        message.error('数据获取失败'); // 打印错误信息
      }
    },
    *fetchCqDetail({ payload }, sagaEffects) {
      const { all, call } = sagaEffects;
      try {
        // 获取服务端数据
        // call 第一个参数为一个函数，后面的参数为函数调用时的入参
        console.log(JSON.stringify(payload));
        const [ detailResponse, tradeResponse ] = yield all([
          // 发起需求详细信息查询
          call(fetchCqDetail, payload),
          // 发起交易列表查询
          call(fetchCqTrades, payload)
        ]);
        console.info(detailResponse);
        console.info(tradeResponse);
        const detailSuccess = detailResponse.success;
        const tradeSuccess = tradeResponse.success;
        if (detailSuccess && tradeSuccess) {
          const detailResult = detailResponse.result;
          const tradeResult = tradeResponse.result;
          const { rows, total } = tradeResult;
          const allResult = {
            result: {
              ...detailResult,
              trades: [...rows],
              tradeTotal: total
            }
          }
          console.info(allResult);
          return allResult;
        }
        return false;
      } catch(e) {
        console.info(e);
        message.error('数据获取失败'); // 打印错误信息
        return false;
      }
    },
    *updateCqState({ payload }, sagaEffects) {
      console.info('updateCqState', payload);
      const { put } = sagaEffects;
      yield put({ type: 'updateState', payload });
    }
  },

  reducers: {
    updateState(state, { payload }) {
      console.info(payload);
      return {
        ...state,
        ...payload
      }
    },
  },
}

export default CqModel;