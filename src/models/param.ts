import { Effect, Reducer } from 'umi';
import { message } from 'antd';
import { ParamItem } from '@/pages/system/param/data';
import { fetchAllParam, addParam, updateParam, deleteParams } from '@/services/paramApis';
import { BaseModelType } from './common';

export interface ParamModelState {
  rows?: ParamItem[];
  total?: number;
  pageSize?: number;
}

export interface ParamModelType extends BaseModelType {
  namespace: 'params';
  state: ParamModelState;
  effects: {
    fetchAllParam: Effect;
    addParam: Effect;
    updateParam: Effect;
    deleteParams: Effect;
  };
  // reducers: {
  //   updateState: Reducer<ParamModelState>;
  // };
}

const ParamModel: ParamModelType = {
  namespace: 'params',

  state: {
    rows: [],
    total: 0,
    pageSize: 10,
  },

  effects: {
    *fetchAllParam({ payload }, { call, put }) {
      try {
        // 获取服务端数据
        // call 第一个参数为一个函数，后面的参数为函数调用时的入参
        console.log(JSON.stringify(payload));
        const response = yield call(fetchAllParam, payload);
        // console.log(JSON.stringify(response));
        const { success } = response;
        if (success) {
          // 通讯成功取出数据
          const { result } = response;
          // 更新表格数据
          yield put({ type: 'updateState', payload: result });
        } else {
          // message.error(`${response.errorCode}:${response.errorMsg}`); // 打印错误信息
        }
      } catch(e) {
        console.info(e);
        message.error('数据获取失败'); // 打印错误信息
      }
    },
    *addParam({ payload }, sagaEffects) {
      const { call } = sagaEffects;
      try {
        // 获取服务端数据
        // call 第一个参数为一个函数，后面的参数为函数调用时的入参
        console.log(JSON.stringify(payload));
        const response = yield call(addParam, payload);
        // console.log(JSON.stringify(response));
        const { success } = response;
        if (success) {
          message.success('新增成功');
          return true;
          // yield put({ type: 'updateState', payload: {addModalVisible: false} });
          // // 刷新表格
          // const queryParams = {
          //   pageSize: 10,
          //   pageNum: 1
          // }
          // // put调用action，action对应effects
          // yield put({ type: 'fetchAllParam', payload: queryParams });
        } else {
          // 打印错误信息
          // message.error(`${response.errorCode}:${response.errorMsg}`);
          return false;
        }
      } catch(e) {
        console.error(e);
        // 打印错误信息
        message.error('新增失败');
        return false;
      }
    },
    *updateParam({ payload }, sagaEffects) {
      const { call } = sagaEffects;
      try {
        // 获取服务端数据
        // call 第一个参数为一个函数，后面的参数为函数调用时的入参
        console.log(JSON.stringify(payload));
        const response = yield call(updateParam, payload);
        // console.log(JSON.stringify(response));
        const { success } = response;
        if (success) {
          message.success('更新成功');
          return true;
        } else {
          // message.error(`${response.errorCode}:${response.errorMsg}`);
          return false;
        }
      } catch(e) {
        console.error(e);
        message.error('更新失败');
        return false;
      }
    },
    *deleteParams({ payload }, sagaEffects) {
      const { call} = sagaEffects;
      try {
        // 获取服务端数据
        // call 第一个参数为一个函数，后面的参数为函数调用时的入参
        console.log(JSON.stringify(payload));
        const response = yield call(deleteParams, payload);
        // console.log(JSON.stringify(response));
        // yield call(delay, 3000);
        const { success } = response;
        if (success) {
          message.success('删除成功');
          // 刷新表格
          return true;
        } else {
          // message.error(`${response.errorCode}:${response.errorMsg}`);
          return false;
        }
      } catch(e) {
        console.error(e);
        message.error('删除失败');
        return false;
      }
    }
  },

  reducers: {
    updateState(state, { payload }) {
      console.info(payload);
      return {
        ...state,
        ...payload
      }
    },
  },
}

export default ParamModel;