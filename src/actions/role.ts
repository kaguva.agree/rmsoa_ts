export const ROLE_NAMESPACE = 'roles';

/**
 * 角色清单查询
 * @Role {*} payload 查询条件
 * @returns 角色清单
 */
export function ROLE_LIST(payload: {}) {
  return {
    type: `${ROLE_NAMESPACE}/fetchAllRole`,
    payload,
  };
}

/**
 * 新增角色
 * @Role {*} payload 角色信息
 * @returns true-新增成功
 */
export function ROLE_ADD(payload: {}) {
  return {
    type: `${ROLE_NAMESPACE}/addRole`,
    payload,
  };
}

/**
 * 更新角色信息
 * @Role {*} payload 角色信息
 * @returns true-更新成功
 */
export function ROLE_UPDATE(payload: {}) {
  return {
    type: `${ROLE_NAMESPACE}/updateRole`,
    payload,
  };
}

/**
 * 删除角色
 * @Role {*} payload 角色id集合
 * @returns true-删除成功
 */
export function ROLE_DELETE(payload: {}) {
  return {
    type: `${ROLE_NAMESPACE}/deleteRoles`,
    payload,
  };
}

/**
 * 角色授权
 * @param {*} payload 角色id及权限id集合
 * @returns true-授权成功
 */
export function GRANT_AUTH(payload: {}) {
  return {
    type: `${ROLE_NAMESPACE}/grantAuths`,
    payload,
  };
}

/**
 * 查询角色对应权限列表
 * @param {*} payload 角色id
 * @returns true-查询成功，返回角色对应权限集合
 */
export function MENU_TREE(payload: {}) {
  return {
    type: `${ROLE_NAMESPACE}/fetchMenuTreeByRole`,
    payload,
  };
}