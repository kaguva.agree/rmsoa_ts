export const APP_NAMESPACE = 'apps';

/**
 * 查询本页面需所有参数
 * @App {*} payload 查询条件
 * @returns 系统应用清单
 */
export function APP_INIT(payload: {}) {
  return {
    type: `${APP_NAMESPACE}/fetchAppInitParams`,
    payload,
  };
}

/**
 * 系统应用清单查询
 * @App {*} payload 查询条件
 * @returns 系统应用清单
 */
export function APP_LIST(payload: {}) {
  return {
    type: `${APP_NAMESPACE}/fetchAllApp`,
    payload,
  };
}

/**
 * 新增系统应用
 * @App {*} payload 系统应用信息
 * @returns true-新增成功
 */
export function APP_ADD(payload: {}) {
  return {
    type: `${APP_NAMESPACE}/addApp`,
    payload,
  };
}

/**
 * 更新系统应用信息
 * @App {*} payload 系统应用信息
 * @returns true-更新成功
 */
export function APP_UPDATE(payload: {}) {
  return {
    type: `${APP_NAMESPACE}/updateApp`,
    payload,
  };
}

/**
 * 删除系统应用
 * @App {*} payload 系统应用id集合
 * @returns true-删除成功
 */
export function APP_DELETE(payload: {}) {
  return {
    type: `${APP_NAMESPACE}/deleteApps`,
    payload,
  };
}

/**
 * 更新props
 * @App {*} payload 待更新的数据
 * @returns true-
 */
export function UPDATE_STATE(payload: {}) {
  return {
    type: `${APP_NAMESPACE}/updateAppState`,
    payload,
  };
}