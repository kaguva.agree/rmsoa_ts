export const PARAM_NAMESPACE = 'params';

/**
 * 参数清单查询
 * @param {*} payload 查询条件
 * @returns 参数清单
 */
export function PARAM_LIST(payload: {}) {
  return {
    type: `${PARAM_NAMESPACE}/fetchAllParam`,
    payload,
  };
}

/**
 * 新增参数
 * @param {*} payload 参数信息
 * @returns true-新增成功
 */
export function PARAM_ADD(payload: {}) {
  return {
    type: `${PARAM_NAMESPACE}/addParam`,
    payload,
  };
}

/**
 * 更新参数信息
 * @param {*} payload 参数信息
 * @returns true-更新成功
 */
export function PARAM_UPDATE(payload: {}) {
  return {
    type: `${PARAM_NAMESPACE}/updateParam`,
    payload,
  };
}

/**
 * 删除参数
 * @param {*} payload 参数id集合
 * @returns true-删除成功
 */
export function PARAM_DELETE(payload: {}) {
  return {
    type: `${PARAM_NAMESPACE}/deleteParams`,
    payload,
  };
}