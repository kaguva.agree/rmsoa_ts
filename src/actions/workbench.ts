export const WORKBENCH_NAMESPACE = 'workbenchs';

/**
 * 查询本页面需所有参数
 * @App {*} payload 查询条件
 * @returns 工作台参数
 */
export function WORKBENCH_INIT(payload: {}) {
  return {
    type: `${WORKBENCH_NAMESPACE}/fetchWorkbenchInitParams`,
    payload,
  };
}

/**
 * 查询需求待办任务
 * @App {*} payload 查询条件
 * @returns 系统应用清单
 */
export function FETCH_CQUNDOTASK(payload: {}) {
  return {
    type: `${WORKBENCH_NAMESPACE}/queryCqUndoTaskList`,
    payload,
  };
}

/**
 * 查询交易待办任务
 * @App {*} payload 查询条件
 * @returns 系统应用清单
 */
export function FETCH_TRADEUNDOTASK(payload: {}) {
  return {
    type: `${WORKBENCH_NAMESPACE}/queryTradeUndoTaskList`,
    payload,
  };
}

/**
 * 更新props
 * @App {*} payload 待更新的数据
 * @returns true-
 */
export function UPDATE_STATE(payload: {}) {
  return {
    type: `${WORKBENCH_NAMESPACE}/updateWorkbenchState`,
    payload,
  };
}

/**
 * 查询本页面需所有参数
 * @App {*} payload 查询条件
 * @returns 参数列表
 */
export function CQ_DETAIL_INIT(payload: {}) {
  return {
    type: `${WORKBENCH_NAMESPACE}/fetchCqDetailInitParams`,
    payload,
  };
}

/**
 * 录入需求详细信息
 * @App {*} payload 需求信息
 * @returns true-新增成功
 */
export function FILL_CQ_DETAIL(payload: {}) {
  return {
    type: `${WORKBENCH_NAMESPACE}/fillCqDetailInfo`,
    payload,
  };
}

/**
 * 查询本页面需所有参数
 * @App {*} payload 查询条件
 * @returns 参数列表
 */
export function CQ_DOC_INIT(payload: {}) {
  return {
    type: `${WORKBENCH_NAMESPACE}/fetchCqDocInitParams`,
    payload,
  };
}

/**
 * 录入需求文档信息
 * @App {*} payload 需求信息
 * @returns true-新增成功
 */
export function FILL_CQ_DOC(payload: {}) {
  return {
    type: `${WORKBENCH_NAMESPACE}/fillCqDocInfo`,
    payload,
  };
}

/**
 * 查询本页面需所有参数
 * @App {*} payload 查询条件
 * @returns 参数列表
 */
export function CQ_TRADE_INIT(payload: {}) {
  return {
    type: `${WORKBENCH_NAMESPACE}/fetchCqTradeInitParams`,
    payload,
  };
}

/**
 * 需求添加交易
 * @App {*} payload 需求交易信息
 * @returns true-新增成功
 */
export function CQ_ADD_TRADE(payload: {}) {
  return {
    type: `${WORKBENCH_NAMESPACE}/cqAddTrade`,
    payload,
  };
}

/**
 * 查询该需求下指定交易已分配的任务
 * @App {*} payload 需求交易信息
 * @returns true-新增成功
 */
export function TRADE_TASK(payload: {}) {
  return {
    type: `${WORKBENCH_NAMESPACE}/fetchTradeTasks`,
    payload,
  };
}

/**
 * 需求分配交易
 * @App {*} payload 需求信息
 * @returns true-新增成功
 */
export function ALLOT_CQ_TRADE(payload: {}) {
  return {
    type: `${WORKBENCH_NAMESPACE}/allotCqTrade`,
    payload,
  };
}

/**
 * 预分配交易任务
 * @App {*} payload 需求交易信息
 * @returns true-新增成功
 */
export function PREALLOC_TRADE_TASK(payload: {}) {
  return {
    type: `${WORKBENCH_NAMESPACE}/preAllocTradeTask`,
    payload,
  };
}

/**
 * 交易任务处理方法，add edit delete
 * @App {*} payload 需求交易信息
 * @returns true-新增成功
 */
export function UPDATE_TRADE_TASK(payload: {}) {
  return {
    type: `${WORKBENCH_NAMESPACE}/updateTradeTask`,
    payload,
  };
}

/**
 * 查询交易任务的历史流程
 * @App {*} payload 需求交易信息
 * @returns 历史流程
 */
export function TRADE_TASK_HIS(payload: {}) {
  return {
    type: `${WORKBENCH_NAMESPACE}/fetchTradeTaskComments`,
    payload,
  };
}

/**
 * 交易任务处理
 * @param payload 交易任务信息
 * @returns 处理结果
 */
export function HANDLE_TRADE_TASK(payload: {}) {
  return {
    type: `${WORKBENCH_NAMESPACE}/handleTradeTask`,
    payload,
  };
}
