export const DASHBOARD_NAMESPACE = 'dashboards';

/**
 * 查询本页面需所有参数
 * @App {*} payload 查询条件
 * @returns 系统应用清单
 */
export function DASHBOARD_INIT(payload: {}) {
  return {
    type: `${DASHBOARD_NAMESPACE}/fetchDashboardInitParams`,
    payload,
  };
}

/**
 * 项目总览数据查询
 * @App {*} payload 查询条件
 * @returns 系统应用清单
 */
export function FETCH_OVERVIEW(payload: {}) {
  return {
    type: `${DASHBOARD_NAMESPACE}/fetchOverview`,
    payload,
  };
}

/**
 * 项目统计数据查询
 * @App {*} payload 查询条件
 * @returns 项目最近一次投产的统计数据
 */
export function FETCH_LASTWILLPRD_STATISTICS(payload: {}) {
  return {
    type: `${DASHBOARD_NAMESPACE}/fetchStatisticOfLastWillPrd`,
    payload,
  };
}

/**
 * 项目统计数据查询
 * @App {*} payload 查询条件
 * @returns 系统应用清单
 */
export function FETCH_STATISTICS(payload: {}) {
  return {
    type: `${DASHBOARD_NAMESPACE}/fetchStatistics`,
    payload,
  };
}

/**
 * 项目统计数据查询
 * @App {*} payload 查询条件
 * @returns 项目已投产需求统计数据
 */
export function FETCH_ALREADYPRD_STATISTICS(payload: {}) {
  return {
    type: `${DASHBOARD_NAMESPACE}/fetchStatisticOfAlreadyPrd`,
    payload,
  };
}

/**
 * 项目统计数据查询
 * @App {*} payload 查询条件
 * @returns 项目未投产需求统计数据
 */
export function FETCH_NOTPRD_STATISTICS(payload: {}) {
  return {
    type: `${DASHBOARD_NAMESPACE}/fetchStatisticOfNotPrd`,
    payload,
  };
}

/**
 * 项目数据排名查询
 * @App {*} payload 查询条件
 * @returns 项目数据排名
 */
export function FETCH_RANKINGS(payload: {}) {
  return {
    type: `${DASHBOARD_NAMESPACE}/fetchRankings`,
    payload,
  };
}

/**
 * 更新props
 * @App {*} payload 待更新的数据
 * @returns true-
 */
export function UPDATE_STATE(payload: {}) {
  return {
    type: `${DASHBOARD_NAMESPACE}/updateAppState`,
    payload,
  };
}