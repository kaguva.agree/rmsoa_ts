export const JOB_NAMESPACE = 'schedules';

/**
 * 查询本页面需所有参数
 * @App {*} payload 查询条件
 * @returns 系统应用清单
 */
export function JOB_INIT(payload: {}) {
  return {
    type: `${JOB_NAMESPACE}/fetchJobInitParams`,
    payload,
  };
}

/**
 * 定时任务清单查询
 * @param {*} payload 查询条件
 * @returns 定时任务清单
 */
export function LIST_JOB(payload: {}) {
  return {
    type: `${JOB_NAMESPACE}/fetchAllJob`,
    payload,
  };
}

/**
 * 新增定时任务
 * @param {*} payload 定时任务信息
 * @returns true-新增成功
 */
export function ADD_JOB(payload: {}) {
  return {
    type: `${JOB_NAMESPACE}/addJob`,
    payload,
  };
}

/**
 * 更新定时任务信息
 * @param {*} payload 定时任务信息
 * @returns true-更新成功
 */
export function UPDATE_JOB(payload: {}) {
  return {
    type: `${JOB_NAMESPACE}/updateJob`,
    payload,
  };
}

/**
 * 删除定时任务
 * @param {*} payload 定时任务id集合
 * @returns true-删除成功
 */
export function DELETE_JOB(payload: {}) {
  return {
    type: `${JOB_NAMESPACE}/deleteJobs`,
    payload,
  };
}

/**
 * 立刻运行定时任务
 * @param {*} payload 定时任务id
 * @returns true-运行成功
 */
export function RUN_JOB(payload: {}) {
  return {
    type: `${JOB_NAMESPACE}/runJob`,
    payload,
  };
}

/**
 * 暂停定时任务
 * @param {*} payload 定时任务id
 * @returns true-暂停成功
 */
export function PAUSE_JOB(payload: {}) {
  return {
    type: `${JOB_NAMESPACE}/pauseJob`,
    payload,
  };
}

/**
 * 恢复定时任务
 * @param {*} payload 定时任务id
 * @returns true-恢复成功
 */
export function RESUME_JOB(payload: {}) {
  return {
    type: `${JOB_NAMESPACE}/resumeJob`,
    payload,
  };
}