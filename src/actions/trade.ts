export const TRADE_NAMESPACE = 'trades';

/**
 * 查询本页面需所有参数
 * @App {*} payload 查询条件
 * @returns 交易清单
 */
export function TRADE_INIT(payload: {}) {
  return {
    type: `${TRADE_NAMESPACE}/fetchTradeInitParams`,
    payload,
  };
}

/**
 * 交易清单查询
 * @Dict {*} payload 查询条件
 * @returns 交易清单
 */
export function TRADE_LIST(payload: {}) {
  return {
    type: `${TRADE_NAMESPACE}/fetchAllTrade`,
    payload,
  };
}

/**
 * 新增系统字典
 * @Dict {*} payload 系统字典信息
 * @returns true-新增成功
 */
export function TRADE_ADD(payload: {}) {
  return {
    type: `${TRADE_NAMESPACE}/addTrade`,
    payload,
  };
}

/**
 * 更新系统字典信息
 * @Dict {*} payload 系统字典信息
 * @returns true-更新成功
 */
export function TRADE_UPDATE(payload: {}) {
  return {
    type: `${TRADE_NAMESPACE}/updateTrade`,
    payload,
  };
}

/**
 * 删除系统字典
 * @Dict {*} payload 系统字典id集合
 * @returns true-删除成功
 */
export function TRADE_DELETE(payload: {}) {
  return {
    type: `${TRADE_NAMESPACE}/deleteTrades`,
    payload,
  };
}