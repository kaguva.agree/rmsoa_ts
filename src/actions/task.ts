export const TASK_NAMESPACE = 'tasks';

/**
 * 查询本页面需所有参数
 * @Task {*} payload 查询条件
 * @returns 系统应用清单
 */
export function TASK_INIT(payload: {}) {
  return {
    type: `${TASK_NAMESPACE}/fetchTaskInitParams`,
    payload,
  };
}

/**
 * 系统应用清单查询
 * @Task {*} payload 查询条件
 * @returns 系统应用清单
 */
export function TASK_LIST(payload: {}) {
  return {
    type: `${TASK_NAMESPACE}/fetchAllTask`,
    payload,
  };
}

/**
 * 查询任务详细信息
 * @Task {*} payload 查询条件
 * @returns 任务详细信息
 */
export function TASK_DETAIL(payload: {}) {
  return {
    type: `${TASK_NAMESPACE}/fetchTaskDetail`,
    payload,
  };
}

/**
 * 更新props
 * @Task {*} payload 待更新的数据
 * @returns true-
 */
export function UPDATE_STATE(payload: {}) {
  return {
    type: `${TASK_NAMESPACE}/updateTaskState`,
    payload,
  };
}