export const CQ_NAMESPACE = 'cqs';

/**
 * 查询本页面需所有参数
 * @App {*} payload 查询条件
 * @returns 参数列表
 */
export function CQ_ADD_INIT(payload: {}) {
  return {
    type: `${CQ_NAMESPACE}/fetchCqAddInitParams`,
    payload,
  };
}

/**
 * 新增需求
 * @App {*} payload 需求信息
 * @returns true-新增成功
 */
export function CQ_ADD_BASE(payload: {}) {
  return {
    type: `${CQ_NAMESPACE}/addCqBaseInfo`,
    payload,
  };
}

/**
 * 查询本页面需所有参数
 * @App {*} payload 查询条件
 * @returns 参数列表
 */
export function CQ_LIST_INIT(payload: {}) {
  return {
    type: `${CQ_NAMESPACE}/fetchCqListInitParams`,
    payload,
  };
}

/**
 * 需求清单查询
 * @App {*} payload 查询条件
 * @returns 需求清单
 */
export function CQ_LIST(payload: {}) {
  return {
    type: `${CQ_NAMESPACE}/fetchAllCq`,
    payload,
  };
}

/**
 * 查询需求详细信息
 * @Task {*} payload 查询条件
 * @returns 任务详细信息
 */
export function CQ_DETAIL(payload: {}) {
  return {
    type: `${CQ_NAMESPACE}/fetchCqDetail`,
    payload,
  };
}

/**
 * 更新props
 * @App {*} payload 待更新的数据
 * @returns true-
 */
export function UPDATE_STATE(payload: {}) {
  return {
    type: `${CQ_NAMESPACE}/updateCqState`,
    payload,
  };
}