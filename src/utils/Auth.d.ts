/**
 * 路由权限模型类，即页面
 */
export interface RouteItem {
  id: string;
  code: string;
  icon: string;
  name: string;
  route: string;
  alias: string;
  action: string;
  hideInMenu: boolean;
  children: RouteItem[];
}

/**
 * 保存在浏览器本地的路由菜单数据结构，当前登录用户的菜单集合
 */
export interface AuthRoute {
  id: string;
  code: string;
  icon: string;
  name: string;
  route: string;
  alias?: string;
  action?: string;
  hideMenu: boolean
  children: AuthRoute[];
}

/**
 * 服务端返回的按钮权限模型类
 */
export interface ButtonItem {
  id?: string;
  code: string;
  icon?: string;
  name: string;
  route?: string;
  alias?: string;
  action: string;
  children?: ButtonItem[];
}

/**
 * 保存在浏览器本地的按钮数据结构，一个页面编码及页面下的所有按钮
 */
export interface AuthButton {
  /** 页面编码 */
  code: string;
  /** 按钮集合 */
  buttons: ButtonItem[];
}