import { message } from 'antd';
import request from './request';

/**
 * 分页查询参数模型
 */
export interface PageQueryParamType {
  pageSize: number;
  pageNum: number;
}

export function sendPostRequest(url: string, params: {}) {
  return request(url, {
    method: 'POST',
    data: params
  });
}

export function sendGetJsonRequest(url: string, params: {}) {
  return request(url, {
    method: 'GET',
    headers: {
      'Content-Type': 'application/json; charset=utf-8',
    },
    data: params
  });
}

/**
 * 分页查询
 * @param {*} url 请求url
 * @param {*} params 分页查询条件
 * @returns 分页结果
 */
export function pageRequest(url: string, params: PageQueryParamType) {
  const { pageSize, pageNum, ...rest } = params;
  if (!pageSize || pageSize <= 0 || !pageNum || pageNum <= 0) {
    const errorMsg: string = '分页查询参数有误，请检查';
    message.error(errorMsg);
    return Promise.reject(errorMsg);
  }
  // 为了统一处理，分页采用特殊的结构类型，
  // 除分页参数外，其他条件查询均上送到params中
  const data = {
    pageSize,
    pageNum,
    params: {
      ...rest
    }
  };
  return request(url, {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json; charset=utf-8',
    },
    data
  });
}