import React from 'react';
import { Button } from 'antd';
import styles from '@/components/SelfComp/style.less';
import { ButtonItem } from '@/utils/Auth';

export interface AToolBarProps {
  /** 按钮集合 */
  buttons?: ButtonItem[];
  /** 左边自定义按钮函数 */
  renderLeftButton?: Function;
  /** 右边自定义按钮函数 */
  renderRightButton?: Function;
  /** 按钮点击事件函数 */
  onClick?: (btn: ButtonItem) => void;
}

/**
 * 表格操作栏
 */
export default class AToolBar extends React.PureComponent<AToolBarProps> {

  render() {
    // 默认按钮，左边按钮，右边按钮，按钮点击事件
    // buttons
    // renderLeftButton 紧跟在默认按钮的后面，按钮集合<Button/>
    // renderRightButton 按钮集合<Button/>
    // onClick 默认按钮点击事件
    const { buttons, renderLeftButton, renderRightButton, onClick } = this.props;
    return (
      <div className={styles.operator}>
        <div>
          {buttons ? buttons
            .filter(button => button.action === '1' || button.action === '3')
            .map(button => (
              <Button
                key={button.code}
                type='primary'
                danger={button.alias === 'delete'}
                onClick={() => {
                  // const { click } = button;
                  // click();
                  if (onClick) {
                    onClick(button);
                  }
                }}
              >
                {button.name}
              </Button>
            )) : null
          }
          {renderLeftButton ? renderLeftButton() : null}
          {renderRightButton ? (<div className={styles.subOperator}>{renderRightButton()}</div>) : null}
        </div>
      </div>
    );
  }
}