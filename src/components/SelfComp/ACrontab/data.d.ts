export interface PaneProps {
  value?: string;
  onChange?: Function
}

export interface PaneState {
  currentRadio: number;
}

interface InputProps {
  value?: string;
  onChange?: Function;
  disabled: boolean;
}

export type InputFromIntervalProps = InputProps & {

}

export interface InputFromIntervalState {
  from: number;
  interval: number;
}

export type InputFromToProps = InputProps & {

}

export interface InputFromToState {
  from: number | string;
  to: number | string;
}

export type InputSpecifiedProps = InputProps & {

}

export interface InputSpecifiedState {
  selected: string[] | number[];
}

export type InputLastProps = InputProps & {

}

export interface InputLastState {
  lastWeekOfMonth: string;
}

export type InputTargetProps = InputProps & {

}

export interface InputTargetState {
  weekOfMonth?: number;
  dayOfWeek?: string;
  dayOfMonth?: number,
  workDayFlag?: string
}

