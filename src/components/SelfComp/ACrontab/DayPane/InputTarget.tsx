import React, { Fragment, PureComponent } from 'react';
import { InputNumber, Select } from 'antd';
import memoizeOne from 'memoize-one';
import WeekSelect from './WeekSelect';
import { InputTargetProps, InputTargetState } from '../data';

/**
 * cron时间表达式生成器-日面板
 * 属性如下：
 */
class InputTarget extends React.PureComponent<InputTargetProps, InputTargetState > {

  state: InputTargetState = {
    dayOfMonth: 1,
    workDayFlag: 'W'
  }

  /**
   * 静态函数，根据新传入的props来映射到state。
   * 该函数必须有返回值。当props传入的内容不需要影响state，就必须返回一个null
   */
  static getDerivedStateFromProps(nextProps: InputTargetProps, prevState: InputTargetState) {
    // console.log('InputFromTo.getDerivedStateFromProps: ', nextProps, prevState);
    // 重新查询清空表格勾选
    const { disabled } = nextProps;
    if (disabled) {
      return {
        dayOfMonth: 1,
        workDayFlag: 'W'
      };
    }
    return null;
  }

  onChangeWorkDay = (value: number | null) => {
    const { onChange } = this.props;
    const { workDayFlag } = this.state;
    if (value) {
      this.setState({
        dayOfMonth: value
      });
      if (onChange) {
        onChange(`${value || 1}${workDayFlag}`);
      }
    }
  }

  render() {
    const { disabled, value } = this.props;
    let { dayOfMonth, workDayFlag } = this.state;
    if (!disabled && value && workDayFlag) {
      const [dayOfMonthEx] = value.split(workDayFlag);
      dayOfMonth = parseInt(dayOfMonthEx, 10);
      console.log('日循环', dayOfMonth, workDayFlag, value);
    }

    return (
      <React.Fragment>
        每月&nbsp;
        <InputNumber
            disabled={disabled}
            min={1}
            max={31}
            value={dayOfMonth}
            onChange={this.onChangeWorkDay}
            style={{ width: 100 }}
        />
        &nbsp;号最近的那个工作日执行一次
      </React.Fragment>
    );
  }
}

export default InputTarget;