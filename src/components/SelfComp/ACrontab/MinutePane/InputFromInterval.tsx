import React from 'react';
import { InputNumber } from 'antd';
import { InputFromIntervalProps, InputFromIntervalState } from '../data';

/**
 * cron时间表达式生成器-分面板
 * 属性如下：
 */
class InputFromInterval extends React.PureComponent<InputFromIntervalProps, InputFromIntervalState> {

  state: InputFromIntervalState = {
    from: 0,
    interval: 1
  }

  /**
   * 静态函数，根据新传入的props来映射到state。
   * 该函数必须有返回值。当props传入的内容不需要影响state，就必须返回一个null
   */
  static getDerivedStateFromProps(nextProps: InputFromIntervalProps, prevState: InputFromIntervalState) {
    const { disabled, value } = nextProps;
    if (disabled) {
      return {
        from: 0,
        interval: 1
      }
    } else {
      if (value) {
        const [from, interval] = value.split('/').map((v) => parseInt(v, 10));
        return {
          from,
          interval
        }
      }
    }
    return null;
  }

  onChangeFrom = (value: number | null) => {
    const { onChange } = this.props;
    const { interval } = this.state;
    if (value) {
      this.setState({
        from: value
      });
      if (onChange) {
        onChange(`${value || 0}/${interval}`);
      }
    }
  }

  onChangeInterval = (value: number | null) => {
    const { onChange } = this.props;
    const { from } = this.state;
    if (value) {
      this.setState({
        interval: value
      });
      if (onChange) {
        onChange(`${from}/${value || 0}`);
      }
    }
  }

  render() {
    const { disabled } = this.props;
    let { from, interval } = this.state;

    return (
      <React.Fragment>
        周期从&nbsp;
        <InputNumber
            disabled={disabled}
            min={0}
            max={59}
            value={from}
            onChange={this.onChangeFrom}
            style={{ width: 100 }}
        />
        &nbsp;分开始，每&nbsp;
        <InputNumber
            disabled={disabled}
            min={1}
            max={59}
            value={interval}
            onChange={this.onChangeInterval}
            style={{ width: 100 }}
        />
        &nbsp;分钟执行一次
      </React.Fragment>
    );
  }
}

export default InputFromInterval;