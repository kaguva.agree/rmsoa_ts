import React from 'react';
import { Radio, RadioChangeEvent, Space } from 'antd';
import InputFromInterval from './InputFromInterval';
import InputFromTo from './InputFromTo';
import InputSpecified from './InputSpecified';
import { PaneProps, PaneState } from '../data';

/**
 * cron时间表达式生成器-分面板
 * 属性如下：
 */
class MinutePane extends React.PureComponent<PaneProps, PaneState> {

  state: PaneState = {
    currentRadio: 0
  }

  /**
   * 静态函数，根据新传入的props来映射到state。
   * 该函数必须有返回值。当props传入的内容不需要影响state，就必须返回一个null
   */
  static getDerivedStateFromProps(nextProps: PaneProps, prevState: PaneState) {
    const { value } = nextProps;
    let currentRadio = 0;
    if (value) {
      if (value === '*') {
        currentRadio = 0;
      } else if (value.indexOf('-') > -1) {
        currentRadio = 1;
      } else if (value.indexOf('/') > -1) {
        currentRadio = 2;
      } else {
        currentRadio = 3;
      }
      return {
        currentRadio
      };
    }
    return null;
  }

  onChangeRadio = (e: RadioChangeEvent) => {
    const valueType = e.target.value;
    this.setState({
      currentRadio: valueType
    });
    const { onChange } = this.props;
    if (onChange) {
      const defaultValues = ['*', '0-0', '0/1', '0'];
      onChange(defaultValues[valueType]);
    }
  };

  render() {

    const radioStyle = { lineHeight: '38px' };
    const { value, onChange } = this.props;
    const { currentRadio } = this.state;

    return (
      <Radio.Group style={{ width: '100%' }} value={currentRadio} onChange={this.onChangeRadio}>
        <Space direction='vertical'>
          <Radio style={radioStyle} value={0}>
            每分钟，允许的通配符 [, - * /]
          </Radio>
          <Radio style={radioStyle} value={1}>
            <InputFromTo disabled={currentRadio !== 1} value={value} onChange={onChange} />
          </Radio>
          <Radio style={radioStyle} value={2}>
            <InputFromInterval disabled={currentRadio !== 2} value={value} onChange={onChange} />
          </Radio>
          <Radio style={radioStyle} value={3}>
            <InputSpecified disabled={currentRadio !== 3} value={value} onChange={onChange} />
          </Radio>
        </Space>
      </Radio.Group>
    );
  }
}

export default MinutePane;