import React from 'react';
import WeekSelect from './WeekSelect';
import { InputLastProps, InputLastState } from '../data';

/**
 * cron时间表达式生成器-周面板
 * 属性如下：
 */
class InputLast extends React.PureComponent<InputLastProps, InputLastState> {

  state: InputLastState = {
    lastWeekOfMonth: 'MON',
  }

  /**
   * 静态函数，根据新传入的props来映射到state。
   * 该函数必须有返回值。当props传入的内容不需要影响state，就必须返回一个null
   */
  static getDerivedStateFromProps(nextProps: InputLastProps, prevState: InputLastState) {
    // console.log('InputFromTo.getDerivedStateFromProps: ', nextProps, prevState);
    // 重新查询清空表格勾选
    const { disabled } = nextProps;
    if (disabled) {
      return {
        lastWeekOfMonth: 'MON'
      };
    }
    return null;
  }

  onChangeLastWeekOfMonth = (value: string) => {
    const { onChange } = this.props;
    this.setState({
      lastWeekOfMonth: value
    });
    if (onChange) {
      onChange(`${value}L`);
    }
  }

  render() {
    const { disabled, value } = this.props;
    let { lastWeekOfMonth } = this.state;
    if (!disabled && value) {
      [lastWeekOfMonth] = value.split('L');
      console.log('周循环2', lastWeekOfMonth, value);
    }

    return (
      <React.Fragment>
        本月的最后一个&nbsp;
        <WeekSelect
          disabled={disabled}
          value={lastWeekOfMonth}
          onChange={this.onChangeLastWeekOfMonth}
          style={{ width: 100 }}
        />
        &nbsp;执行一次
      </React.Fragment>
    );
  }
}

export default InputLast;