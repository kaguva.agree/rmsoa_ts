import React from 'react';
import { Radio, RadioChangeEvent, Space } from 'antd';
import InputFromTo from './InputFromTo';
import InputTarget from './InputTarget';
import InputLast from './InputLast';
import InputSpecified from './InputSpecified';

export interface WeekProps {
  value?: string;
  onChange?: Function
}

export interface WeekState {
  currentRadio: number;
}

/**
 * cron时间表达式生成器-周面板
 * 属性如下：
 */
class WeekPane extends React.PureComponent<WeekProps, WeekState> {

  state: WeekState = {
    currentRadio: 1
  }

  /**
   * 静态函数，根据新传入的props来映射到state。
   * 该函数必须有返回值。当props传入的内容不需要影响state，就必须返回一个null
   */
  static getDerivedStateFromProps(nextProps: WeekProps, prevState: WeekState) {
    const { value } = nextProps;
    let currentRadio = 1;
    if (value) {
      if (value === '*') {
        currentRadio = 0;
      } else if (value === '?') {
        currentRadio = 1;
      } else if (value.indexOf('-') > -1) {
        currentRadio = 2;
      } else if (value.indexOf('#') > -1) {
        currentRadio = 3;
      } else if (value.indexOf('L') > -1) {
        currentRadio = 4;
      } else {
        currentRadio = 5;
      }
      return {
        currentRadio
      };
    }
    return null;
  }

  onChangeRadio = (e: RadioChangeEvent) => {
    const valueType = e.target.value;
    this.setState({
      currentRadio: valueType
    });
    const { onChange } = this.props;
    if (onChange) {
      const defaultValues = ['*', '?', 'MON-SUN', '1#MON', 'MONL', 'MON'];
      onChange(defaultValues[valueType]);
    }
  };

  render() {

    const radioStyle = { lineHeight: '38px' };
    const { value, onChange } = this.props;
    const { currentRadio } = this.state;

    return (
      <Radio.Group style={{ width: '100%' }} value={currentRadio} onChange={this.onChangeRadio}>
        <Space direction='vertical'>
          <Radio style={radioStyle} value={0}>
            每周，允许的通配符 [, - * /]
          </Radio>
          <Radio style={radioStyle} value={1}>
            不指定，已设置了日相关配置
          </Radio>
          <Radio style={radioStyle} value={2}>
            <InputFromTo disabled={currentRadio !== 2} value={value} onChange={onChange} />
          </Radio>
          <Radio style={radioStyle} value={3}>
            <InputTarget disabled={currentRadio !== 3} value={value} onChange={onChange} />
          </Radio>
          <Radio style={radioStyle} value={4}>
            <InputLast disabled={currentRadio !== 4} value={value} onChange={onChange} />
          </Radio>
          <Radio style={radioStyle} value={5}>
            <InputSpecified disabled={currentRadio !== 5} value={value} onChange={onChange} />
          </Radio>
        </Space>
      </Radio.Group>
    );
  }
}

export default WeekPane;