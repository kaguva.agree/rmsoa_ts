import React from 'react';
import { Table, Pagination, TableProps, PaginationProps } from 'antd';
// import styles from './index.less';

export type ATableProps<TableDataType extends object = any> = TableProps<TableDataType> & PaginationProps & {
  /** 表格勾选数据 */
  selectedRows?: TableDataType[];
  /** 表格勾选钩子函数 */
  onSelectRow?: Function;
  /** 页码改变或每页记录数改变的回调函数 */
  onPageNumAndSizeChange?: (page: number, pageSize: number) => void
};

// interface ATableProps2<RecordType extends object = any> extends TableProps<RecordType> {
//   /** 表格勾选数据 */
//   selectedRows?: React.Key[];
//   /** 表格勾选钩子函数 */
//   onSelectRow?: Function;
//   /** 数据总数 */
//   total: number;
//   /** 当前页码 */
//   currentPageNum: number;
//   /** 每页记录数 */
//   pageSize: number;
//   onShowSizeChange: (page: number, pageSize: number) => void;
//   onPageNumChange: (current: number, size: number) => void
// }

interface ATableState {
  /** 表格勾选数据key */
  selectedRowKeys: React.Key[]
}

/**
 * 表格和分页的简单封装，设置了一些默认属性
 */
export default class ATable<TableDataType extends object> extends React.PureComponent<ATableProps, ATableState> {

  constructor(props: ATableProps) {
    console.info('ATable.constructor');
    super(props);
    this.state = {
      selectedRowKeys: [],
    };
  }

  /**
   * 静态函数，根据新传入的props来映射到state。
   * 该函数必须有返回值。当props传入的内容不需要影响state，就必须返回一个null
   */
  static getDerivedStateFromProps(nextProps: ATableProps, prevState: ATableState) {
    // console.log('getDerivedStateFromProps: ', nextProps, prevState);
    // 重新查询清空表格勾选
    const { selectedRows = [] } = nextProps;
    if (selectedRows.length === 0) {
      return {
        selectedRowKeys: []
      };
    }
    return null;
  }

  /**
   * 表格勾选处理事件
   * @param selectedRowKeys 指定选中项的 key 数组
   * @param selectedRows 指定选中项数组
   */
  onSelectRowChange = (newSelectedRowKeys: React.Key[] = [], newSelectedRows: TableDataType[] = []) => {
    console.log('selectedRowKeys changed: ', newSelectedRowKeys);
    console.log(newSelectedRows);
    this.setState({
      selectedRowKeys: [...newSelectedRowKeys]
    });
    // 表格勾选事件回调，给父组件传递勾选数据
    const { onSelectRow } = this.props;
    if (onSelectRow) {
      onSelectRow(newSelectedRowKeys, newSelectedRows);
    }
  };

  render() {
    const { columns, rowKey, dataSource, scroll, loading, total, rowClassName } = this.props;
    const { onPageNumAndSizeChange } = this.props;
    const { selectedRowKeys } = this.state;

    const showTotal = (total2: number, range: [number, number]) => {
      // console.log(range)
      if (total2 === 0 || total2 === undefined) {
        return '表中暂无数据';
      }
      return `显示第${range[0]}条到第${range[1]}条，共${total2}条数据`;
    };
    // 表格勾选处理
    const rowSelection = {
      selectedRowKeys,
      onChange: this.onSelectRowChange,
      hideDefaultSelections: true,
    };

    return (
      <React.Fragment>
        <Table<TableDataType>
          rowSelection={rowSelection}
          columns={columns}
          rowKey={rowKey}
          dataSource={dataSource}
          pagination={false}
          scroll={scroll}
          bordered
          loading={loading}
          rowClassName={rowClassName}
          size='middle'
        />
        {
          (total === undefined || total === 0) ? <React.Fragment /> :
          <Pagination
            showSizeChanger
            total={total}
            style={{marginTop:10}}
            showTotal={showTotal}
            onChange={onPageNumAndSizeChange}
            defaultCurrent={1}
            defaultPageSize={10}
          />
        }
      </React.Fragment>
    );
  }
}